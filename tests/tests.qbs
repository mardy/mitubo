import qbs 1.0

Project {
    condition: project.buildTests

    Test {
        name: "feed-parser-test"
        files: [
            "../src/feed_parser.cpp",
            "../src/feed_parser.h",
            "tst_feed_parser.cpp",
        ]
        Depends { name: 'Qt.network' }
    }

    Test {
        name: "utils-test"
        files: [
            "../src/utils.cpp",
            "../src/utils.h",
            "tst_utils.cpp",
        ]
    }

    CoverageClean {
        condition: project.enableCoverage
    }

    CoverageReport {
        condition: project.enableCoverage
        extractPatterns: [ '*/src/*.cpp' ]
    }
}
