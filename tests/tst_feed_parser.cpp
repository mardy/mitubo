/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed_parser.h"

#include <QBuffer>
#include <QLocalSocket>
#include <QMetaEnum>
#include <QSignalSpy>
#include <QTest>
#include <QVector>

#include <unistd.h>

using namespace MiTubo;

namespace QTest {

template<>
char *toString(const MiTubo::FeedItem &p)
{
    const QMetaEnum formatMeta = QMetaEnum::fromType<Qt::TextFormat>();

    QByteArray ba = "FeedItem{\n";
    ba += p.title.toUtf8() + '\n';
    ba += p.description.toUtf8() + '\n';
    ba += QByteArray(formatMeta.valueToKey(p.descriptionFormat)) + '\n';
    ba += p.date.toString(Qt::ISODate).toUtf8() + '\n';
    ba += p.url.toString().toUtf8() + '\n';
    ba += p.thumbnail.toString().toUtf8() + '\n';
    ba += "}";
    return qstrdup(ba.data());
}

} // namespace QTest

class FeedParserTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testSegmentation_data();
    void testSegmentation();
};

static const QByteArray peerTubeRss = R"mituboDelimiter(<?xml version="1.0" encoding="utf-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/">
    <channel>
        <title>TraTerra&amp;Cielo</title>
        <link>https://peertube.it</link>
        <description>E&apos; nato in città.</description>
        <lastBuildDate>Tue, 24 Aug 2021 16:03:24 GMT</lastBuildDate>
        <docs>http://blogs.law.harvard.edu/tech/rss</docs>
        <generator>Toraifōsu</generator>
        <image>
            <title>TraTerra&amp;Cielo</title>
            <url>https://peertube.it/client/assets/icons/icon-96x96.png</url>
            <link>https://peertube.it</link>
        </image>
        <copyright>All rights reserved.</copyright>
        <atom:link href="https://peertube.it/feeds/videos.atom?videoChannelId=546" rel="self" type="application/rss+xml"/>
        <item>
            <title><![CDATA[Walter Ricciardi su La7]]></title>
            <link>https://peertube.it/w/4944-a288-33745b70f735</link>
            <guid>https://peertube.it/w/4944-a288-33745b70f735</guid>
            <pubDate>Tue, 24 Aug 2021 09:08:57 GMT</pubDate>
            <description><![CDATA[Ricciardi fa capire tante cose.]]></description>
            <dc:creator>TraTerra&amp;Cielo.live</dc:creator>
            <media:category scheme="http://search.yahoo.com/mrss/category_schema" label="News &amp; Politics">11</media:category>
            <media:community>
                <media:statistics views="61">
                </media:statistics>
            </media:community>
            <media:embed url="/videos/embed/4eac8aa4-10a8-4944-a288-33745b70f735">
            </media:embed>
            <media:player url="/w/4eac8aa4-10a8-4944-a288-33745b70f735">
            </media:player>
            <enclosure type="application/x-bittorrent" url="https://peertube.it/464.torrent" length="888382">
            </enclosure>
            <media:group>
                <media:peerLink type="application/x-bittorrent" href="https://peertube.it/464.torrent" isDefault="true">
                </media:peerLink>
                <media:peerLink type="application/x-bittorrent" href="https://peertube.it/464-hls.torrent">
                </media:peerLink>
                <media:content url="https://peertube.it/464.mp4" fileSize="888382" type="video/mp4" medium="video" framerate="30" duration="32" height="464" lang="it">
                </media:content>
                <media:content url="https://peertube.it/fragmented.mp4" fileSize="1180334" type="video/mp4" medium="video" framerate="30" duration="32" height="464" lang="it">
                </media:content>
            </media:group>
            <media:thumbnail url="https://peertube.it/previews/f8f9db75600d.jpg" height="480" width="850">
            </media:thumbnail>
            <media:title type="plain">titolo media</media:title>
            <media:description type="plain">descrizione media</media:description>
            <media:rating>nonadult</media:rating>
        </item>
        <item>
            <title><![CDATA[Meluzzi sceglie di non vaccinarsi per finta]]></title>
            <link>https://peertube.it/w/892f-34724e36cf82</link>
            <guid>https://peertube.it/w/892f-34724e36cf82</guid>
            <pubDate>Tue, 24 Aug 2021 09:07:00 GMT</pubDate>
            <description><![CDATA[Meluzzi fa una scelta per l'umanità.
Una lezione di civilità.]]></description>
            <content:encoded><![CDATA[Meluzzi fa tante cose]]></content:encoded>
            <dc:creator>TraTerra&amp;Cielo.live</dc:creator>
            <media:category scheme="http://search.yahoo.com/mrss/category_schema" label="People">8</media:category>
            <media:community>
                <media:statistics views="50">
                </media:statistics>
            </media:community>
            <media:embed url="/videos/embed/34724e36cf82">
            </media:embed>
            <media:player url="/w/34724e36cf82">
            </media:player>
            <enclosure type="application/x-bittorrent" url="https://peertube.it/360.torrent" length="1686932">
            </enclosure>
            <media:group>
                <media:peerLink type="application/x-bittorrent" href="https://peertube.it/360.torrent" isDefault="true">
                </media:peerLink>
                <media:peerLink type="application/x-bittorrent" href="https://peertube.it/360-hls.torrent">
                </media:peerLink>
                <media:content url="https://peertube.it/360.mp4" fileSize="1686932" type="video/mp4" medium="video" framerate="25" duration="55" height="360" lang="it">
                </media:content>
                <media:content url="https://peertube.it/360-fragmented.mp4" fileSize="2089331" type="video/mp4" medium="video" framerate="25" duration="55" height="360" lang="it">
                </media:content>
            </media:group>
            <media:thumbnail url="https://peertube.it/previews/75871056f.jpg" height="480" width="850">
            </media:thumbnail>
            <media:title type="plain">Meluzzi sceglie di non vaccinarsi per finta</media:title>
            <media:description type="html">Meluzzi fa una scelta per l&apos;umanità.
Una lezione di coerenza e di civilità.</media:description>
            <media:rating>nonadult</media:rating>
        </item>
    </channel>
</rss>)mituboDelimiter";

static const QByteArray youTubeAtom = R"mituboDelimiter(<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns:yt="http://www.youtube.com/xml/schemas/2015" xmlns:media="http://search.yahoo.com/mrss/" xmlns="http://www.w3.org/2005/Atom">
 <link rel="self" href="http://www.youtube.com/feeds/videos.xml?channel_id=UCMYxuPQId-JRp-EK6NHsMzA"/>
 <id>yt:channel:UCMYxuPQId-JRp-EK6NHsMzA</id>
 <yt:channelId>UCMYxuPQId-JRp-EK6NHsMzA</yt:channelId>
 <title>Сталинград. XXI век</title>
 <link rel="alternate" href="https://www.youtube.com/channel/UCMYxuPQId-JRp-EK6NHsMzA"/>
 <author>
  <name>Сталинград. XXI век</name>
  <uri>https://www.youtube.com/channel/UCMYxuPQId-JRp-EK6NHsMzA</uri>
 </author>
 <published>2019-12-25T13:30:33+00:00</published>
 <entry>
  <id>yt:video:JiYgOcMu0Uw</id>
  <yt:videoId>JiYgOcMu0Uw</yt:videoId>
  <yt:channelId>UCMYxuPQId-JRp-EK6NHsMzA</yt:channelId>
  <title>Золотая молочка, 200$ за свободу, осенью по-новой</title>
  <link rel="alternate" href="https://www.youtube.com/watch?v=JiYgOcMu0Uw"/>
  <author>
   <name>Сталинград. XXI век</name>
   <uri>https://www.youtube.com/channel/UCMYxuPQId-JRp-EK6NHsMzA</uri>
  </author>
  <published>2021-08-26T16:40:42+00:00</published>
  <updated>2021-08-26T16:41:43+00:00</updated>
  <media:group>
   <media:title>Золотая молочка, 200$ за свободу, осенью по-новой</media:title>
   <media:content url="https://www.youtube.com/v/JiYgOcMu0Uw?version=3" type="application/x-shockwave-flash" width="640" height="390"/>
   <media:thumbnail url="https://i3.ytimg.com/vi/JiYgOcMu0Uw/hqdefault.jpg" width="480" height="360"/>
   <media:description>помогите каналу &quot;Сталинград&quot;:
Яндекс-деньги: 4100110580591219</media:description>
   <media:community>
    <media:starRating count="2767" average="4.98" min="1" max="5"/>
    <media:statistics views="14350"/>
   </media:community>
  </media:group>
 </entry>
 <entry>
  <id>yt:video:9Wo4Y0YUNcU</id>
  <yt:videoId>9Wo4Y0YUNcU</yt:videoId>
  <yt:channelId>UCMYxuPQId-JRp-EK6NHsMzA</yt:channelId>
  <title>Путинская подачка, инопаспорт для чиновника, экокатастрофа</title>
  <link rel="alternate" href="https://www.youtube.com/watch?v=9Wo4Y0YUNcU"/>
  <author>
   <name>Сталинград. XXI век</name>
   <uri>https://www.youtube.com/channel/UCMYxuPQId-JRp-EK6NHsMzA</uri>
  </author>
  <published>2021-08-25T16:24:55+00:00</published>
  <updated>2021-08-26T06:22:19+00:00</updated>
  <media:group>
   <media:title>Путинская подачка, инопаспорт для чиновника, экокатастрофа</media:title>
   <media:content url="https://www.youtube.com/v/9Wo4Y0YUNcU?version=3" type="application/x-shockwave-flash" width="640" height="390"/>
   <media:thumbnail url="https://i2.ytimg.com/vi/9Wo4Y0YUNcU/hqdefault.jpg" width="480" height="360"/>
   <media:description>Подписывайтесь на наши каналы:
НАПРАВЛЕНИЕ УДАРА</media:description>
   <media:community>
    <media:starRating count="3890" average="4.96" min="1" max="5"/>
    <media:statistics views="33000"/>
   </media:community>
  </media:group>
 </entry>
</feed>)mituboDelimiter";

static const QByteArray exampleAtom = R"mituboDelimiter(<?xml version="1.0" encoding="utf-8"?>
<feed xmlns="http://www.w3.org/2005/Atom">
 <title type="text">dive into mark</title>
 <subtitle type="html">
   A &lt;em&gt;lot&lt;/em&gt; of effort
   went into making this effortless
 </subtitle>
 <updated>2005-07-31T12:29:29Z</updated>
 <id>tag:example.org,2003:3</id>
 <link rel="alternate" type="text/html"
  hreflang="en" href="http://example.org/"/>
 <link rel="self" type="application/atom+xml"
  href="http://example.org/feed.atom"/>
 <rights>Copyright (c) 2003, Mark Pilgrim</rights>
 <generator uri="http://www.example.com/" version="1.0">
   Example Toolkit
 </generator>
 <entry>
   <title>Atom draft-07 snapshot</title>
   <link rel="alternate" type="text/html"
    href="http://example.org/2005/04/02/atom"/>
   <link rel="enclosure" type="audio/mpeg" length="1337"
    href="http://example.org/audio/ph34r_my_podcast.mp3"/>
   <id>tag:example.org,2003:3.2397</id>
   <updated>2005-07-31T12:29:29Z</updated>
   <published>2003-12-13T08:29:29-04:00</published>
   <author>
     <name>Mark Pilgrim</name>
     <uri>http://example.org/</uri>
     <email>f8dy@example.com</email>
   </author>
   <contributor>
     <name>Sam Ruby</name>
   </contributor>
   <contributor>
     <name>Joe Gregorio</name>
   </contributor>
   <content type="xhtml" xml:lang="en"
    xml:base="http://diveintomark.org/">
     <div xmlns="http://www.w3.org/1999/xhtml">
       <p><i>[Update: The Atom draft is finished.]</i></p>
     </div>
   </content>
 </entry>
</feed>)mituboDelimiter";

void FeedParserTest::testSegmentation_data()
{
    QTest::addColumn<QByteArray>("feedData");
    /* This is an array determining how many bytes will be delivered on each
     * readyRead() signal. */
    QTest::addColumn<QVector<int>>("segments");

    QTest::addColumn<QString>("expectedTitle");
    QTest::addColumn<QString>("expectedDescription");
    QTest::addColumn<QDateTime>("expectedLastUpdate");
    QTest::addColumn<QUrl>("expectedLogoUrl");
    QTest::addColumn<QVector<FeedItem>>("expectedItems");

    const QVector<FeedItem> expectedExampleAtomItems = {
        FeedItem {
            "Atom draft-07 snapshot",
            "",
            Qt::PlainText,
            QDateTime::fromString("2003-12-13T08:29:29-04:00", Qt::ISODate),
            QUrl("http://example.org/2005/04/02/atom"),
            QUrl(),
        },
    };
    QTest::newRow("example Atom, three bytes") <<
        exampleAtom <<
        QVector<int>((exampleAtom.size() / 3) + 1, 3) <<
        "dive into mark" <<
        "\n   A <em>lot</em> of effort\n   went into making this effortless\n " <<
        QDateTime::fromString("2005-07-31T12:29:29Z", Qt::ISODate) <<
        QUrl() <<
        expectedExampleAtomItems;

    const QVector<FeedItem> expectedYouTubeAtomItems = {
        FeedItem {
            "Золотая молочка, 200$ за свободу, осенью по-новой",
            "помогите каналу \"Сталинград\":\nЯндекс-деньги: 4100110580591219",
            Qt::PlainText,
            QDateTime::fromString("2021-08-26T16:40:42Z", Qt::ISODate),
            QUrl("https://www.youtube.com/watch?v=JiYgOcMu0Uw"),
            QUrl("https://i3.ytimg.com/vi/JiYgOcMu0Uw/hqdefault.jpg"),
        },
        FeedItem {
            "Путинская подачка, инопаспорт для чиновника, экокатастрофа",
            "Подписывайтесь на наши каналы:\nНАПРАВЛЕНИЕ УДАРА",
            Qt::PlainText,
            QDateTime::fromString("2021-08-25T16:24:55Z", Qt::ISODate),
            QUrl("https://www.youtube.com/watch?v=9Wo4Y0YUNcU"),
            QUrl("https://i2.ytimg.com/vi/9Wo4Y0YUNcU/hqdefault.jpg"),
        },
    };
    QTest::newRow("youTube Atom, two bytes") <<
        youTubeAtom <<
        QVector<int>((youTubeAtom.size() / 2) + 1, 2) <<
        "Сталинград. XXI век" <<
        QString() <<
        QDateTime() <<
        QUrl() <<
        expectedYouTubeAtomItems;

    int posInMediaDescription = youTubeAtom.indexOf("</media:description>");
    QTest::newRow("youTube Atom, break after media description") <<
        youTubeAtom <<
        QVector<int>{
            posInMediaDescription,
            youTubeAtom.size() - posInMediaDescription
        } <<
        "Сталинград. XXI век" <<
        QString() <<
        QDateTime() <<
        QUrl() <<
        expectedYouTubeAtomItems;

    const QVector<FeedItem> expectedRssItems = {
        FeedItem {
            "Walter Ricciardi su La7",
            "descrizione media",
            Qt::PlainText,
            QDateTime::fromString("2021-08-24T09:08:57Z", Qt::ISODate),
            QUrl("https://peertube.it/w/4944-a288-33745b70f735"),
            QUrl("https://peertube.it/previews/f8f9db75600d.jpg"),
        },
        FeedItem {
            "Meluzzi sceglie di non vaccinarsi per finta",
            QString::fromUtf8("Meluzzi fa una scelta per l'umanità.\n"
                              "Una lezione di coerenza e di civilità."),
            Qt::RichText,
            QDateTime::fromString("2021-08-24T09:07:00Z", Qt::ISODate),
            QUrl("https://peertube.it/w/892f-34724e36cf82"),
            QUrl("https://peertube.it/previews/75871056f.jpg"),
        },
    };
    QTest::newRow("rss, 4k") <<
        peerTubeRss <<
        QVector<int>((peerTubeRss.size() / 4096) + 1, 4096) <<
        "TraTerra&Cielo" <<
        QString::fromUtf8("E' nato in città.") <<
        QDateTime::fromString("2021-08-24T16:03:24Z", Qt::ISODate) <<
        QUrl("https://peertube.it/client/assets/icons/icon-96x96.png") <<
        expectedRssItems;

    QTest::newRow("rss, four bytes") <<
        peerTubeRss <<
        QVector<int>((peerTubeRss.size() / 4) + 1, 4) <<
        "TraTerra&Cielo" <<
        QString::fromUtf8("E' nato in città.") <<
        QDateTime::fromString("2021-08-24T16:03:24Z", Qt::ISODate) <<
        QUrl("https://peertube.it/client/assets/icons/icon-96x96.png") <<
        expectedRssItems;

    QTest::newRow("rss, byte by byte") <<
        peerTubeRss <<
        QVector<int>(peerTubeRss.size(), 1) <<
        "TraTerra&Cielo" <<
        QString::fromUtf8("E' nato in città.") <<
        QDateTime::fromString("2021-08-24T16:03:24Z", Qt::ISODate) <<
        QUrl("https://peertube.it/client/assets/icons/icon-96x96.png") <<
        expectedRssItems;
}

void FeedParserTest::testSegmentation()
{
    QFETCH(QByteArray, feedData);
    QFETCH(QVector<int>, segments);

    QFETCH(QString, expectedTitle);
    QFETCH(QString, expectedDescription);
    QFETCH(QDateTime, expectedLastUpdate);
    QFETCH(QUrl, expectedLogoUrl);
    QFETCH(QVector<FeedItem>, expectedItems);

    QBuffer input;
    input.setData(feedData);
    QVERIFY(input.open(QIODevice::ReadOnly));

    int fds[2];
    int ret = pipe(fds);
    QCOMPARE(ret, 0);

    QLocalSocket writeSocket;
    writeSocket.setSocketDescriptor(fds[1], QLocalSocket::ConnectedState, QIODevice::WriteOnly);

    QLocalSocket network;
    network.setSocketDescriptor(fds[0], QLocalSocket::ConnectedState, QIODevice::ReadOnly);

    FeedParser parser(&network);
    QSignalSpy gotTitle(&parser, &FeedParser::gotTitle);
    QSignalSpy gotDescription(&parser, &FeedParser::gotDescription);
    QSignalSpy gotLastUpdate(&parser, &FeedParser::gotLastUpdate);
    QSignalSpy gotLogoUrl(&parser, &FeedParser::gotLogoUrl);
    QSignalSpy gotItem(&parser, &FeedParser::gotItem);

    for (int segmentSize: segments) {
        QByteArray data = input.read(segmentSize);
        writeSocket.write(data);
        // need to iterate the event loop
        QCoreApplication::instance()->processEvents();
        // For some reason, the above is not enough for the 4k case...
        QCoreApplication::instance()->processEvents();
    }

    QCOMPARE(gotTitle.count(), 1);
    QCOMPARE(gotTitle.at(0).at(0).toString(), expectedTitle);

    if (!expectedDescription.isEmpty()) {
        QCOMPARE(gotDescription.count(), 1);
        QCOMPARE(gotDescription.at(0).at(0).toString(), expectedDescription);
    } else {
        QCOMPARE(gotDescription.count(), 0);
    }

    if (expectedLastUpdate.isValid()) {
        QCOMPARE(gotLastUpdate.count(), 1);
        QCOMPARE(gotLastUpdate.at(0).at(0).toDateTime(), expectedLastUpdate);
    } else {
        QCOMPARE(gotLastUpdate.count(), 0);
    }

    if (!expectedLogoUrl.isEmpty()) {
        QCOMPARE(gotLogoUrl.count(), 1);
        QCOMPARE(gotLogoUrl.at(0).at(0).toUrl(), expectedLogoUrl);
    } else {
        QCOMPARE(gotLogoUrl.count(), 0);
    }

    QCOMPARE(gotItem.count(), expectedItems.count());
    for (int i = 0; i < expectedItems.count(); i++) {
        const FeedItem item = gotItem.at(i).at(0).value<FeedItem>();
        QCOMPARE(item, expectedItems[i]);
    }
}

QTEST_GUILESS_MAIN(FeedParserTest)

#include "tst_feed_parser.moc"
