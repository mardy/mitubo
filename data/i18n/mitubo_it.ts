<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="ia">
<context>
    <name>AboutPopup</name>
    <message>
        <location filename="../../src/desktop/qml/AboutPopup.qml" line="42"/>
        <source>MiTubo version %1&lt;br/&gt;&lt;font size=&quot;1&quot;&gt;Copyright ⓒ 2020-2022 mardy.it&lt;/font&gt;</source>
        <translation>MiTubo versione %1&lt;br/&gt;&lt;font size=&quot;1&quot;&gt;Copyright ⓒ 2020-2022 mardy.it&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/AboutPopup.qml" line="50"/>
        <source>MiTubo is free software, distributed under the &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;GPL-v3 license&lt;/a&gt;. Among other things, this means that you can share it with your friends for free, or resell it for a fee.</source>
        <translation>MiTubo è software libero, distribuito con la &lt;a href=&quot;https://www.gnu.org/licenses/gpl-3.0.en.html&quot;&gt;licenza GPL-v3&lt;/a&gt;. Tra le altre cose, questo significa che può essere liberamente distribuito gratuitamente agli amici o rivenduto dietro compenso.</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/AboutPopup.qml" line="61"/>
        <source>If you got MiTubo for free, please consider making a small donation to its author:&lt;br/&gt;&lt;ul&gt;&lt;li&gt;Paypal: &lt;a href=&quot;https://www.paypal.com/donate/?hosted_button_id=P7YTMKGGCD9JC&quot;&gt;click here to donate a free amount&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;a href=&quot;https://money.yandex.ru/quickpay/shop-widget?writer=seller&amp;targets=MiTubo&amp;targets-hint=&amp;default-sum=&amp;button-text=14&amp;hint=&amp;successURL=&amp;quickpay=shop&amp;account=410015419471966&quot;&gt;Yandex Money&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>Se hai ricevuto MiTubo gratuitamente, prendi in considerazione l&apos;idea di versare una piccola donazione al suo autore:&lt;br/&gt;&lt;ul&gt;&lt;li&gt;Paypal: &lt;a href=&quot;https://www.paypal.com/donate/?hosted_button_id=P7YTMKGGCD9JC&quot;&gt;scegli tu l&apos;ammontare della donazione&lt;/a&gt;&lt;/li&gt;&lt;li&gt;&lt;a href=&quot;https://money.yandex.ru/quickpay/shop-widget?writer=seller&amp;targets=MiTubo&amp;targets-hint=&amp;default-sum=&amp;button-text=14&amp;hint=&amp;successURL=&amp;quickpay=shop&amp;account=410015419471966&quot;&gt;Yandex Money&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
</context>
<context>
    <name>BackButton</name>
    <message>
        <location filename="../../src/desktop/qml/BackButton.qml" line="5"/>
        <source>❮</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DeleteButton</name>
    <message>
        <location filename="../../src/desktop/qml/DeleteButton.qml" line="4"/>
        <location filename="../../src/ubuntu-touch/qml/DeleteButton.qml" line="9"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>DownloadItem</name>
    <message>
        <location filename="../../src/desktop/qml/DownloadItem.qml" line="28"/>
        <source>&lt;a href=&quot;%1&quot;&gt;Open file&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Apri il file&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadItem.qml" line="41"/>
        <source>🗙 Cancel</source>
        <translation>🗙 Annulla</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadItem.qml" line="60"/>
        <source>S: %1</source>
        <translation>D: %1</translation>
    </message>
</context>
<context>
    <name>DownloadPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="14"/>
        <source>Download media file</source>
        <translation>Scaricamento del file multimediale</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="35"/>
        <source>Media format selection</source>
        <translation>Selezione del formato</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="42"/>
        <source>Download streams</source>
        <translation>Flussi da scaricare</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="53"/>
        <source>Format</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadPage.qml" line="68"/>
        <source>Download</source>
        <translation>Avviare lo scaricamento</translation>
    </message>
</context>
<context>
    <name>DownloaderErrorPopup</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderErrorPopup.qml" line="10"/>
        <source>Error opening video</source>
        <translation>Errore nell&apos;aprire il video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderErrorPopup.qml" line="23"/>
        <source>There was an error retrieving the video information:</source>
        <translation>Si è verificato un errore durante la raccolta delle informazioni sul video:</translation>
    </message>
</context>
<context>
    <name>DownloaderInstallationPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="11"/>
        <source>youtube-dl installation</source>
        <translation>Installazione di youtube-dl</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="17"/>
        <source>Checking for updates…</source>
        <translation>Controllo degli aggiornamenti…</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="22"/>
        <source>No download available</source>
        <translation>Nessun aggiornamento disponibile</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="28"/>
        <source>Failed. Please check your network connection.</source>
        <translation>Errore. Verificare di essere connessi ad internet.</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="33"/>
        <source>Downloading…</source>
        <translation>Scaricamento in corso…</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="39"/>
        <source>Download failed. Please check your network connection.</source>
        <translation>Scaricamento fallito. Verificare la propria connessione ad internet.</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="45"/>
        <source>Installing…</source>
        <translation>Installazione in corso…</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="50"/>
        <source>Installation complete!</source>
        <translation>Installazione completata!</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="56"/>
        <source>Installation failed</source>
        <translation>Installazione fallita</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderInstallationPage.qml" line="83"/>
        <source>Done</source>
        <translation>Torna al programma</translation>
    </message>
</context>
<context>
    <name>DownloaderIntroPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderIntroPage.qml" line="10"/>
        <source>youtube-dl installation</source>
        <translation>Installazione di youtube-dl</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderIntroPage.qml" line="20"/>
        <source>&lt;p&gt;MiTubo relies on the &lt;b&gt;youtube-dl&lt;/b&gt; program to perform the extraction of the video streams.&lt;/p&gt;&lt;p&gt;A working installation of &lt;b&gt;youtube-dl&lt;/b&gt; could not be found on your system. MiTubo will now start the installation process.</source>
        <translation>&lt;p&gt;MiTubo utilizza il programma &lt;b&gt;youtube-dl&lt;/b&gt; per estrarre le informazioni sui flussi multimediali.&lt;/p&gt;&lt;p&gt;Non è stata rilevata un&apos;installazione di &lt;b&gt;youtube-dl&lt;/b&gt; nel sistema; MiTubo procederà ora all&apos;installazione di questo componente.</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderIntroPage.qml" line="30"/>
        <source>Install youtube-dl</source>
        <translation>Installare youtube-dl</translation>
    </message>
</context>
<context>
    <name>DownloaderMaintenance</name>
    <message>
        <location filename="../../src/desktop/qml/DownloaderMaintenance.qml" line="25"/>
        <source>A new version (%1) of &lt;b&gt;%2&lt;/b&gt; is available; &lt;a href=&quot;foo&quot;&gt;click here&lt;/a&gt; to install it.</source>
        <translation>Una nuova versione (%1) di &lt;b&gt;%2&lt;/b&gt; è disponibile; &lt;a href=&quot;foo&quot;&gt;cliccare qui&lt;/a&gt; per installarla automaticamente.</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloaderMaintenance.qml" line="33"/>
        <source>Hide</source>
        <translation>Nascondi</translation>
    </message>
</context>
<context>
    <name>DownloadsPage</name>
    <message>
        <location filename="../../src/desktop/qml/DownloadsPage.qml" line="11"/>
        <source>Active downloads</source>
        <translation>Scaricamenti attivi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadsPage.qml" line="29"/>
        <source>Open &lt;a href=&quot;%1&quot;&gt;MiTubo&apos;s download folder&lt;/a&gt; to see all past downloads</source>
        <translation>Aprire la &lt;a href=&quot;%1&quot;&gt;cartella degli scaricamenti di MiTubo&lt;/a&gt; per vedere tutti gli scaricamenti passati</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/DownloadsPage.qml" line="44"/>
        <source>No active downloads</source>
        <translation>Nessun scaricamento attivo</translation>
    </message>
</context>
<context>
    <name>FeedContentsOverlay</name>
    <message>
        <location filename="../../src/desktop/qml/FeedContentsOverlay.qml" line="47"/>
        <source>View more items…</source>
        <translation>Vedi gli altri video…</translation>
    </message>
</context>
<context>
    <name>FeedDelegate</name>
    <message>
        <location filename="../../src/desktop/qml/FeedDelegate.qml" line="24"/>
        <source>Unsubscribe</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>FeedPage</name>
    <message>
        <location filename="../../src/desktop/qml/FeedPage.qml" line="17"/>
        <source>RSS feed</source>
        <translation>Flusso RSS</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FeedPage.qml" line="25"/>
        <source>Subscribed</source>
        <translation>Stai seguendo</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FeedPage.qml" line="25"/>
        <source>Subscribe</source>
        <translation>Inizia a seguire</translation>
    </message>
</context>
<context>
    <name>FormatModels</name>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="36"/>
        <source>%1 (bitrate: %2)</source>
        <translation>%1 (bitrate: %2)</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="49"/>
        <source>Audio + video</source>
        <translation>Audio + video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="53"/>
        <source>Audio only</source>
        <translation>Solo audio</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/FormatModels.qml" line="57"/>
        <source>Video only</source>
        <translation>Solo video</translation>
    </message>
</context>
<context>
    <name>InvidiousSearch</name>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/InvidiousSearch.qml" line="108"/>
        <source>%n year(s) ago</source>
        <translation>
            <numerusform>%n anno fa</numerusform>
            <numerusform>%n anni fa</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/InvidiousSearch.qml" line="110"/>
        <source>%n month(s) ago</source>
        <translation>
            <numerusform>%n mese fa</numerusform>
            <numerusform>%n mesi fa</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>InvidiousSearchOptions</name>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="28"/>
        <source>Type:</source>
        <translation>Tipo:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="35"/>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="36"/>
        <source>Channels</source>
        <translation>Canali</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="44"/>
        <source>Published:</source>
        <translation>Pubblicato:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="52"/>
        <source>Any time</source>
        <translation>In qualsiasi data</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="53"/>
        <source>Last hour</source>
        <translation>Nell&apos;ultima ora</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="54"/>
        <source>Today</source>
        <translation>Oggi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="55"/>
        <source>Less than a week ago</source>
        <translation>Meno di una settimana fa</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="56"/>
        <source>Less than a month ago</source>
        <translation>Meno di un mese fa</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="57"/>
        <source>Less than one year ago</source>
        <translation>Meno di un anno fa</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="64"/>
        <source>Duration:</source>
        <translation>Durata:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="71"/>
        <source>Any</source>
        <translation>Qualsiasi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="72"/>
        <source>Short</source>
        <translation>Breve</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="73"/>
        <source>Long</source>
        <translation>Lungo</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="80"/>
        <source>Sort by:</source>
        <translation>Ordina per:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="87"/>
        <source>Relevance</source>
        <translation>Rilevanza</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="88"/>
        <source>Upload date</source>
        <translation>Data di caricamento</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="89"/>
        <source>Rating</source>
        <translation>Punteggio</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/InvidiousSearchOptions.qml" line="90"/>
        <source>View count</source>
        <translation>Visualizzazioni</translation>
    </message>
</context>
<context>
    <name>LinkDropPopup</name>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="20"/>
        <source>Link action:</source>
        <translation>Azione sul link:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="41"/>
        <source>Play now</source>
        <translation>Riproduci ora</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="48"/>
        <source>Add to “Watch later” (first)</source>
        <translation>Aggiungi alla lista “Guarda dopo” (in alto)</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/LinkDropPopup.qml" line="54"/>
        <source>Add to “Watch later” (last)</source>
        <translation>Accoda alla lista “Guarda dopo”</translation>
    </message>
</context>
<context>
    <name>MainMenu</name>
    <message>
        <location filename="../../src/desktop/qml/MainMenu.qml" line="14"/>
        <source>Downloads…</source>
        <translation>Scaricamenti…</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainMenu.qml" line="23"/>
        <source>About MiTubo…</source>
        <translation>Informazioni su MiTubo…</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="20"/>
        <source>Video selection</source>
        <translation>Selezione del video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="49"/>
        <source>Enter some search terms or the address of the page containing the desired video</source>
        <translation>Inserire alcuni termini di ricerca o l&apos;indirizzo della pagina contenente il video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="58"/>
        <source>Video address</source>
        <translation>Indirizzo del video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="76"/>
        <source>Add subscription</source>
        <translation>Aggiungere ai seguiti</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="76"/>
        <source>Play</source>
        <translation>Riproduci</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="76"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="82"/>
        <source>Show info</source>
        <translation>Mostra informazioni</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/MainPage.qml" line="92"/>
        <source>RSS feed(s) detected</source>
        <translation>
            <numerusform>Trovato un flusso RSS</numerusform>
            <numerusform>Trovati %n flussi RSS</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="100"/>
        <source>The following RSS feeds were found in the webpage:</source>
        <translation>I seguenti flussi RSS sono stati trovati nella pagina:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="121"/>
        <source>Subscribe</source>
        <translation>Inizia a seguire</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="121"/>
        <source>Subscribed</source>
        <translation>Stai seguendo</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="137"/>
        <source>or</source>
        <translation>oppure</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="144"/>
        <source>Go to your playlists</source>
        <translation>Apri le tue liste</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="154"/>
        <source>Your subscriptions</source>
        <translation>Le tue sottoscrizioni</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/MainPage.qml" line="175"/>
        <source>You have no subscriptions; enter an RSS address in the search box above to add one.</source>
        <translation>Nessuna sottoscrizione; scrivere l&apos;indirizzo di un flusso RSS nel campo di ricarca per aggiungerlo.</translation>
    </message>
</context>
<context>
    <name>Media</name>
    <message>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="8"/>
        <location filename="../../src/ubuntu-touch/qml/BufferingLabel.qml" line="8"/>
        <source>Loading</source>
        <translation>Caricamento</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="9"/>
        <source>Loaded</source>
        <translation>Caricato</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="10"/>
        <location filename="../../src/desktop/qml/BufferingLabel.qml" line="11"/>
        <location filename="../../src/ubuntu-touch/qml/BufferingLabel.qml" line="9"/>
        <location filename="../../src/ubuntu-touch/qml/BufferingLabel.qml" line="10"/>
        <source>Buffering</source>
        <translation>Buffering</translation>
    </message>
</context>
<context>
    <name>MiTubo::Playlist</name>
    <message>
        <location filename="../../src/playlist.cpp" line="358"/>
        <source>ContinueWatching</source>
        <translation>Continua a guardare</translation>
    </message>
    <message>
        <location filename="../../src/playlist.cpp" line="362"/>
        <source>WatchHistory</source>
        <translation>Registro delle visualizzazioni</translation>
    </message>
    <message>
        <location filename="../../src/playlist.cpp" line="366"/>
        <source>WatchLater</source>
        <translation>Guarda dopo</translation>
    </message>
</context>
<context>
    <name>PlayerErrorPopup</name>
    <message>
        <location filename="../../src/desktop/qml/PlayerErrorPopup.qml" line="11"/>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="12"/>
        <source>Playback error</source>
        <translation>Errore di riproduzione</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlayerErrorPopup.qml" line="23"/>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="27"/>
        <source>An error occurred during media playback (%1): %2</source>
        <translation>Si è verificato un errore durante la riproduzione (%1): %2</translation>
    </message>
    <message>
        <location filename="../../src/desktop/windows/qml/PlayerErrorPopup.qml" line="37"/>
        <source>&lt;p&gt;An error occurred during media playback; this is likely caused by missing codecs.&lt;/p&gt;&lt;p&gt;Please consider installing additional video codecs from one of these sources:&lt;/p&gt;&lt;ul&gt; &lt;li&gt;&lt;a href=&quot;https://www.codecguide.com/download_kl.htm&quot;&gt;K-lite codecs&lt;/a&gt; &lt;li&gt;&lt;a href=&quot;https://github.com/Nevcairiel/LAVFilters/releases&quot;&gt;LAV filters&lt;/a&gt;&lt;/ul&gt;&lt;p&gt;Once these codecs have been installed, restart MiTubo and trying playing the media again.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Si è verificato un errore durante la riproduzione; probabilmente sono assenti dei codec.&lt;/p&gt;&lt;p&gt;Si consiglia di installare i codec video addizionali disponibili a uno di questi indirizzi:&lt;/p&gt;&lt;ul&gt; &lt;li&gt;&lt;a href=&quot;https://www.codecguide.com/download_kl.htm&quot;&gt;Codecs K-lite&lt;/a&gt; &lt;li&gt;&lt;a href=&quot;https://github.com/Nevcairiel/LAVFilters/releases&quot;&gt;Filtri LAV&lt;/a&gt;&lt;/ul&gt;&lt;p&gt;Una volta installati questi codec, riavviare MiTubo e riprovare a riprodurre il  file multimediale.&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>PlaylistItemDelegate</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemDelegate.qml" line="150"/>
        <source>Play</source>
        <translation>Riproduci</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemDelegate.qml" line="159"/>
        <source>More…</source>
        <translation>Altro…</translation>
    </message>
</context>
<context>
    <name>PlaylistItemNew</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemNew.qml" line="9"/>
        <source>New list...</source>
        <translation>Nuova lista…</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemNew.qml" line="16"/>
        <source>Create a new playlist</source>
        <translation>Creare una nuova playlist</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistItemNew.qml" line="25"/>
        <source>Enter the list name, e.g. &quot;Watch later&quot;</source>
        <translation>Scrivere il nome della lista, p.e. “Guarda dopo”</translation>
    </message>
</context>
<context>
    <name>PlaylistPage</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistPage.qml" line="15"/>
        <source>Playlist %1</source>
        <translation>Lista %1</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistPage.qml" line="55"/>
        <source>Newer first</source>
        <translation>Prima i più recenti</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/PlaylistPage.qml" line="56"/>
        <source>Older first</source>
        <translation>Prima i più vecchi</translation>
    </message>
</context>
<context>
    <name>PlaylistsPage</name>
    <message>
        <location filename="../../src/desktop/qml/PlaylistsPage.qml" line="15"/>
        <source>Playlists</source>
        <translation>Liste di riproduzione</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/PlaylistsPage.qml" line="27"/>
        <source>%n elements</source>
        <translation>
            <numerusform>%n elemento</numerusform>
            <numerusform>%n elementi</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>SearchChannelDelegate</name>
    <message>
        <location filename="../../src/desktop/qml/SearchChannelDelegate.qml" line="54"/>
        <source>Subscribe</source>
        <translation>Inizia a seguire</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchChannelDelegate.qml" line="62"/>
        <source>Visit site</source>
        <translation>Visita il sito</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="14"/>
        <source>Search</source>
        <translation>Ricerca</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="25"/>
        <source>Search:</source>
        <translation>Cerca:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="31"/>
        <source>Enter search terms</source>
        <translation>Scrivere i termini da ricercare</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/SearchPage.qml" line="51"/>
        <source>⚙</source>
        <translation>⚙</translation>
    </message>
</context>
<context>
    <name>TextFieldWithContextMenu</name>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="16"/>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="22"/>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="28"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="34"/>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/TextFieldWithContextMenu.qml" line="42"/>
        <source>Select all</source>
        <translation>Seleziona tutto</translation>
    </message>
</context>
<context>
    <name>UnsupportedCodecPopup</name>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="13"/>
        <source>Unsupported media codec</source>
        <translation>Codec multimediale non supportato</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="30"/>
        <source>Some or all of the media streams are using an unsupported codec. You can blacklist the codec used in the unplayable stream so that MiTubo will never try to play similar streams again.</source>
        <translation>Alcuni dei flussi audio usano un coded non supportato. È possibile registrare il codec come inutilizzabile per far sì che MiTubo in futuro ignori i flussi generati con questo codec.</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="42"/>
        <source>&lt;b&gt;Video stream&lt;/b&gt;&lt;br&gt;Codec: %1</source>
        <translation>&lt;b&gt;Flusso video&lt;/b&gt;&lt;br&gt;Codec: %1</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="62"/>
        <source>&lt;b&gt;Audio stream&lt;/b&gt;&lt;br&gt;Codec: %1</source>
        <translation>&lt;b&gt;Flusso audio&lt;/b&gt;&lt;br&gt;Codec: %1</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="51"/>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="71"/>
        <source>Blacklisted</source>
        <translation>Escluso</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="52"/>
        <source>Blacklist video codec</source>
        <translation>Escludi questo codec</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/UnsupportedCodecPopup.qml" line="72"/>
        <source>Blacklist audio codec</source>
        <translation>Escludi questo codec</translation>
    </message>
</context>
<context>
    <name>VideoBottomRow</name>
    <message>
        <location filename="../../src/desktop/qml/VideoBottomRow.qml" line="59"/>
        <source>%1 / %2</source>
        <translation>%1 / %2</translation>
    </message>
</context>
<context>
    <name>VideoInfoPage</name>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="15"/>
        <source>Video details</source>
        <translation>Informazioni sul video</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="45"/>
        <source>Failed to retrieve title</source>
        <translation>Titolo del video non disponibile</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="77"/>
        <source>Play</source>
        <translation>Riproduci</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="85"/>
        <source>Watch later</source>
        <translation>Guarda più tardi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="92"/>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="119"/>
        <source>By &lt;b&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/b&gt;</source>
        <translation>Di &lt;b&gt;&lt;a href=&quot;%1&quot;&gt;%2&lt;/a&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="120"/>
        <source>By &lt;b&gt;%1&lt;/b&gt;</source>
        <translation>Di &lt;b&gt;%1&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="128"/>
        <source>Duration: %1</source>
        <translation>Durata: %1</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="135"/>
        <source>Uploaded on %1</source>
        <translation>Caricato il %1</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="136"/>
        <source>Upload date unknown</source>
        <translation>Data di caricamento sconosciuta</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="143"/>
        <source>%n views</source>
        <translation>
            <numerusform>%n visualizzazione</numerusform>
            <numerusform>%n visualizzazioni</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="144"/>
        <source>View count unknown</source>
        <translation>Numero di visualizzazioni ignoto</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="152"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoInfoPage.qml" line="166"/>
        <source>&lt;a href=&quot;%1&quot;&gt;View web page in browser&lt;/a&gt;</source>
        <translation>&lt;a href=&quot;%1&quot;&gt;Apri la pagina nel browser&lt;/a&gt;</translation>
    </message>
</context>
<context>
    <name>VideoSettingsPanel</name>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="15"/>
        <source>Half speed</source>
        <translation>Velocità dimezzata</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="16"/>
        <source>Normal speed</source>
        <translation>Velocità normale</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="17"/>
        <source>1.25x</source>
        <translation>1,25x</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="18"/>
        <source>1.5x</source>
        <translation>1,5x</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="19"/>
        <source>Double speed</source>
        <translation>Velocità doppia</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="20"/>
        <source>Quadruple speed</source>
        <translation>Velocità quadrupla</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="31"/>
        <source>Streams</source>
        <translation>Flussi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="43"/>
        <source>Format</source>
        <translation>Formato</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/VideoSettingsPanel.qml" line="56"/>
        <source>Playback speed</source>
        <translation>Velocità di riproduzione</translation>
    </message>
</context>
<context>
    <name>WatchLaterPanel</name>
    <message>
        <location filename="../../src/desktop/qml/WatchLaterPanel.qml" line="20"/>
        <source>Add to playlist:</source>
        <translation>Aggiungere alla lista:</translation>
    </message>
    <message numerus="yes">
        <location filename="../../src/desktop/qml/WatchLaterPanel.qml" line="36"/>
        <source>%n elements</source>
        <translation>
            <numerusform>%n elemento</numerusform>
            <numerusform>%n elementi</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>YandexSearchOptions</name>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="26"/>
        <source>Published:</source>
        <translation>Pubblicato:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="34"/>
        <source>Any time</source>
        <translation>In qualsiasi data</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="35"/>
        <source>Today</source>
        <translation>Oggi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="36"/>
        <source>Less than a week ago</source>
        <translation>Meno di una settimana fa</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="37"/>
        <source>Less than a month ago</source>
        <translation>Meno di un mese fa</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="38"/>
        <source>Less than one year ago</source>
        <translation>Meno di un anno fa</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="45"/>
        <source>Duration:</source>
        <translation>Durata:</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="52"/>
        <source>Any</source>
        <translation>Qualsiasi</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="53"/>
        <source>Less than 10 minutes</source>
        <translation>Meno di 10 minuti</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="54"/>
        <source>10-65 minutes</source>
        <translation>10-65 minuti</translation>
    </message>
    <message>
        <location filename="../../src/desktop/qml/YandexSearchOptions.qml" line="55"/>
        <source>More than 65 minutes</source>
        <translation>Più di 65 minuti</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../../src/desktop/qml/main.qml" line="92"/>
        <source>Retrieving stream information from the web page…</source>
        <translation>Estrazione delle informazioni dalla pagina web…</translation>
    </message>
</context>
</TS>
