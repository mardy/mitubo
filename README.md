[![pipeline status](https://gitlab.com/mardy/mitubo/badges/master/pipeline.svg)](https://gitlab.com/mardy/mitubo/-/commits/master)
[![coverage report](https://gitlab.com/mardy/mitubo/badges/master/coverage.svg)](https://gitlab.com/mardy/mitubo/-/commits/master)


# MiTubo

MiTubo is a desktop and mobile application to playback videos from RSS feeds
and from major video platforms.

Features include:

- Video playback from web URL
- Video info extracted via [youtube-dl](https://youtube-dl.org/) or
  [yt-dlp](https://github.com/yt-dlp/yt-dlp) (automatically installed as needed)
- Drag&drop URLs from other applications
- Search: Yandex video and YouTube (via [Invidious API](https://invidious.io/))
- Playlists
- Watch history
- “Watch later” playlist
- “Continue watching” playlist, automatically updated
- Selection of playback quality

The program is under heavy development, so expect this list to expand in the
future!


## Installation

At the moment, MiTubo is available for the Linux desktop
([PPA](https://launchpad.net/~mardy/+archive/ubuntu/mitubo) for Ubuntu,
[AppImage](https://gitlab.com/mardy/mitubo/-/tags) for all other distributions)
and for the [Ubuntu Touch](https://ubuntu-touch.io/) mobile platform (get if
from the [OpenStore](https://open-store.io/app/it.mardy.mitubo)!). The
application should be easily portable to more desktop and mobile platforms; if
you are interested, get in touch!


## Contribute

I do welcome testers, translators (even though I'm not sure that the desktop
version of the app is ready for translations) and of course code contributions.
Donations are also welcome, either via
[Paypal](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NQ6NU5FEB9JLY)
or [Yandex](https://yoomoney.ru/quickpay/shop-widget?writer=buyer&targets=&targets-hint=MiTubo%20development&default-sum=&button-text=14&payment-type-choice=on&hint=&successURL=&quickpay=shop&account=410015419471966).
