/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "playlist_model.h"

#include "playlist.h"
#include "playlist_store.h"

#include <QDebug>
#include <QQmlEngine>

using namespace MiTubo;

namespace MiTubo {

class PlaylistModelPrivate {
public:
    PlaylistModelPrivate(PlaylistModel *q);

    void onPlaylistsChanged();

private:
    Q_DECLARE_PUBLIC(PlaylistModel)
    QList<QSharedPointer<Playlist>> m_playlists;
    QHash<int, QByteArray> m_roles;
    PlaylistModel *q_ptr;
};

} // namespace

PlaylistModelPrivate::PlaylistModelPrivate(PlaylistModel *q):
    q_ptr(q)
{
    m_roles[PlaylistModel::UniqueId] = "uniqueId";
    m_roles[PlaylistModel::NameRole] = "name";
    m_roles[PlaylistModel::PlaylistRole] = "playlist";
    m_roles[PlaylistModel::CountRole] = "count";

    const PlaylistStore *store = PlaylistStore::instance();
    m_playlists = store->playlists();
    QObject::connect(store, &PlaylistStore::playlistsChanged,
                     q, [this]() { onPlaylistsChanged(); });
}

void PlaylistModelPrivate::onPlaylistsChanged()
{
    Q_Q(PlaylistModel);

    q->beginResetModel();
    m_playlists = PlaylistStore::instance()->playlists();
    q->endResetModel();
}

PlaylistModel::PlaylistModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new PlaylistModelPrivate(this))
{
    qRegisterMetaType<Playlist*>();
    QObject::connect(this, &QAbstractListModel::modelReset,
                     this, &PlaylistModel::countChanged);
    QObject::connect(this, &QAbstractListModel::rowsInserted,
                     this, &PlaylistModel::countChanged);
    QObject::connect(this, &QAbstractListModel::rowsRemoved,
                     this, &PlaylistModel::countChanged);
}

PlaylistModel::~PlaylistModel() = default;

QString PlaylistModel::continueWatchingListId() const
{
    return Playlist::continueWatchingListId();
}

QString PlaylistModel::watchHistoryListId() const
{
    return Playlist::watchHistoryListId();
}

QString PlaylistModel::watchLaterListId() const
{
    return Playlist::watchLaterListId();
}

QVariant PlaylistModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

Playlist *PlaylistModel::playlistById(const QString &id) const
{
    Q_D(const PlaylistModel);

    for (const auto &p: d->m_playlists) {
        if (p->uniqueId() == id) {
            QQmlEngine::setObjectOwnership(p.data(), QQmlEngine::CppOwnership);
            return p.data();
        }
    }
    return nullptr;
}

Playlist *PlaylistModel::playlistFromDownloader(const QJsonObject &jsonData)
{
    return Playlist::fromDownloader(jsonData);
}

Playlist *PlaylistModel::createPlaylist(const QString &name,
                                        const QJsonArray &items)
{
    Playlist *playlist = new Playlist(name, items);
    PlaylistStore::instance()->insertPlaylist(playlist);
    return playlist;
}

void PlaylistModel::deletePlaylist(int row)
{
    Q_D(PlaylistModel);
    if (Q_UNLIKELY(row < 0 || row >= rowCount())) {
        qWarning() << "Invalid row requested:" << row;
        return;
    }

    PlaylistStore::instance()->deletePlaylist(d->m_playlists[row]->name());
}

QVariant PlaylistModel::data(const QModelIndex &index, int role) const
{
    Q_D(const PlaylistModel);

    Q_ASSERT(index.column() == 0);

    int row = index.row();
    if (Q_UNLIKELY(row < 0 || row >= rowCount())) {
        qWarning() << "Invalid index requested:" << row;
        return QVariant();
    }

    const QSharedPointer<Playlist> &playlist = d->m_playlists[row];
    switch (role) {
    case UniqueId:
        return playlist->uniqueId();
    case NameRole:
        return playlist->name();
    case PlaylistRole:
        return QVariant::fromValue(playlist.data());
    case CountRole:
        return playlist->count();
    default:
        return QVariant();
    }
}

int PlaylistModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const PlaylistModel);
    Q_ASSERT(!parent.isValid());
    return d->m_playlists.count();
}

QHash<int, QByteArray> PlaylistModel::roleNames() const
{
    Q_D(const PlaylistModel);
    return d->m_roles;
}
