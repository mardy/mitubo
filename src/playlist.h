/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_PLAYLIST_H
#define MITUBO_PLAYLIST_H

#include <QJsonArray>
#include <QJsonObject>
#include <QObject>
#include <QScopedPointer>
#include <QString>

class QStringList;
class QUrl;

namespace MiTubo {

class PlaylistPrivate;
class Playlist: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString uniqueId READ uniqueId CONSTANT)
    Q_PROPERTY(bool editable READ isEditable CONSTANT)
    Q_PROPERTY(SortOrder sortOrder READ sortOrder WRITE setSortOrder
               NOTIFY itemsChanged)
    Q_PROPERTY(QJsonArray items READ items NOTIFY itemsChanged)
    Q_PROPERTY(int count READ count NOTIFY itemsChanged)

public:
    enum SortOrder {
        OlderFirst,
        NewerFirst,
    };
    Q_ENUM(SortOrder)

    Playlist(const QString &name, const QJsonArray &items,
             bool isEditable = true, QObject *parent = nullptr);
    Playlist(const QString &uniqueId, const QString &name, SortOrder order,
             const QJsonArray &items,
             QObject *parent = nullptr);
    static Playlist *fromDownloader(const QJsonObject &playlistData,
                                    QObject *parent = nullptr);
    ~Playlist();

    QString name() const;

    QString uniqueId() const;

    bool isEditable() const;

    void setSortOrder(SortOrder order);
    SortOrder sortOrder() const;

    QJsonArray items() const;
    int count() const;

    Q_INVOKABLE void addVideo(const QUrl &url, qint64 position = 0);
    Q_INVOKABLE void addVideo(const QJsonObject &videoData,
                              qint64 position = 0);
    Q_INVOKABLE void updateVideo(const QJsonObject &videoData);
    Q_INVOKABLE void addVideos(const QJsonArray &videos, int index = 0);
    Q_INVOKABLE void addVideos(const QStringList &urls, int index = 0);

    Q_INVOKABLE int indexOf(const QJsonObject &videoData) const;
    Q_INVOKABLE int indexOf(const QUrl &url) const;
    Q_INVOKABLE QJsonObject itemAt(int index) const;
    Q_INVOKABLE QJsonObject takeAt(int index);
    Q_INVOKABLE QJsonObject takeFirst() { return takeAt(0); }

    static QString continueWatchingListId();
    static QString watchHistoryListId();
    static QString watchLaterListId();

Q_SIGNALS:
    void itemsChanged();

private:
    Q_DECLARE_PRIVATE(Playlist)
    QScopedPointer<PlaylistPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_PLAYLIST_H
