/*
 * Copyright (C) 2022-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "video_extractor.h"

#include "video_detector.h"
#include "youtube_dl.h"

#include <QDebug>
#include <QJSEngine>
#include <QJSValue>
#include <QJsonArray>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QQueue>
#include <QUrl>

using namespace MiTubo;

namespace MiTubo {

struct InfoRequest {
    QUrl url;
    QJSValue callback;
};

class VideoExtractorPrivate
{
public:
    VideoExtractorPrivate(VideoExtractor *q);

    void processNext();
    void runVideoDetector(const QUrl &url);
    void runYoutubeDl(const QUrl &url);
    void addUrlAlias(QJsonObject &response, const QUrl &url);
    void addResponse(const QJsonObject &response);
    bool isBusy() const { return !m_currentInfoRequest.url.isEmpty(); }

    void queueGetUrlInfo(const InfoRequest &request);

private:
    Q_DECLARE_PUBLIC(VideoExtractor)
    YoutubeDl *m_youtubeDl;
    VideoDetector m_detector;
    QNetworkAccessManager m_nam;
    QJsonObject m_errorResponse;
    QJsonArray m_videosFound;
    QJsonObject m_playlistResponse;
    int m_pendingRequests;
    InfoRequest m_currentInfoRequest;
    QQueue<InfoRequest> m_infoRequests;
    VideoExtractor *q_ptr;
};

} // namespace

VideoExtractorPrivate::VideoExtractorPrivate(VideoExtractor *q):
    m_youtubeDl(nullptr),
    m_pendingRequests(0),
    q_ptr(q)
{
}

void VideoExtractorPrivate::processNext()
{
    m_errorResponse = QJsonObject();
    m_playlistResponse = QJsonObject();
    m_videosFound = QJsonArray();
    m_pendingRequests = 0;
    if (m_infoRequests.isEmpty()) {
        m_currentInfoRequest = {};
    } else {
        m_currentInfoRequest = m_infoRequests.dequeue();
        runVideoDetector(m_currentInfoRequest.url);
        runYoutubeDl(m_currentInfoRequest.url);
    }
}

void VideoExtractorPrivate::runVideoDetector(const QUrl &url)
{
    Q_Q(VideoExtractor);

    m_pendingRequests++;

    m_detector.reset();
    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);
    QNetworkReply *reply = m_nam.get(req);
    QObject::connect(reply, &QIODevice::readyRead,
                     q, [this, reply]() {
        int statusCode =
            reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).
            toInt();
        if (statusCode == 0) return;
        m_detector.setBaseUrl(reply->url());
        QByteArray data = reply->readAll();
        m_detector.feed(data);
    });
    QObject::connect(reply, &QNetworkReply::finished,
                     q, [this, reply, url]() {
        reply->deleteLater();

        for (const auto &video: m_detector.videosFound()) {
            if (video.linkType == VideoDetector::LinkMediaFile) {
                QJsonObject videoInfo(video.info);
                addUrlAlias(videoInfo, url);
                addResponse(videoInfo);
            } else if (video.linkType == VideoDetector::LinkYoutube) {
                runYoutubeDl(video.url);
            }
        }
        addResponse(QJsonObject());
    });
    using NetErr = QNetworkReply::NetworkError;
    QObject::connect(reply, QOverload<NetErr>::of(&QNetworkReply::error),
                     q, [](NetErr code) {
        qWarning() << "Network error:" << code;
    });
}

void VideoExtractorPrivate::runYoutubeDl(const QUrl &url)
{
    m_pendingRequests++;
    m_youtubeDl->getUrlInfo(url, [this, url](const QJsonObject &info) {
        QJsonObject videoInfo(info);
        addUrlAlias(videoInfo, url);
        addResponse(videoInfo);
    });
}

void VideoExtractorPrivate::addUrlAlias(QJsonObject &response,
                                        const QUrl &url)
{
    if (url == m_currentInfoRequest.url) {
        // No need to replicate the url as an alias
        return;
    }

    QJsonValueRef urlsValue = response["urlAliases"];
    QJsonArray urls = urlsValue.toArray();
    QJsonValue urlValue(url.toString());
    if (!urls.contains(urlValue)) {
        urls.append(urlValue);
        urlsValue = urls;
    }
}

void VideoExtractorPrivate::addResponse(const QJsonObject &response)
{
    Q_Q(VideoExtractor);

    if (response.contains(QStringLiteral("error"))) {
        if (m_errorResponse.isEmpty()) {
            m_errorResponse = response;
        }
    } else if (!response.isEmpty()) {
        // successful response
        QJsonObject videoInfo(response);
        videoInfo["inputUrl"] = m_currentInfoRequest.url.toString();
        if (videoInfo.contains(QLatin1String("items"))) {
            // This is a playlist
            m_playlistResponse = videoInfo;
        } else {
            m_videosFound.append(videoInfo);
        }
    }
    m_pendingRequests--;
    if (m_pendingRequests == 0) {
        // Deliver the response
        QJsonObject response;
        if (!m_playlistResponse.isEmpty()) {
            /* If a playlist was extracted, this takes precedence */
            response = m_playlistResponse;
        } else if (!m_videosFound.isEmpty()) {
            response.insert(QStringLiteral("items"), m_videosFound);
        } else {
            response = m_errorResponse;
        }
        QJSEngine *engine = qjsEngine(q);
        QJSValue &callback = m_currentInfoRequest.callback;
        callback.call(QJSValueList { engine->toScriptValue(response) });
        processNext();
    }
}

void VideoExtractorPrivate::queueGetUrlInfo(const InfoRequest &request)
{
    m_infoRequests.enqueue(request);
    if (!isBusy()) {
        processNext();
    }
}

VideoExtractor::VideoExtractor(QObject *parent):
    QObject(parent),
    d_ptr(new VideoExtractorPrivate(this))
{
}

VideoExtractor::~VideoExtractor() = default;

void VideoExtractor::setYoutubeDl(QObject *youtubeDl)
{
    Q_D(VideoExtractor);

    YoutubeDl *dl = qobject_cast<YoutubeDl*>(youtubeDl);
    if (dl == d->m_youtubeDl) return;

    d->m_youtubeDl = dl;
    Q_EMIT youtubeDlChanged();
}

QObject *VideoExtractor::youtubeDl() const
{
    Q_D(const VideoExtractor);
    return d->m_youtubeDl;
}

void VideoExtractor::getUrlInfo(const QUrl &url, const QJSValue &callback)
{
    Q_D(VideoExtractor);
    d->queueGetUrlInfo(InfoRequest { url, callback });
}
