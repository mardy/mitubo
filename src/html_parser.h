/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_HTML_PARSER_H
#define MITUBO_HTML_PARSER_H

#include <QObject>
#include <QScopedPointer>
#include <QString>

class QByteArray;
class QUrl;

namespace MiTubo {

class HtmlParserPrivate;
class HtmlParser: public QObject
{
    Q_OBJECT

public:
    HtmlParser(QObject *parent = nullptr);
    virtual ~HtmlParser();

    void reset();
    void setBaseUrl(const QUrl &url);
    void feed(const QByteArray &data);

Q_SIGNALS:
    void gotAlternate(const QString &title, const QString &type,
                      const QUrl &url);
    void finished();

private:
    Q_DECLARE_PRIVATE(HtmlParser)
    QScopedPointer<HtmlParserPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_HTML_PARSER_H
