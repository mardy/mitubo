/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_FEED_MODEL_H
#define MITUBO_FEED_MODEL_H

#include <QAbstractListModel>
#include <QScopedPointer>
#include <QString>
#include <QVariantMap>

class QUrl;

namespace MiTubo {

class FeedModelPrivate;
class FeedModel: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QString parentPath READ parentPath NOTIFY pathChanged)
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    /* The "folder" property contains:
     * - title
     * - description
     * - logoUrl
     */
    Q_PROPERTY(QVariantMap folder READ folderInfo NOTIFY pathChanged)

public:
    enum Roles {
        UrlRole = Qt::UserRole,
        IsFolderRole,
        FolderPathRole,
        TitleRole,
        DescriptionRole,
        LogoUrlRole,
        LastUpdatedRole,
    };
    Q_ENUM(Roles)

    FeedModel(QObject *parent = nullptr);
    ~FeedModel();

    void setPath(const QString &path);
    QString path() const;

    QString parentPath() const;

    QVariantMap folderInfo() const;

    Q_INVOKABLE QVariant get(int row, const QString &roleName) const;

    Q_INVOKABLE void addOrUpdateFeed(const QVariantMap &feed);
    Q_INVOKABLE void addOrUpdateFolder(const QVariantMap &feed);
    Q_INVOKABLE void moveFeed(int row, int toRow);
    /* Move feed into a folder. If intoRow is -1, the feed is moved to the
     * parent folder */
    Q_INVOKABLE void moveFeedInto(int row, int intoRow);
    Q_INVOKABLE void deleteFeed(int row);

    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void countChanged();
    void pathChanged();

private:
    Q_DECLARE_PRIVATE(FeedModel)
    QScopedPointer<FeedModelPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_FEED_MODEL_H
