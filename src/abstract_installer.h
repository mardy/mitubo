/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_ABSTRACT_INSTALLER_H
#define MITUBO_ABSTRACT_INSTALLER_H

#include <QByteArray>
#include <QObject>
#include <QUrl>
#include <QString>

class QNetworkAccessManager;

namespace MiTubo {

class AbstractInstaller: public QObject
{
    Q_OBJECT

public:
    AbstractInstaller(QObject *parent = nullptr);
    ~AbstractInstaller();

    void setNetworkAccessManager(QNetworkAccessManager *nam) {
        m_nam = nam;
    }

    virtual void checkLatest() = 0;
    virtual void downloadUpdate() = 0;
    virtual void installUpdate() = 0;

    QString programName() const { return m_programName; }
    QString latestVersion() const { return m_latestVersion; }

Q_SIGNALS:
    void checkLatestDone(bool ok);
    void downloadProgress(quint64 received, quint64 total);
    void downloadDone(bool ok);
    void installDone(bool ok);

protected:
    QNetworkAccessManager *m_nam;
    QString m_programName;
    QString m_latestVersion;
};

} // namespace

#endif // MITUBO_ABSTRACT_INSTALLER_H
