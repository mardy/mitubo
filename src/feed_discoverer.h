/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_FEED_DISCOVERER_H
#define MITUBO_FEED_DISCOVERER_H

#include <QAbstractListModel>
#include <QScopedPointer>
#include <QString>
#include <QUrl>

namespace MiTubo {

class FeedDiscovererPrivate;
class FeedDiscoverer: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QString inputText READ inputText WRITE setInputText
               NOTIFY inputTextChanged)
    Q_PROPERTY(bool isFeed READ isFeed NOTIFY countChanged)
    Q_PROPERTY(QUrl canonicalUrl READ canonicalUrl NOTIFY countChanged)

public:
    enum Roles {
        UrlRole = Qt::UserRole,
        TitleRole,
        MimeTypeRole,
    };
    Q_ENUM(Roles)

    FeedDiscoverer(QObject *parent = nullptr);
    ~FeedDiscoverer();

    void setInputText(const QString &text);
    QString inputText() const;

    bool isFeed() const;
    QUrl canonicalUrl() const;

    Q_INVOKABLE QVariant get(int row, const QString &roleName) const;

    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;

Q_SIGNALS:
    void inputTextChanged();
    void countChanged();

private:
    Q_DECLARE_PRIVATE(FeedDiscoverer)
    QScopedPointer<FeedDiscovererPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_FEED_DISCOVERER_H
