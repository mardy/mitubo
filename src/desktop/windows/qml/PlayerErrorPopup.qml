import QtMultimedia 5.8
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Dialog {
    id: root

    property int errorCode: 0
    property string errorMessage

    property bool hasMoreStreams: false

    title: qsTr("Playback error")
    standardButtons: Dialog.Close
    modal: true
    anchors.centerIn: parent

    property bool _codecError: errorCode == MediaPlayer.ResourceError &&
                               errorMessage == ""

    ColumnLayout {
        anchors.fill: parent
        spacing: 12

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            text: qsTr("An error occurred during media playback (%1): %2").
                arg(root.errorCode).arg(root.errorMessage)
            visible: !root._codecError
        }

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            textFormat: Text.StyledText
            text: qsTr("<p>An error occurred during media playback; \
this is likely caused by missing codecs.</p>\
<p>Please consider installing additional video codecs from one of these \
sources:</p>\
<ul>\
 <li><a href=\"https://www.codecguide.com/download_kl.htm\">K-lite codecs</a>\
 <li><a href=\"https://github.com/Nevcairiel/LAVFilters/releases\">LAV filters</a>\
</ul>\
<p>Once these codecs have been installed, restart MiTubo and trying playing \
the media again.</p>")
            visible: root.codecError
            onLinkActivated: Qt.openUrlExternally(link)
        }

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            visible: root.hasMoreStreams
            text: qsTr("You can also try to switch to a different media format by clicking on the \u2699 symbol near the bottom right corner.")
        }
    }
}
