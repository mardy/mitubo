import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Popup {
    id: root

    property alias formats: formatModels.formats
    property alias currentFormatIndex: formatModels.currentFormatIndex

    signal formatRequested(int formatIndex)
    signal playbackRateRequested(real rate)

    property var _speedModel: [
        { "label": qsTr("Half speed"), "speed": 0.5 },
        { "label": qsTr("Normal speed"), "speed": 1.0 },
        { "label": qsTr("1.25x"), "speed": 1.25 },
        { "label": qsTr("1.5x"), "speed": 1.5 },
        { "label": qsTr("Double speed"), "speed": 2.0 },
        { "label": qsTr("Quadruple speed"), "speed": 4.0 },
    ]

    margins: 8

    GridLayout {
        anchors.fill: parent
        columns: 2

        Label {
            Layout.fillWidth: true
            text: qsTr("Streams")
        }

        ComboBox {
            id: streamSelector
            Layout.minimumWidth: implicitWidth * 2
            Layout.fillWidth: true
            model: formatModels.streamModel
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Format")
        }

        ComboBox {
            id: formatSelector
            Layout.fillWidth: true
            model: formatModels.formatsPerStream[streamSelector.currentIndex]
            textRole: "label"
            onActivated: root.selectFormat(model[currentIndex])
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Playback speed")
        }

        ComboBox {
            id: speedSelector
            Layout.fillWidth: true
            model: _speedModel
            textRole: "label"
            currentIndex: 1
            onActivated: root.playbackRateRequested(model[currentIndex].speed)
        }
    }

    FormatModels {
        id: formatModels
    }

    function selectFormat(format) {
        formatRequested(format["index"])
    }
}
