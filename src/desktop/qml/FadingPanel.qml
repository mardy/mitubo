import QtQuick 2.7

OsdPanel {
    id: root

    opacity: 0.0
    enabled: false

    states: [
        State {
            name: "exposed"
            PropertyChanges { target: root; opacity: 1.0 }
            PropertyChanges { target: root; enabled: true }
        }
    ]

    transitions: [
        Transition {
            SequentialAnimation {
                PropertyAnimation { properties: "enabled"; duration: 0 }
                NumberAnimation { properties: "opacity"; duration: 300 }
            }
        }
    ]
}
