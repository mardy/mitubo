import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

PlaylistItemDelegate {
    descriptionFormat: Text.StyledText
    canDelete: false
    showMoreInfoButton: true
}
