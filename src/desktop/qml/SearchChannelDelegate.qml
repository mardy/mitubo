import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

RowLayout {
    id: root

    property url siteUrl
    property url feedUrl
    property alias title: titleLabel.text
    property alias description: descriptionLabel.text
    property alias logoUrl: logo.source

    signal openRequested()

    Image {
        id: logo
        Layout.minimumWidth: status == Image.Null ? 0 : 64
        Layout.maximumWidth: 64
        Layout.rowSpan: 2
        sourceSize.width: 64
        fillMode: Image.PreserveAspectFit
    }

    ColumnLayout {
        Layout.fillWidth: true

        Label {
            id: titleLabel
            Layout.fillWidth: true
            elide: Text.ElideRight
            font.bold: true
        }

        Label {
            id: descriptionLabel
            Layout.fillWidth: true
            Layout.fillHeight: true
            maximumLineCount: 3
            elide: Text.ElideRight
            wrapMode: Text.WordWrap
            textFormat: Text.RichText
            clip: true
        }
    }

    ColumnLayout {
        Layout.fillWidth: false

        Button {
            Layout.fillWidth: true
            Layout.minimumWidth: implicitWidth
            text: qsTr("Subscribe")
            highlighted: true
            onClicked: root.openRequested()
        }

        Button {
            Layout.fillWidth: true
            Layout.minimumWidth: implicitWidth
            text: qsTr("Visit site")
            onClicked: Qt.openUrlExternally(root.siteUrl)
        }
    }
}
