import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "popupUtils.js" as PopupUtils

Page {
    id: root

    property alias folderPath: feedModel.path
    property alias playlistModel: feedList.playlistModel

    signal feedClicked(var feedModel, int index)
    signal playRequested(url videoUrl)
    signal infoRequested(url videoUrl)

    title: qsTr("Folder: %1").arg(feedModel.folder.title)
    header: TitleHeader {
        ToolButton {
            icon.source: "qrc:/icons/refresh"
            onClicked: feedList.refresh()
        }
    }

    FeedList {
        id: feedList
        anchors.fill: parent
        ScrollBar.vertical: ScrollBar {}
        model: feedModel
        onFeedClicked: root.feedClicked(model, feedIndex)
        onPlayRequested: root.playRequested(videoUrl)

        Label {
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("No subscriptions")
            wrapMode: Text.WordWrap
            font.italic: true
            visible: feedList.count == 0
        }

        DropArea {
            id: parentDrop
            anchors { left: parent.left; top: parent.top; bottom: parent.bottom }
            width: feedList.dragging ? 100 : 0
            opacity: feedList.dragging ? 1.0 : 0
            onDropped: {
                var item = drop.source
                console.log("Dropped item " + item)
                feedModel.moveFeedInto(item.index, -1)
            }

            Rectangle {
                anchors.fill: parent
                color: palette.base
                opacity: 0.8
            }

            Label {
                anchors.centerIn: parent
                visible: !parentDrop.containsDrag
                width: parent.height
                height: parent.width
                rotation: -90
                text: qsTr("Drop the feed here to move it to the parent folder")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font { pointSize: 18; italic: true }
            }

            Item {
                anchors.fill: parent
                visible: parentDrop.containsDrag

                Label {
                    id: dropLabel2
                    anchors { left: parent.left; top: parent.top; right: parent.right }
                    height: width
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    text: "\u2BAA" // Black Curved Upwards and Leftwards Arrow
                    fontSizeMode: Text.Fit
                    font.pointSize: 999
                }

                Item {
                    anchors {
                        left: parent.left; right: parent.right;
                        top: dropLabel2.bottom; bottom: parent.bottom
                    }
                    Label {
                        anchors.centerIn: parent
                        width: parent.height
                        height: parent.width
                        rotation: -90
                        text: qsTr("Move to the parent folder")
                        wrapMode: Text.WordWrap
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font { pointSize: 18; italic: true }
                    }
                }
            }

            Behavior on width {
                NumberAnimation { duration: 300 }
            }
            Behavior on opacity {
                NumberAnimation { duration: 300 }
            }
        }
    }

    FeedModel { id: feedModel }
    SystemPalette { id: palette }
}
