import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

ComboBox {
    id: root

    property var selectedProvider: model[currentIndex]

    width: height
    textRole: "name"
    flat: true
    indicator: Image {
        width: root.width
        height: root.height
        source: selectedProvider.icon
    }
    delegate: ItemDelegate {
        width: parent.width
        text: modelData.name
        icon { source: modelData.icon; color: "transparent" }
    }
    popup.width: 160
    popup.rightMargin: 4
}
