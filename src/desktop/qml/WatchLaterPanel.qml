import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var playlistModel: null

    signal addToPlaylistRequested(var list)

    margins: 8

    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            text: qsTr("Add to playlist:")
        }

        ListView {
            id: streamSelector
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: 200
            ScrollBar.vertical: ScrollBar {}
            implicitWidth: contentItem.childrenRect.width
            implicitHeight: contentItem.childrenRect.height
            clip: true
            model: root.playlistModel
            delegate: PlaylistDelegate {
                editable: false
                text: model.name
                subText: qsTr("%n elements", "", model.count)
                onClicked: root.addToList(model.playlist)
            }
            footer: PlaylistItemNew {
                onPlaylistNameChosen: {
                    var list = root.playlistModel.createPlaylist(playlistName, [])
                    root.addToPlaylistRequested(list)
                    root.close()
                }
            }
        }
    }

    function addToList(list) {
        root.addToPlaylistRequested(list)
        root.close()
    }
}
