import MiTubo 1.0
import QtQuick 2.7
import "popupUtils.js" as PopupUtils

LinkDropArea {
    id: root

    property var playlistModel: null

    signal playUrlRequested(url url)

    onUrlsDropped: handleUrlDrop(urls)

    function handleUrlDrop(urls) {
        console.log("Dropped urls: " + urls)
        var popup = PopupUtils.open(Qt.resolvedUrl("LinkDropPopup.qml"), root, {
            "x": root.drag.x,
            "y": root.drag.y,
            "urls": urls,
        })
        popup.watchLaterRequested.connect(function(urls, onTop) {
            console.log("Requested watch later for " + urls + ", onTop=" + onTop)
            playlistModel.addToWatchLater(urls, onTop)
        })
        popup.watchNowRequested.connect(function(url) {
            console.log("Requested now for " + url)
            root.playUrlRequested(url)
        })
    }
}
