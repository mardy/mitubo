import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Dialog {
    id: root

    property var response: ({})

    title: qsTr("Error opening video")
    standardButtons: Dialog.Close
    modal: true
    anchors.centerIn: parent
    margins: 24

    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            text: qsTr("There was an error retrieving the video information:")
        }

        Label {
            Layout.fillWidth: true
            Layout.margins: 24
            wrapMode: Text.WordWrap
            text: response.error
        }
    }
}
