import MiTubo 1.0
import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Rectangle {
    id: root

    property alias feedItemModel: itemPreviews.model
    property var playlistModel: null
    property alias title: titleLabel.text

    signal clicked()

    color: palette.base
    implicitWidth: layout.implicitWidth
    implicitHeight: layout.implicitHeight

    ColumnLayout {
        id: layout
        anchors { fill: parent; margins: 4 }

        Label {
            id: titleLabel
            Layout.fillWidth: true
            Layout.fillHeight: true
            textFormat: Text.StyledText
            elide: Text.ElideRight
            font.pointSize: 8
            font.bold: true
        }

        ListView {
            id: itemPreviews
            Layout.fillWidth: true
            Layout.fillHeight: true
            orientation: ListView.Horizontal
            implicitWidth: contentWidth
            implicitHeight: 80
            clip: true
            interactive: false
            spacing: 2
            delegate: FeedItemPreview {
                height: ListView.view.height
                width: height * 3 / 2
                enabled: false

                title: model.title
                datePublished: model.date
                previewUrl: model.thumbnail
                watchProgress: root.playlistModel.watchedProgress(model.url)
            }
            Rectangle {
                anchors { top: parent.top; right: parent.right; bottom: parent.bottom }
                width: 30
                gradient: Gradient {
                    orientation: Gradient.Horizontal
                    GradientStop { position: 0.0; color: "transparent"}
                    GradientStop { position: 1.0; color: root.color}
                }
                visible: itemPreviews.contentWidth > itemPreviews.width
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }

    SystemPalette { id: palette }
}
