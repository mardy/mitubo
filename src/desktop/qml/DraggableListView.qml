import QtQuick 2.9

ListView {
    id: root

    property bool moving: false
    property alias coveredItem: dragArea.coveredItem
    property alias draggedItem: dragArea._movedItem
    readonly property bool dragging: dragArea.drag.active

    signal moveRequested(int indexFrom, int indexTo)
    signal moveIntoRequested(int indexFrom, int indexInto)

    MouseArea {
        id: dragArea

        property var _flickable: firstFlickableParent(dragArea)
        property int _flickY: 0
        property var coveredItem: null
        property var listView: root
        property var _movedItem: null

        anchors.fill: parent
        enabled: root.moving && mouseConnection.target == null
        onPressed: handlePressed(mouse, drag)
        onPositionChanged: handlePositionChanged(mouse, drag)
        onReleased: handleReleased(drag)

        function handlePressed(mouse, drag) {
            var item = root.itemAt(mouse.x, mouse.y)
            console.log("Dragging item " + item)

            var pos = item.mapFromItem(dragArea, mouse.x, mouse.y)
            item.hotSpotX = pos.x
            item.hotSpotY = pos.y

            drag.target = item.contentItem
            drag.target.Drag.active = true
            drag.target.Drag.source = item
            drag.target.Drag.hotSpot.x = item.width / 2
            drag.target.Drag.hotSpot.y = item.height / 2

            _movedItem = item
            item.draggedItemParent = _flickable
            item.dragged = true
        }

        function handlePositionChanged(mouse, drag) {
            // No special handling if hovering a drop area:
            if (drag.target.Drag.target) {
                coveredItem = null
                return
            }

            var margin = 64
            var fastMargin = 32
            var m = mapToItem(_flickable, mouse.x, mouse.y)
            if (m.y < margin) {
                startFlick((m.y < fastMargin) ? -2 : -1)
            } else if (m.y > _flickable.height - margin) {
                startFlick((m.y > _flickable.height - fastMargin) ? 2 : 1)
            } else {
                stopFlick()
            }

            updateCoveredItem(mouse)
        }

        function handleReleased(drag) {
            stopFlick()
            _movedItem.dragged = false
            _movedItem.hotSpotX = 0
            _movedItem.hotSpotY = 0
            /* Since the action might cause the item to be removed from the
             * model, perform it last. */
            var action = null
            var movedIndex = _movedItem.index
            if (coveredItem) {
                var targetIndex = coveredItem.index
                if (coveredItem.coveredForDrop) {
                    action = "moveInto"
                } else {
                    if (_movedItem.index < targetIndex) {
                        // QVector move() is like "insert(to, takeAt(from))"
                        targetIndex -= 1
                    }
                    if (!coveredItem.overTopHalf) targetIndex += 1
                    if (_movedItem.index != targetIndex) {
                        action = "move"
                    }
                }
            } else {
                drag.target.Drag.drop()
            }
            _movedItem.resetPositions()
            _movedItem = null
            coveredItem = null
            root.moving = false
            if (action == "moveInto") {
                root.moveIntoRequested(movedIndex, targetIndex)
            } else if (action == "move") {
                root.moveRequested(movedIndex, targetIndex)
            }
        }

        function updateCoveredItem(mouse) {
            var actualCoveredItem = listView.itemAt(width / 2, mouse.y)
            if (!actualCoveredItem) {
                // We are on the spacing between items, pick the item above
                actualCoveredItem = listView.itemAt(width / 2, mouse.y - listView.spacing)
                if (!actualCoveredItem) return; // No idea where we are!
            }
            /* First, check to see if coveredItem needs to be changed; it may
             * be that even if the actually covered item (actualCoveredItem) is
             * the adjacent one, no movement is needed.
             * In order to find that out, check how deep into the adjacent
             * item's height we can retain our current coveredItem (=
             * dropHeight).
             */
            var dropHeight = listView.spacing + actualCoveredItem.borderHeight

            var newCoveredItem = null
            if (coveredItem) {
                var ciY = mapToItem(coveredItem, 0, mouse.y).y
                if (ciY >= -dropHeight &&
                    ciY <= coveredItem.height + dropHeight) {
                    // the current coveredItem is still good
                    newCoveredItem = coveredItem
                }
            }
            if (!newCoveredItem) {
                newCoveredItem = actualCoveredItem
            }
            var point = mapToItem(newCoveredItem.contentItem, mouse.x, mouse.y)
            newCoveredItem.updateDrag(point)
            if (newCoveredItem != coveredItem) {
                coveredItem = newCoveredItem
            }
        }

        function startFlick(direction) {
            _flickY = -500 * direction
            if (!flickTimer.running) flickTimer.start()
        }

        function stopFlick() {
            flickTimer.stop()
            _flickY = 0
            _flickable.cancelFlick()
        }

        function firstFlickableParent(item) {
            var p = item ? item.parent : null;
            while (p && (!p.hasOwnProperty("flicking") || !p.interactive)) {
                p = p.parent;
            }
            return p;
        }

        function onFlickTimer() {
            if ((_flickable.atYBeginning && _flickY > 0) ||
                (_flickable.atYEnd && _flickY < 0))
                return
            _flickable.flick(0, _flickY)
        }

        Timer {
            id: flickTimer
            repeat: true
            interval: 100
            triggeredOnStart: true
            onTriggered: dragArea.onFlickTimer()
        }
    }

    Connections {
        id: mouseConnection
        target: null
        ignoreUnknownSignals: true
        onPositionChanged: {
            var pos = dragArea.mapFromItem(target, mouse.x, mouse.y)
            dragArea.handlePositionChanged(pos, target.drag)
        }
        onReleased: dragArea.handleReleased(target.drag)
    }

    function startDragging(mouseArea, mouse) {
        mouseConnection.target = mouseArea
        listView.moving = true
        var pos = dragArea.mapFromItem(mouseArea, mouse.x, mouse.y)
        dragArea.handlePressed(pos, mouseArea.drag)
    }
}
