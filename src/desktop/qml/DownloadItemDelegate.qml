import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

SwipeDelegate {
    id: root

    property var download: null
    property var videoData: ({})

    clip: true
    contentItem: RowLayout {
        id: layout

        Image {
            id: logo
            Layout.minimumWidth: status == Image.Null ? 0 : 64
            Layout.maximumWidth: 64
            sourceSize.width: 64
            fillMode: Image.PreserveAspectFit
            source: root.videoData.thumbnail
        }

        DownloadItem {
            Layout.fillWidth: true
            download: root.download
        }
    }
}
