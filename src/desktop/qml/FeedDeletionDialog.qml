import QtQuick 2.7
import QtQuick.Controls 2.12

Dialog {
    id: root
    margins: 10
    title: qsTr("Delete folder")
    focus: true
    standardButtons: Dialog.Yes | Dialog.No

    Label {
        anchors.fill: parent
        wrapMode: Text.WordWrap
        text: qsTr("Removing the folder will also remove all the feeds it contains.\n\nDelete it anyway?")
    }
}
