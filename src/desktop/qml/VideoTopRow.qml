import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "Constants.js" as UI

RowLayout {
    id: root

    property string videoTitle: ""

    signal backRequested()

    spacing: 4

    OsdButton {
        icon: "qrc:/icons/back"
        onClicked: root.backRequested()
    }

    Label {
        Layout.fillWidth: true
        color: UI.OsdColorFg
        font.pointSize: 16
        text: videoTitle
        elide: Text.ElideRight
    }
}
