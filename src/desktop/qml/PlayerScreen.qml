import MiTubo 1.0
import QtMultimedia 5.8
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Window 2.2
import "popupUtils.js" as PopupUtils

Rectangle {
    id: root

    property int formatIndex: -1
    property var videoData: null
    property var blacklistedCodecs: Settings.blacklistedCodecs
    property var preferredFormats: Settings.preferredFormats
    property var playlistModel: null
    readonly property bool playing: mediaPlayer.playbackState == MediaPlayer.PlayingState

    signal playUrlRequested(url url)
    signal downloadRequested()

    objectName: "playerScreen"
    color: "black"
    onBlacklistedCodecsChanged: filterFormats()
    onVideoDataChanged: setAutoPlay(true)
    Keys.onPressed: {
        switch (event.key) {
        case Qt.Key_F: toggleFullScreen(); break
        case Qt.Key_Space: mediaPlayer.togglePlaying(); break
        }
    }

    property var _formats: videoData.formats
    property int _automaticallySelectedFormatIndex: 0
    property int _selectedFormatIndex: formatIndex >= 0 ?
        formatIndex : _automaticallySelectedFormatIndex
    property var _selectedFormat: _formats[_selectedFormatIndex]
    property var _errorPopup: null
    // The most advanced playback position reached
    property int _lastPosition: 0

    on_FormatsChanged: _automaticallySelectedFormatIndex = pickBestFormat()

    MediaPlayer {
        id: mediaPlayer
        autoPlay: true
        source: _selectedFormat.url
        volume: Settings.defaultVolume
        onSourceChanged: if (position <= 0) setStartingPosition()
        onVolumeChanged: volumeTimer.restart()
        onError: root.handleError(error, errorString)
        onPositionChanged: if (position > _lastPosition) { _lastPosition = position }

        function togglePlaying() {
            if (playbackState == MediaPlayer.PlayingState) {
                pause()
            } else {
                play()
            }
        }

        function setStartingPosition() {
            if (root.videoData.startTime > 0) {
                seek(root.videoData.startTime)
            }
        }
    }

    VideoOutput {
        anchors.fill: parent
        source: mediaPlayer

        SlidingPanel {
            position: "top"
            padding: 4
            state: panelController.effectiveState
            VideoTopRow {
                anchors.fill: parent
                videoTitle: videoData.title
                onBackRequested: root.goBack()
            }
        }

        SlidingPanel {
            position: "bottom"
            padding: 4
            state: panelController.effectiveState
            VideoBottomRow {
                id: videoBottomRow
                anchors.fill: parent
                formats: _formats
                currentFormatIndex: _selectedFormatIndex
                videoData: root.videoData
                playlistModel: root.playlistModel
                playing: root.playing
                duration: mediaPlayer.duration
                position: mediaPlayer.position
                volume: mediaPlayer.volume
                onSeekRequested: mediaPlayer.seek(seekPosition)
                onTogglePlayRequested: mediaPlayer.togglePlaying()
                onToggleFullScreenRequested: root.toggleFullScreen()
                onFormatRequested: {
                    root.addPreferredFormat(formatIndex)
                    root.playFormat(formatIndex)
                }
                onVolumeRequested: mediaPlayer.volume = volume
                onPlaybackRateRequested: mediaPlayer.playbackRate = rate
                onPlayVideoRequested: root.playVideoFromUrl(videoData.inputUrl)
                onDownloadRequested: root.downloadRequested()
            }
        }

        FadingPanel {
            id: playButton
            anchors.centerIn: parent
            padding: 16
            cornerRadius: 16
            state: panelController.effectiveState
            AbstractButton {
                implicitWidth: 100
                implicitHeight: 100
                indicator: Image {
                    anchors.fill: parent
                    source: root.playing ? "qrc:/icons/pause" : "qrc:/icons/play"
                    sourceSize { width: width; height: height }
                }
                onClicked: mediaPlayer.togglePlaying()
            }
        }

        BufferingIndicator {
            anchors { margins: 50; horizontalCenter: parent.horizontalCenter; top: playButton.bottom }
            bufferProgress: mediaPlayer.bufferProgress
            playerStatus: mediaPlayer.status
        }

        HidingPanelController {
            id: panelController
            anchors.fill: parent
            forceOpen: !root.playing || videoBottomRow.interacting
        }
    }

    LinkDropHandler {
        id: linkDropHandler
        anchors.fill: parent
        playlistModel: root.playlistModel
        onPlayUrlRequested: root.playVideoFromUrl(url)
    }

    ScreenSaverInhibitor {
        active: mediaPlayer.hasVideo && root.playing
    }

    Timer {
        id: volumeTimer
        interval: 2000
        onTriggered: Settings.defaultVolume = mediaPlayer.volume
    }

    Connections {
        target: RequestDispatcher
        onOpenUrlRequested: linkDropHandler.handleUrlDrop([url.toString()])
    }

    Connections {
        target: Qt.application
        onAboutToQuit: addVideoToAutomaticList()
    }

    function addVideoToAutomaticList() {
        if (mediaPlayer.status == MediaPlayer.NoMedia) {
            // nothing to do
            return
        }
        if (mediaPlayer.status == MediaPlayer.EndOfMedia ||
            (mediaPlayer.duration > 0 &&
             _lastPosition > mediaPlayer.duration - 30000)) {
            playlistModel.addWatchedVideo(videoData)
        } else if (_lastPosition > 0) {
            playlistModel.addToContinueWatching(videoData, _lastPosition)
        }
    }

    function goBack() {
        addVideoToAutomaticList()
        mediaPlayer.stop()
        root.StackView.view.pop()
    }

    function toggleFullScreen() {
        var window = Window.window
        if (window.visibility == Window.FullScreen) {
            window.showNormal()
        } else {
            window.showFullScreen()
        }
    }

    function playFormat(formatIndex) {
        var playerStatus = mediaPlayer.playbackState
        var position = mediaPlayer.position
        _selectedFormatIndex = formatIndex
        console.log("Play format switched to " + JSON.stringify(_selectedFormat, null, 2))
        mediaPlayer.seek(position)
        if (playerStatus == MediaPlayer.PlayingState) {
            mediaPlayer.play()
        } else if (playerStatus == MediaPlayer.PausedState) {
            mediaPlayer.pause()
        }
    }

    function setAutoPlay(autoPlay) {
        mediaPlayer.autoPlay = autoPlay
        mediaPlayer.autoLoad = autoPlay
    }

    function handleError(code, message) {
        // Avoid immediately trying to use the next codec
        setAutoPlay(false)

        console.log("MediaPlayer error (" + code + "):" + message)
        if (_errorPopup) {
            // If a popup is alread open, don't open another one
            return
        }
        if (code == MediaPlayer.FormatError) {
            _errorPopup = PopupUtils.open(Qt.resolvedUrl("UnsupportedCodecPopup.qml"), root, {
                "format": _selectedFormat,
                "blacklistedCodecs": Qt.binding(function() { return root.blacklistedCodecs }),
            });
            _errorPopup.blacklistRequested.connect(blacklistCodec)
        } else {
            var hasMoreStreams = root._formats.length > 1
            _errorPopup = PopupUtils.open(Qt.resolvedUrl("PlayerErrorPopup.qml"), root, {
                "errorCode": code,
                "errorMessage": message,
                "hasMoreStreams": hasMoreStreams,
            });
            // Abandon the page if this was the only playable format
            if (!hasMoreStreams) {
                _errorPopup.closed.connect(root.goBack)
            }
        }
        if (_errorPopup) {
            _errorPopup.closed.connect(function() {
                _errorPopup = null
            })
        }
    }

    function blacklistCodec(codec) {
        console.log("Blacklisting codec:" + codec)
        var tmp = blacklistedCodecs
        tmp.push(codec)
        Settings.blacklistedCodecs = tmp
    }

    function codecIsValid(codec) {
        return codec && codec != "none";
    }

    function codecIsUnsupported(codec) {
        return blacklistedCodecs.indexOf(codec) >= 0;
    }

    function filterFormats() {
        var formats = _formats;
        var supportedFormats = [];
        for (var i = 0; i < formats.length; i++) {
            var format = formats[i];
            if (codecIsValid(format.acodec) &&
                codecIsUnsupported(format.acodec)) continue;
            if (codecIsValid(format.vcodec) &&
                codecIsUnsupported(format.vcodec)) continue;
            supportedFormats.push(format);
        }
        _formats = supportedFormats;
    }

    function addPreferredFormat(index) {
        var format = root._formats[index]
        var formatNote = '' + format.height
        console.log("Setting preferred format to " + formatNote)
        var preferredFormats = root.preferredFormats
        var oldIndex = preferredFormats.indexOf(formatNote)
        if (oldIndex >= 0) {
            // bring it to the front
            if (oldIndex > 0) {
                var previousFirst = preferredFormats[0]
                preferredFormats[0] = formatNote
                preferredFormats[oldIndex] = previousFirst
            }
        } else {
            var length = preferredFormats.unshift(formatNote)
            if (length > 5) {
                preferredFormats.length = 5
            }
        }
        console.log("New preferred formats: " + preferredFormats)
        Settings.preferredFormats = preferredFormats
    }

    function pickPreferredFormat(formats, indexes) {
        var lowestPreferredFormatFound = 100 // just a high number
        var formatIndex = -1
        for (var i = 0; i < indexes.length; i++) {
            var index = indexes[i]
            var format = formats[index]
            var formatNote = '' + format.height
            if (!formatNote) continue

            // Now find the format note among the preferred ones
            var preference = root.preferredFormats.indexOf(formatNote)
            if (preference < 0) continue

            if (preference < lowestPreferredFormatFound) {
                lowestPreferredFormatFound = preference
                formatIndex = index
            }
        }
        if (formatIndex >= 0) {
            console.log("Picked format by preference: " + formats[formatIndex].height)
        }
        return formatIndex
    }

    function pickBestVideoFormat(formats, indexes) {
        var chosenIndex = pickPreferredFormat(formats, indexes)
        if (chosenIndex > 0) return chosenIndex

        var minimumHeight = Math.min(root.height, root.width) * 0.9
        var chosenHeight = -1
        for (var i = 0; i < indexes.length; i++) {
            var index = indexes[i]
            var format = formats[index]
            var height = format.height
            if (height <= 0) continue
            if (chosenHeight > 0) {
                if (chosenHeight >= minimumHeight && height > chosenHeight) continue
                if (height <= minimumHeight && height < chosenHeight) continue
            }
            chosenHeight = height
            chosenIndex = index
        }
        return chosenIndex >= 0 ? chosenIndex : indexes[0]
    }

    function pickBestFormat() {
        var formats = _formats;
        var audioVideoFormats = [];
        var audioOnlyFormats = [];
        var videoOnlyFormats = [];
        for (var i = 0; i < formats.length; i++) {
            var format = formats[i];
            if (format.acodec != "none" && format.vcodec != "none") {
                audioVideoFormats.push(i);
            } else if (format.acodec != "none") {
                audioOnlyFormats.push(i);
            } else {
                videoOnlyFormats.push(i);
            }
        }
        if (audioVideoFormats.length > 0) {
            return pickBestVideoFormat(formats, audioVideoFormats);
        } else if (audioOnlyFormats.length > 0) {
            return audioOnlyFormats[0];
        } else if (videoOnlyFormats.length > 0) {
            return pickBestVideoFormat(formats, videoOnlyFormats);
        } else {
            console.warn("No audio or video formats found")
            return 0;
        }
    }

    function playVideoFromUrl(url) {
        // Go back to make sure that all controls are re-initialized
        root.goBack()
        root.playUrlRequested(url)
    }
}
