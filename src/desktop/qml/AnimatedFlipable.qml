import QtQuick 2.9

Flipable {
    id: root

    property bool flipped: false

    implicitWidth: Math.max(front.implicitWidth, back.implicitWidth)
    implicitHeight: Math.max(front.implicitHeight, back.implicitHeight)
    transform: Rotation {
        id: rotation
        origin.x: root.width / 2
        origin.y: root.height / 2
        axis.x: 1; axis.y: 0; axis.z: 0
        angle: 0
    }
    states: [
        State {
            name: "back"
            PropertyChanges { target: rotation; angle: 180 }
            PropertyChanges { target: front; visible: false }
            when: root.flipped
        }
    ]
    transitions: Transition {
        NumberAnimation { target: rotation; property: "angle"; duration: 300 }
    }
}
