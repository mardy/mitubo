import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "popupUtils.js" as PopupUtils

Page {
    id: root

    property string searchText: ""

    signal urlAdded(url videoUrl, string action)
    signal openFeedRequested(url feedUrl)
    signal openPlaylistRequested(url playlistUrl)

    title: qsTr("Search")

    header: ToolBar {
        RowLayout {
            anchors.fill: parent

            BackButton {
                onClicked: root.StackView.view.pop()
            }

            Label {
                text: qsTr("Search:")
            }

            TextFieldWithContextMenu {
                id: searchField
                Layout.fillWidth: true
                placeholderText: qsTr("Enter search terms")
                text: root.searchText
                selectByMouse: true
                onAccepted: root.searchText = text

                rightPadding: searchLayout.width + searchLayout.anchors.margins * 2

                RowLayout {
                    id: searchLayout
                    anchors {
                        top: parent.top; bottom: parent.bottom
                        right: parent.right; margins: 4
                    }

                    Button {
                        Layout.fillHeight: true
                        Layout.preferredWidth: height
                        flat: true
                        contentItem: Text {
                            anchors.fill: parent
                            text: qsTr("\u2699") // Gears
                            font.pointSize: 18
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                        }
                        enabled: searchProviders.engine.optionsComponent != null
                        onClicked: root.showSearchOptions()
                    }

                    SearchProviderChooser {
                        Layout.fillHeight: true
                        Layout.preferredWidth: height
                        model: searchProviders.model
                        onActivated: searchProviders.selectedProvider = index
                    }
                }
            }
        }
    }

    Component.onCompleted: searchField.forceActiveFocus()

    SearchProviders {
        id: searchProviders
        searchText: root.searchText
    }

    ListView {
        property alias searchEngine: searchProviders.engine
        anchors.fill: parent
        model: searchEngine.model
        delegate: {
            switch (searchEngine.searchType) {
            case "videos": return searchDelegate
            case "channels": return channelDelegate
            case "playlists": return playlistDelegate
            }
        }

        property bool canFetchPage: true
        onVerticalOvershootChanged: {
            if (verticalOvershoot > 0) {
                if (canFetchPage) searchEngine.fetchMore();
                canFetchPage = false;
            } else {
                canFetchPage = true;
            }
        }

        ScrollBar.vertical: ScrollBar {}

        Component {
            id: channelDelegate
            SearchChannelDelegate {
                title: model.title
                description: model.description
                logoUrl: model.previewUrl
                siteUrl: model.url
                feedUrl: model.feedUrl
                x: 12
                width: ListView.view.width - 24
                height: 120
                onOpenRequested: root.openFeedRequested(model.feedUrl)
            }
        }

        Component {
            id: searchDelegate
            SearchDelegate {
                pageUrl: model.url
                previewUrl: model.previewUrl
                author: model.author
                feedUrl: model.feedUrl
                title: model.title
                description: model.description
                duration: model.duration
                host: model.host
                datePublished: model.datePublished
                width: ListView.view.width
                height: 120
                onPlayRequested: root.urlAdded(model.url, "play")
                onInfoRequested: root.urlAdded(model.url, "info")
                onOpenFeedRequested: root.openFeedRequested(feedUrl)
            }
        }

        Component {
            id: playlistDelegate
            SearchPlaylistDelegate {
                pageUrl: model.url
                title: model.title
                author: model.author
                itemCount: model.itemCount
                width: ListView.view.width - 24
                x: 12
                height: 120
                onOpenRequested: root.openPlaylistRequested(model.url)
            }
        }
    }

    function showSearchOptions() {
        var searchEngine = searchProviders.engine
        var component = searchEngine.optionsComponent
        var popup = PopupUtils.open(component, searchLayout, {
            searchOptions: searchEngine.searchOptions,
        })
        popup.closed.connect(function() {
            searchEngine.searchOptions = popup.searchOptions
        })
    }
}
