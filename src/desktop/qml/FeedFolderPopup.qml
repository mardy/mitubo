import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Dialog {
    id: root

    signal newFolderRequested(string name)

    anchors.centerIn: parent
    title: qsTr("Create new folder")
    standardButtons: Dialog.Ok | Dialog.Cancel
    focus: true

    property var _okButton: null

    Component.onCompleted: {
        _okButton = footer.standardButton(Dialog.Ok)
        nameField.forceActiveFocus()
    }
    onAccepted: root.newFolderRequested(nameField.text)

    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            text: qsTr("Folder Name:")
        }

        TextField {
            id: nameField
            Layout.fillWidth: true
            placeholderText: qsTr("Enter a name")
            // We use "/" as a path separator
            validator: RegExpValidator { regExp: /[^\/]+/ }
            onAccepted: root.accept()
        }
    }

    Binding {
        target: _okButton
        property: "enabled"
        value: nameField.text.acceptableInput
    }
}
