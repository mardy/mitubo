import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick.Window 2.2

Popup {
    id: root

    anchors.centerIn: parent
    margins: 12
    modal: true

    Flickable {
        anchors.fill: parent
        contentHeight: layout.height + layout.anchors.margins * 2
        implicitWidth: layout.implicitWidth + layout.anchors.margins * 2
        implicitHeight: contentHeight
        clip: true

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: layout
            anchors {
                left: parent.left; right: parent.right
                top: parent.top; margins: 20
            }

            Image {
                Layout.fillWidth: true
                Layout.maximumWidth: 256
                Layout.alignment: Qt.AlignHCenter
                horizontalAlignment: Image.AlignHCenter
                source: "qrc:/icons/mitubo"
                sourceSize.width: width
            }

            Label {
                Layout.fillWidth: true
                Layout.bottomMargin: 12
                textFormat: Text.StyledText
                text: qsTr("MiTubo version %1<br/><font size=\"1\">\
Copyright ⓒ 2020-2022 mardy.it</font>").arg(Qt.application.version)
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                text: qsTr("MiTubo is free software, distributed under the \
<a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\">GPL-v3 license</a>. \
Among other things, this means that you can share it with your friends for \
free, or resell it for a fee.")
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
                text: qsTr("If you got MiTubo for free, please consider \
making a small donation to its author:<br/><ul>\
<li>Paypal: \
<a href=\"https://www.paypal.com/donate/?hosted_button_id=P7YTMKGGCD9JC\">\
click here to donate a free amount</a>\
</li><li>\
<a href=\"https://money.yandex.ru/quickpay/shop-widget?\
writer=seller&targets=MiTubo&targets-hint=&default-sum=&\
button-text=14&hint=&successURL=&quickpay=shop&account=410015419471966\">\
Yandex Money</a></li></ul>")
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }
}
