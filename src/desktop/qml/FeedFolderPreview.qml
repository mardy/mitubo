import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

ListView {
    id: root

    property alias overlayColor: overlay.color
    property int minimumOverlayWidth: 200
    property var playlistModel: null

    signal feedClicked(var feedModel, int feedIndex)

    property int _overlayStart: -1

    signal refreshRequestedInternal()

    orientation: ListView.Horizontal
    implicitWidth: contentWidth
    implicitHeight: 120
    clip: true
    spacing: 2
    delegate: FeedPreview {
        height: ListView.view.height
        width: height * 2
        enabled: root._overlayStart < 0 || x < root._overlayStart
        rotation: -5; antialiasing: true

        title: model.title
        feedItemModel: FeedItemModel {
            url: model.url
        }
        playlistModel: root.playlistModel
        onClicked: root.feedClicked(root.model, index)

        Connections {
            target: root
            onRefreshRequestedInternal: feedItemModel.refresh()
        }
    }

    Label {
        anchors.fill: parent
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignVCenter
        text: qsTr("This folder is empty.<br><i>Swipe to the left, activate the “Move” option and drag feeds onto this folder.</i>")
        textFormat: Text.StyledText
        wrapMode: Text.WordWrap
        visible: count == 0
    }

    onCountChanged: overlayTimer.restart()
    onWidthChanged: overlayTimer.restart()
    onHeightChanged: overlayTimer.restart()

    function checkItemVisibility() {
        // Find how many items can be made interactive
        var probeX = Math.max(width - minimumOverlayWidth, 0)
        var probeY = height / 2
        var item = itemAt(probeX, probeY)
        if (!item) {
            // did we hit the spacing gap?
            probeX += spacing
            item = itemAt(probeX, probeY)
        }
        _overlayStart = item ? item.x : -1
    }

    Timer {
        id: overlayTimer
        interval: 50
        onTriggered: checkItemVisibility()
    }

    FeedContentsOverlay {
        id: overlay
        anchors { fill: parent; leftMargin: _overlayStart }
        visible: _overlayStart >= 0
    }

    function refresh() {
        root.refreshRequestedInternal()
    }
}
