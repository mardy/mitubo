import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Rectangle {
    id: root

    signal clicked()

    property alias title: titleLabel.text
    property date datePublished
    property int duration: 0
    property alias previewUrl: preview.source
    property real watchProgress: 0.0

    color: palette.base
    implicitWidth: layout.implicitWidth
    implicitHeight: layout.implicitHeight

    ColumnLayout {
        id: layout
        anchors { fill: parent; margins: 4 }

        property bool hasImage: preview.status == Image.Ready ||
                                preview.status == Image.Loading

        Image {
            id: preview
            Layout.fillWidth: true
            Layout.fillHeight: layout.hasImage
            Layout.minimumHeight: progressBar.visible ?
                (progressBar.implicitHeight + 4) : 0
            Layout.minimumWidth: 0
            sourceSize.height: root.height
            fillMode: Image.PreserveAspectFit
            antialiasing: true

            VideoPreviewProgressBar {
                id: progressBar
                anchors {
                    left: parent.left; right: parent.right;
                    bottom: parent.bottom; margins: 2
                }
                value: root.watchProgress
            }
        }

        Item {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.minimumHeight: titleLabel.lineCount > 1 ? titleLabel.implicitHeight :
                (titleLabel.implicitHeight + publishedLabel.implicitHeight)

            Label {
                id: titleLabel
                anchors.fill: parent
                maximumLineCount: layout.hasImage ? 2 : -1
                textFormat: Text.StyledText
                elide: Text.ElideRight
                wrapMode: Text.WordWrap
                font.pointSize: 8
                font.bold: true
                onLineLaidOut: if (layout.hasImage && line.number == 1) {
                    line.width = width - (publishedLabel.contentWidth + 8)
                }
            }

            Label {
                id: publishedLabel
                anchors.fill: parent
                verticalAlignment: Text.AlignBottom
                horizontalAlignment: Text.AlignRight
                font.pointSize: 7
                text: Qt.formatDateTime(root.datePublished)
            }
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: root.clicked()
    }

    SystemPalette { id: palette }
}
