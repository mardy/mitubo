import MiTubo 1.0
import QtQuick 2.7

DropArea {
    id: root

    signal urlsDropped(var urls)

    property var _urlRegexp: RegExp('https?:\/\/[^ )]*', 'ig')

    keys: [ "text/uri-list", "text/plain" ]

    onEntered: {
        if (drag.hasText && !_urlRegexp.test(drag.text)) {
            drag.accepted = false
        }
    }

    onDropped: {
        if (drop.hasUrls) {
            var urls = []
            for (var i = 0; i < drop.urls.length; i++) {
                urls.push(drop.urls[i].toString())
            }
            root.urlsDropped(urls)
        } else if (drop.hasText) {
            var urls = Utils.extractUrls(drop.text)
            if (urls.length > 0) {
                root.urlsDropped(urls)
            }
        }
    }
}
