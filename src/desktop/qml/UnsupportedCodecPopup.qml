import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Dialog {
    id: root

    property var format: ({})
    property var blacklistedCodecs: []

    signal blacklistRequested(var codec)

    title: qsTr("Unsupported media codec")
    standardButtons: Dialog.Close
    modal: true
    anchors.centerIn: parent

    property string _vcodec: format.vcodec
    property bool _hasVideo: _vcodec && _vcodec != "none"
    property string _acodec: format.acodec
    property bool _hasAudio: _acodec && _acodec != "none"

    ColumnLayout {
        anchors.fill: parent

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: 500
            wrapMode: Text.WordWrap
            text: qsTr("Some or all of the media streams are using an unsupported codec. You can blacklist the codec used in the unplayable stream so that MiTubo will never try to play similar streams again.")
        }

        GridLayout {
            Layout.fillWidth: true
            Layout.margins: 12
            columns: 2

            Label {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
                text: qsTr("\
<b>Video stream</b><br>\
Codec: %1").arg(_vcodec)
                visible: _hasVideo
            }

            Button {
                Layout.fillWidth: true
                property bool blacklisted: blacklistedCodecs.indexOf(_vcodec) >= 0
                text: blacklisted ? qsTr("Blacklisted") :
                                    qsTr("Blacklist video codec")
                enabled: !blacklisted
                visible: _hasVideo
                onClicked: root.blacklist(_vcodec)
            }

            Label {
                Layout.fillWidth: true
                wrapMode: Text.WordWrap
                textFormat: Text.StyledText
                text: qsTr("\
<b>Audio stream</b><br>\
Codec: %1").arg(_acodec)
                visible: _hasAudio
            }

            Button {
                Layout.fillWidth: true
                property bool blacklisted: blacklistedCodecs.indexOf(_acodec) >= 0
                text: blacklisted ? qsTr("Blacklisted") :
                                    qsTr("Blacklist audio codec")
                enabled: !blacklisted
                visible: _hasAudio
                onClicked: root.blacklist(_acodec)
            }
        }
    }

    function blacklist(codec) {
        blacklistRequested(codec)
    }
}
