import QtQml 2.2
import QtQml.Models 2.2

SearchProviderBase {
    id: root

    optionsComponent: Component {
        PeerTubeSearchOptions {}
    }
    searchOptions: {
        "type": "video",
        "sort": "-match",
    }
    searchType: {
        switch (searchOptions.type) {
        case "channel": return "channels"
        case "playlist": return "playlists"
        default: return "videos"
        }
    }

    property string _searchUrl: ""
    property string _baseUrl: "https://sepiasearch.org/"
    property int _currentPage: -1
    property string _apiUrl: {
        switch (searchOptions.type) {
        case "channel": return "api/v1/search/video-channels"
        case "playlist": return "api/v1/search/video-playlists"
        default: return "api/v1/search/videos"
        }
    }

    function fetchPage() {
        var xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                parseResponse(xhr.responseText)
            }
        }

        var url = _searchUrl
        _currentPage += 1
        if (_currentPage > 0) {
            url += '&start=' + _currentPage * 10
        }
        console.log("query: " + url)
        xhr.open("GET", url)
        xhr.send()
    }

    function runSearch() {
        model.clear()
        var parameters = buildParameters()
        _searchUrl = _baseUrl + _apiUrl + "?" +
            parameters +
            "count=10&" +
            "search=" + encodeURIComponent(root.searchText)
        _currentPage = -1
        fetchPage()
    }

    function fetchMore() {
        fetchPage();
    }

    function parseResponse(json) {
        try {
            var response = JSON.parse(json)
        } catch (e) {
            console.warn("Could not parse JSON: " + json)
            return
        }
        var items = response.data
        var len = items.length
        for (var i = 0; i < len; i++) {
            var o = items[i]
            //console.log("   Item: " + JSON.stringify(o))

            switch (root.searchType) {
            case "videos": parseVideo(o); break
            case "channels": parseChannel(o); break
            case "playlists": parsePlaylist(o); break
            }
        }
    }

    function parseVideo(o) {
        var now = new Date()
        var month = 30 * 24 * 60 * 60 * 1000 // 30 days
        var datePublishedText = ""
        if (o.createdAt) {
            var datePublished = new Date(o.createdAt)
            var monthsAgo = (now - datePublished) / month
            if (monthsAgo < 3) {
                datePublishedText = Qt.formatDate(datePublished)
            } else {
                if (monthsAgo > 12) {
                    datePublishedText = qsTr("%n year(s) ago", "", monthsAgo / 12)
                } else {
                    datePublishedText = qsTr("%n month(s) ago", "", monthsAgo)
                }
            }
        }

        var feedUrl = "http://" + o.channel.host +
            "/feeds/videos.xml?videoChannelId=" + o.channel.id

        // Use the same fields/roles for videos and channels, or the ListModel
        // will get confused (without using dynamicRoles)
        model.append({
            "url": o.url,
            "author": o.account.displayName,
            "feedUrl": feedUrl,
            "duration": o.duration,
            "host": o.account.host,
            "title": o.name,
            "previewUrl": o.thumbnailUrl,
            "description": o.description,
            "datePublished": datePublishedText,
        });
    }

    function parseChannel(o) {
        var feedUrl = "http://" + o.host +
            "/feeds/videos.xml?videoChannelId=" + o.id

        model.append({
            "url": o.url,
            "feedUrl": feedUrl,
            "title": o.displayName,
            "previewUrl": o.avatar ? o.avatar.url : "",
            "description": o.description,
        });
    }

    function parsePlaylist(o) {
        var playlistUrl =
            "https://www.youtube.com/playlist?list=" + o.playlistId
        // TODO: add o.description, o.thumbnailUrl
        model.append({
            "url": o.url,
            "title": o.displayName,
            "author": o.ownerAccount.displayName,
            "itemCount": o.videosLength,
        });
    }
}
