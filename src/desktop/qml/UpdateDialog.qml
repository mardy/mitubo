import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Dialog {
    id: root

    anchors.centerIn: parent
    margins: 12
    title: qsTr("MiTubo update check")
    standardButtons: Dialog.Ok
    modal: true

    Component.onCompleted: updater.start()

    property int _preferredWidth: 400
    property int _preferredHeight: 200

    ColumnLayout {
        anchors.fill: parent
        spacing: 20

        RowLayout {
            Layout.fillWidth: true
            Layout.preferredWidth: root._preferredWidth
            Layout.preferredHeight: root._preferredHeight
            visible: updater.status == Updater.Busy

            Label {
                Layout.fillWidth: true
                text: qsTr("Checking for updates…")
            }

            BusyIndicator {
                id: indicator
                running: true
            }
        }

        GridLayout {
            Layout.fillWidth: true
            Layout.preferredWidth: root._preferredWidth
            Layout.preferredHeight: root._preferredHeight
            columns: 2
            visible: updater.status == Updater.Failed

            Image {
                source: "qrc:/icons/failed"
                height: 32
                width: height
                sourceSize.width: width
            }

            Label {
                Layout.fillWidth: true
                text: qsTr("An error occurred while contacting the server")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                color: "red"
            }

            Button {
                Layout.columnSpan: 2
                Layout.alignment: Qt.AlignHCenter
                text: qsTr("Retry")
                onClicked: updater.start()
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.preferredWidth: root._preferredWidth
            Layout.preferredHeight: root._preferredHeight
            visible: updater.status == Updater.Succeeded
            spacing: 20

            Label {
                Layout.fillWidth: true
                visible: updateLabel.visible
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("A new version is available!")
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 16
                color: "green"
            }

            Label {
                Layout.fillWidth: true
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("Latest version: <b>%1</b>").arg(updater.latestVersion.version)
                horizontalAlignment: Text.AlignHCenter
            }

            Label {
                id: updateLabel
                Layout.fillWidth: true
                visible: Object.keys(updater.latestVersion).length > 0 ?
                    updater.latestVersion.isNewer : false
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("You are running MiTubo %1; if you obtained MiTubo from an application store, visit it to update to the latest version. Otherwise, you can download the latest version by downloading it from <a href=\"http://mardy.it/mitubo\">mardy.it/mitubo</a>.").arg(Qt.application.version)
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Label {
                Layout.fillWidth: true
                visible: !updateLabel.visible
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                text: qsTr("Your installation of MiTubo is up to date.")
            }
        }
    }

    Updater {
        id: updater
    }
}
