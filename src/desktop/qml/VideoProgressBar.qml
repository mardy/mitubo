import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import "Constants.js" as UI

Item {
    id: root

    property alias duration: bar.to
    property int position: 0

    signal seekRequested(int seekPosition)

    implicitWidth: bar.implicitWidth
    implicitHeight: bar.implicitHeight + 8

    Rectangle {
        id: timeIndicator
        x: bar.handle.x - width / 2
        anchors.bottom: parent.top
        radius: 4
        color: UI.OsdColorFg
        opacity: 0.8
        width: label.implicitWidth + 8
        height: label.implicitHeight + 8
        visible: bar.pressed
        Label {
            id: label
            anchors.centerIn: parent
            text: Utils.formatMediaTime(bar.value)
            font.pointSize: 14
        }
    }

    Slider {
        id: bar
        anchors.fill: parent
        value: root.position
        onMoved: root.seekRequested(value)
    }
}
