import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var searchOptions: ({})

    margins: 12

    onOpened: load()
    onClosed: save()

    property var __controls: ({
        "date": dateControl,
        "duration": lengthControl,
        "type": typeControl,
        "sort_by": sortControl,
    })

    GridLayout {
        anchors.fill: parent
        columns: 2

        Label {
            Layout.fillWidth: true
            text: qsTr("Type:")
        }

        ComboBox {
            id: typeControl
            Layout.fillWidth: true
            model: ListModel {
                ListElement { text: qsTr("Videos"); value: "video" }
                ListElement { text: qsTr("Channels"); value: "channel" }
                ListElement { text: qsTr("Playlists"); value: "playlist" }
                // Invidious supports "all" and "playlist", but we don't
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Published:")
        }

        ComboBox {
            id: dateControl
            Layout.fillWidth: true
            Layout.minimumWidth: 200
            model: ListModel {
                ListElement { text: qsTr("Any time"); value: "" }
                ListElement { text: qsTr("Last hour"); value: "hour" }
                ListElement { text: qsTr("Today"); value: "today" }
                ListElement { text: qsTr("Less than a week ago"); value: "week" }
                ListElement { text: qsTr("Less than a month ago"); value: "month" }
                ListElement { text: qsTr("Less than one year ago"); value: "year" }
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Duration:")
        }

        ComboBox {
            id: lengthControl
            Layout.fillWidth: true
            model: ListModel {
                ListElement { text: qsTr("Any"); value: "" }
                ListElement { text: qsTr("Short"); value: "short" }
                ListElement { text: qsTr("Long"); value: "long" }
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Sort by:")
        }

        ComboBox {
            id: sortControl
            Layout.fillWidth: true
            model: ListModel {
                ListElement { text: qsTr("Relevance"); value: "relevance" }
                ListElement { text: qsTr("Upload date"); value: "upload_date" }
                ListElement { text: qsTr("Rating"); value: "rating" }
                ListElement { text: qsTr("View count"); value: "view_count" }
            }
            textRole: "text"
        }
    }

    function load() {
        for (var key in searchOptions) {
            if (!__controls.hasOwnProperty(key)) continue
            var control = __controls[key]

            // lookup the index
            var value = searchOptions[key]
            var model = control.model
            var index = 0 // Let the first one be the default
            for (var i = 0; i < model.count; i++) {
                if (model.get(i).value == value) {
                    index = i
                    break
                }
            }
            control.currentIndex = index
        }
    }

    function save() {
        var options = {}
        for (var key in __controls) {
            var control = __controls[key]
            var value = control.model.get(control.currentIndex).value
            if (value) {
                options[key] = value
            }
        }
        searchOptions = options
    }
}
