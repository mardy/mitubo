import QtQml 2.2
import QtQml.Models 2.2

SearchProviderBase {
    id: root

    optionsComponent: Component {
        YandexSearchOptions {}
    }

    property string _searchUrl: ""
    property int _currentPage: -1

    function fetchPage() {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
            if (xhr.readyState == XMLHttpRequest.DONE) {
                parseResponse(xhr.responseText);
            }
        }

        var url = _searchUrl;
        _currentPage += 1;
        if (_currentPage > 0) {
            url += '&p=' + _currentPage
        }
        xhr.open("GET", url);
        xhr.send();
    }

    function runSearch() {
        model.clear();
        var parameters = buildParameters()
        _searchUrl = "https://yandex.ru/video/search?" +
            parameters +
            "text=" +
            encodeURIComponent(root.searchText);
        _currentPage = -1;
        fetchPage();
    }

    function fetchMore() {
        fetchPage();
    }

    function unquoteHtml(text) {
        return text
            .replace(/&quot;/g, '"')
            .replace(/&apos;/g, "'")
            .replace(/&amp;/g, '&');
    }

    function parseResponse(html) {
        // TODO: use a HTML parser (gumbo, maybe?)
        var pos = html.indexOf('<noframes')
        if (pos < 0) {
            console.log("Missing <noframes>")
            return;
        }
        html = html.substring(pos)
        var re = /^<noframes[^>]*>/
        var match = re.exec(html)
        if (!match) {
            console.log("Cannot find end of element ")
            console.log("HTML: " + html)
            return;
        }

        pos = match[0].length
        var startArray = pos
        var endArray = -1
        // find the end of the array
        var stringStart = -1
        var openObjects = 0
        var openArrays = 0
        while (pos < html.length) {
            if (stringStart >= 0) {
                var endQuote = html.indexOf('"', pos)
                if (endQuote < 0) {
                    console.log("String not terminated: " + html.slice(stringStart));
                    return;
                }

                if (html[endQuote - 1] == '\\') {
                    pos = endQuote + 1
                    continue
                }
                stringStart = -1
                pos = endQuote + 1
            }
            switch (html[pos]) {
            case '"':
                stringStart = pos;
                break;
            case '{':
                openObjects++;
                break;
            case '}':
                openObjects--;
                break;
            case '[':
                openArrays++;
                break;
            case ']':
                openArrays--;
                break;
            }

            if (stringStart < 0 && openObjects == 0 && openArrays == 0) {
                endArray = pos + 1;
                break;
            }
            pos++
        }

        if (endArray < 0) {
            console.log("End array not found, open objects: " + openObjects + ", open arrays: " + openArrays);
            return;
        }

        var json = html.slice(startArray, endArray)

        try {
            var o = JSON.parse(json)
        } catch (e) {
            console.warn("Could not parse JSON");
            return;
        }

        var currentPage = o.pages.search.currentPage.toString()
        var snippets = o.pages.search.viewerData.organicSnippets[currentPage]
        console.log("Snippets: " + snippets.length)
        for (var i = 0; i < snippets.length; i++) {
            var item = snippets[i]
            if (!item.title) continue
            var previewUrl = ""
            try {
                previewUrl = item.thumb.image
                if (previewUrl[0] == '/')
                    previewUrl = 'https:' + previewUrl;
            } catch (e) {}
            var datePublished = ""
            try {
                var d = new Date(item.time)
                datePublished = Qt.formatDate(d)
            } catch (e) {
                console.log(e)
            }
            model.append({
                "url": item.url,
                "author": "",
                "feedUrl": "",
                "duration": item.duration,
                "title": unquoteHtml(item.title.text),
                "host": item.providerName,
                "previewUrl": previewUrl,
                "description": item.description ? unquoteHtml(item.description) : "",
                "datePublished": datePublished,
            });
        }
    }
}
