import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import "Constants.js" as UI

ProgressBar {
    id: root
    visible: value > 0
    background: Rectangle {
        implicitWidth: 200
        implicitHeight: 10
        radius: 3
        border { color: UI.WatchProgressColorFg; width: 1 }
        color: UI.WatchProgressColorBg
    }
    contentItem: Item {
        implicitWidth: 200
        implicitHeight: 8
        Rectangle {
            width: root.visualPosition * parent.width
            height: parent.height
            radius: 2
            color: UI.WatchProgressColorFg
        }
    }
}
