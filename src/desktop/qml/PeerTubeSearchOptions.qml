import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Popup {
    id: root

    property var searchOptions: ({})

    margins: 12

    onOpened: load()
    onClosed: save()

    /* Options can be found here:
     https://framagit.org/framasoft/peertube/search-index/-/blob/master/server/lib/elastic-search/elastic-search-videos.ts
     */
    property var __controls: ({
        "nsfw": nsfwControl,
        "type": typeControl,
        "sort": sortControl,
    })

    GridLayout {
        anchors.fill: parent
        columns: 2

        Label {
            Layout.fillWidth: true
            text: qsTr("Type:")
        }

        ComboBox {
            id: typeControl
            Layout.fillWidth: true
            model: ListModel {
                ListElement { text: qsTr("Videos"); value: "video" }
                ListElement { text: qsTr("Channels"); value: "channel" }
                /* TODO when youtube-dl starts supporting playlists
                ListElement { text: qsTr("Playlists"); value: "playlist" }
                */
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Display sensitive content:")
        }

        ComboBox {
            id: nsfwControl
            Layout.fillWidth: true
            enabled: typeControl.currentIndex == 0
            model: ListModel {
                ListElement { text: qsTr("No"); value: "false" }
                ListElement { text: qsTr("Yes"); value: "both" }
                ListElement { text: qsTr("Only sensitive content"); value: "true" }
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Published:")
        }

        ComboBox {
            id: dateControl
            Layout.fillWidth: true
            Layout.minimumWidth: 200
            enabled: typeControl.currentIndex == 0
            model: ListModel {
                ListElement { text: qsTr("Any time"); value: "" }
                ListElement { text: qsTr("Last hour"); value: "hour" }
                ListElement { text: qsTr("Today"); value: "today" }
                ListElement { text: qsTr("Less than a week ago"); value: "week" }
                ListElement { text: qsTr("Less than a month ago"); value: "month" }
                ListElement { text: qsTr("Less than one year ago"); value: "year" }
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Duration:")
        }

        ComboBox {
            id: lengthControl
            Layout.fillWidth: true
            enabled: typeControl.currentIndex == 0
            model: ListModel {
                ListElement { text: qsTr("Any"); value: "" }
                ListElement { text: qsTr("Short (< 4 min)"); value: "short" }
                ListElement { text: qsTr("Medium (4-10 min)"); value: "medium" }
                ListElement { text: qsTr("Long (> 10 min"); value: "long" }
            }
            textRole: "text"
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Sort by:")
        }

        ComboBox {
            id: sortControl
            Layout.fillWidth: true
            model: ListModel {
                ListElement { text: qsTr("Relevance"); value: "-match" }
                ListElement { text: qsTr("Upload date"); value: "-createdAt" }
            }
            textRole: "text"
        }
    }

    function setControlValue(control, value) {
        var model = control.model
        var index = 0 // Let the first one be the default
        for (var i = 0; i < model.count; i++) {
            if (model.get(i).value == value) {
                index = i
                break
            }
        }
        control.currentIndex = index
    }

    function load() {
        for (var key in searchOptions) {
            if (!__controls.hasOwnProperty(key)) continue
            var control = __controls[key]

            // lookup the index
            var value = searchOptions[key]
            setControlValue(control, value)
        }

        loadDuration(lengthControl, searchOptions)
    }

    function dateValue(control) {
        var value = control.model.get(control.currentIndex).value
        var now = new Date()
        var secs = 0
        switch (value) {
        case "hour": secs = 3600; break
        case "today": secs = 3600 * 24; break
        case "week": secs = 3600 * 24 * 7; break
        case "month": secs = 3600 * 24 * 31; break
        case "year": secs = 3600 * 24 * 365; break
        default: return ""
        }
        var startDate = new Date()
        startDate.setTime(now.getTime() - secs * 1000)
        return startDate.toISOString()
    }

    function saveDuration(control, options) {
        var duration = control.model.get(control.currentIndex).value
        switch (duration) {
        case "short": options["durationMax"] = 4 * 60; break
        case "medium":
            options["durationMin"] = 4 * 60
            options["durationMax"] = 10 * 60
            break
        case "long": options["durationMin"] = 10 * 60; break
        }
    }

    function loadDuration(control, options) {
        var duration = ""
        if (options.durationMax) {
            if (options.durationMin) duration = "medium"
            else duration = "short"
        } else if (options.durationMin) {
            duration = "long"
        }
        setControlValue(control, duration)
    }

    function save() {
        var options = {}
        for (var key in __controls) {
            var control = __controls[key]
            if (!control.enabled) continue
            var value = control.model.get(control.currentIndex).value
            if (value) {
                options[key] = value
            }
        }

        // The publishing date filter is a bit more complex
        var dv = dateValue(dateControl)
        if (dv) {
            options["startDate"] = dv
        }

        saveDuration(lengthControl, options)

        searchOptions = options
    }
}
