import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Item {
    id: root

    property var color

    Rectangle {
        id: gradientSquare
        anchors { top: parent.top; bottom: parent.bottom }
        width: Math.max(height, parent.width - textRect.width)
        gradient: Gradient {
            orientation: Gradient.Horizontal
            GradientStop { position: 0.0; color: "transparent" }
            GradientStop { position: 1.0; color: root.color }
        }
    }

    Rectangle {
        anchors {
            top: parent.top; bottom: parent.bottom
            left: gradientSquare.right; right: parent.right
        }
        color: root.color
    }

    Rectangle {
        id: textRect
        anchors { right: parent.right; verticalCenter: parent.verticalCenter }
        radius: 8
        color: palette.base
        width: Math.min(parent.width, layout.implicitWidth)
        height: layout.implicitHeight

        RowLayout {
            id: layout
            anchors.fill: parent

            Label {
                Layout.fillWidth: true
                Layout.leftMargin: 12
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                wrapMode: Text.WordWrap
                text: qsTr("View more items…")
            }
            Label {
                Layout.margins: 12
                font.pointSize: 24
                text: "»"
            }
        }
    }

    SystemPalette { id: palette }
}
