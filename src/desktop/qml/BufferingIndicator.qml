import QtMultimedia 5.8
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "Constants.js" as UI

OsdPanel {
    id: root

    property alias playerStatus: label.playerStatus
    property real bufferProgress: 0.0

    visible: label.text != ""
    cornerRadius: 8
    padding: 12

    ColumnLayout {
        anchors.fill: parent

        BufferingLabel {
            id: label
            Layout.fillWidth: true
            color: UI.OsdColorFg
        }

        ProgressBar {
            id: bar
            Layout.fillWidth: true
            value: root.bufferProgress
            background: Rectangle {
                implicitWidth: 200
                implicitHeight: 10
                radius: 3
                border { color: UI.OsdColorFg; width: 1 }
                color: UI.OsdColorBg
            }
            contentItem: Item {
                implicitWidth: 200
                implicitHeight: 8
                Rectangle {
                    width: bar.visualPosition * parent.width
                    height: parent.height
                    radius: 2
                    color: UI.OsdColorFg
                }
            }
        }
    }
}
