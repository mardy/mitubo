import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

ListView {
    id: root

    property alias overlayColor: overlay.color
    property int minimumOverlayWidth: 200
    property var playlistModel: null

    signal itemClicked(var item)

    property int _overlayStart: -1

    orientation: ListView.Horizontal
    implicitWidth: contentWidth
    implicitHeight: 120
    clip: true
    spacing: 2
    delegate: FeedItemPreview {
        height: ListView.view.height
        width: height * 3 / 2
        enabled: root._overlayStart < 0 || x < root._overlayStart

        title: model.title
        datePublished: model.date
        previewUrl: model.thumbnail
        watchProgress: root.playlistModel.watchedProgress(model.url)

        onClicked: root.itemClicked(model)
    }
    onCountChanged: checkItemVisibility()
    onWidthChanged: checkItemVisibility()

    function checkItemVisibility() {
        // Find how many items can be made interactive
        var probeX = Math.max(width - minimumOverlayWidth, 0)
        var probeY = height / 2
        var item = itemAt(probeX, probeY)
        if (!item) {
            // did we hit the spacing gap?
            probeX += spacing
            item = itemAt(probeX, probeY)
        }
        _overlayStart = item ? item.x : -1
    }

    FeedContentsOverlay {
        id: overlay
        anchors { fill: parent; leftMargin: _overlayStart }
        visible: _overlayStart >= 0
    }

    function refresh() {
        model.refresh()
    }
}
