import MiTubo 1.0
import QtQuick 2.9
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Page {
    id: root

    property var videoData: ({})
    property var download: null

    signal downloadRequested(var videoData, var mediaFormat)

    title: qsTr("Download media file")

    header: TitleHeader {}

    Flickable {
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height + layout.anchors.margins * 2

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: layout
            spacing: 24

            anchors {
                margins: 8; top: parent.top
                left: parent.left; right: parent.right
            }

            GroupBox {
                Layout.fillWidth: true
                title: qsTr("Media format selection")

                ColumnLayout {
                    anchors.fill: parent

                    Label {
                        Layout.fillWidth: true
                        text: qsTr("Download streams")
                    }

                    ComboBox {
                        id: streamSelector
                        Layout.fillWidth: true
                        model: formatModels.streamModel
                    }

                    Label {
                        Layout.fillWidth: true
                        text: qsTr("Format")
                    }

                    ComboBox {
                        id: formatSelector
                        Layout.fillWidth: true
                        model: formatModels.formatsPerStream[streamSelector.currentIndex]
                        textRole: "label"
                    }
                }
            }

            Button {
                Layout.alignment: Qt.AlignHCenter
                highlighted: true
                text: qsTr("Download")
                enabled: root.download == null
                onClicked: root.downloadSelected()
            }

            Frame {
                Layout.fillWidth: true
                visible: root.download != null

                Loader {
                    anchors.fill: parent
                    sourceComponent: downloadItemComponent
                    active: parent.visible
                }
            }
        }
    }

    Component {
        id: downloadItemComponent
        DownloadItem {
            anchors.fill: parent
            download: root.download
        }
    }

    FormatModels {
        id: formatModels
        formats: root.videoData.formats
    }

    function downloadSelected() {
        downloadRequested(videoData,
                          formatSelector.model[formatSelector.currentIndex])
    }
}
