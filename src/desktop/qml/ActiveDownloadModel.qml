import QtQml.Models 2.9

ListModel {
    id: root

    property var downloader: null

    function addDownload(videoData, download) {
        root.append({
            download: download,
            videoData: videoData,
        })
    }
}
