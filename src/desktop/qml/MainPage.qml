import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "popupUtils.js" as PopupUtils

Page {
    id: root

    property alias feedModel: feedList.model
    property alias playlists: feedList.playlistModel
    property alias activeDownloadModel: mainMenu.activeDownloadModel

    signal playlistPageRequested()
    signal searchRequested(string searchText)
    signal urlAdded(url videoUrl)
    signal feedAdded(url feedUrl)
    signal feedClicked(var feedModel, int index)
    signal infoRequested(url videoUrl)

    title: qsTr("Video selection")
    header: TitleHeader {
        ToolButton {
            text: "\u2630"
            font.pointSize: 16
            onClicked: mainMenu.open()
            MainMenu {
                id: mainMenu
                y: parent.height
            }
        }
    }

    Component.onCompleted: urlField.forceActiveFocus()

    Flickable {
        id: pageFlickable
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height + layout.anchors.margins * 2

        ScrollBar.vertical: ScrollBar {}

        ColumnLayout {
            id: layout
            anchors {
                left: parent.left; right: parent.right; top: parent.top
                margins: 8
            }


            Label {
                Layout.fillWidth: true
                text: qsTr("Enter some search terms or the address of the page containing the desired video")
                wrapMode: Text.WordWrap
            }

            TextFieldWithContextMenu {
                id: urlField
                property bool isUrl: text.startsWith("http")
                property bool isFeed: feedDiscoverer.isFeed
                Layout.fillWidth: true
                placeholderText: qsTr("Video address")
                inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhNoAutoUppercase
                selectByMouse: true
                onAccepted: activate()

                function activate() {
                    if (isFeed) root.feedAdded(feedDiscoverer.canonicalUrl)
                    else if (isUrl) root.urlAdded(urlField.text)
                    else root.searchRequested(urlField.text)
                }
            }

            RowLayout {
                Layout.alignment: Qt.AlignHCenter
                Layout.bottomMargin: 12
                spacing: 18

                Button {
                    text: urlField.isFeed ? qsTr("Add subscription") : (urlField.isUrl ? qsTr("Play") : qsTr("Search"))
                    highlighted: true
                    onClicked: urlField.activate()
                }

                Button {
                    text: qsTr("Show info")
                    visible: urlField.isUrl && !urlField.isFeed
                    onClicked: root.infoRequested(urlField.text)
                }
            }

            GroupBox {
                Layout.alignment: Qt.AlignHCenter
                Layout.maximumWidth: parent.width
                Layout.topMargin: 12
                title: qsTr("RSS feed(s) detected", "", feedRepeater.count)
                visible: feedRepeater.count > 0

                ColumnLayout {
                    anchors.fill: parent

                    Label {
                        Layout.fillWidth: true
                        text: qsTr("The following RSS feeds were found in the webpage:")
                        font.pointSize: 8
                        wrapMode: Text.WordWrap
                    }

                    Repeater {
                        id: feedRepeater
                        model: FeedDiscoverer {
                            id: feedDiscoverer
                            inputText: urlField.text
                        }

                        RowLayout {
                            Layout.fillWidth: true

                            Label {
                                Layout.fillWidth: true
                                elide: Text.ElideMiddle
                                text: model.title
                            }
                            Button {
                                text: enabled ? qsTr("Subscribe") : qsTr("Subscribed")
                                onClicked: {
                                    enabled = false
                                    root.feedAdded(model.url)
                                }
                            }
                        }
                    }
                }
            }

            Label {
                Layout.topMargin: 40
                Layout.bottomMargin: 10
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                text: qsTr("or")
                visible: playlistButton.visible
            }

            Button {
                id: playlistButton
                Layout.alignment: Qt.AlignHCenter
                text: qsTr("Go to your playlists")
                onClicked: root.playlistPageRequested()
            }

            RowLayout {
                Layout.topMargin: 50
                Layout.bottomMargin: 10
                Layout.fillWidth: true
                Label {
                    Layout.fillWidth: true
                    text: qsTr("Your subscriptions")
                    font.pointSize: 14
                }
                Button {
                    flat: true
                    icon.source: "qrc:/icons/refresh"
                    onClicked: feedList.refresh()
                }
                Button {
                    flat: true
                    text: "+"
                    font.pointSize: 16
                    onClicked: {
                        var item = PopupUtils.open(Qt.resolvedUrl("FeedFolderPopup.qml"), root)
                        item.newFolderRequested.connect(function(name) {
                            root.feedModel.addOrUpdateFolder({
                                "title": name,
                            })
                        })
                    }
                }
            }

            FeedList {
                id: feedList
                Layout.fillWidth: true
                visible: count > 0
                interactive: false  // The page is already flickable
                onFeedClicked: root.feedClicked(feedModel, feedIndex)
                onFolderClicked: root.openFeedFolder(index)
                onPlayRequested: root.urlAdded(videoUrl)

                ToolTip {
                    id: itemsAddedNotifier
                    enter: Transition {
                        PropertyAnimation {
                            property: "opacity"
                            from: 0.0
                            to: 1.0
                            duration: 500
                            easing.type: Easing.Linear
                        }
                        NumberAnimation {
                            property: "y"
                            from: itemsAddedNotifier.parent.mapFromItem(root, 0, root.height).y
                            to: 0
                            duration: 1400
                            easing.type: Easing.OutBounce
                        }
                    }
                }
            }

            Label {
                Layout.fillWidth: true
                text: qsTr("You have no subscriptions; enter an RSS address in the search box above to add one.")
                wrapMode: Text.WordWrap
                font.italic: true
                visible: feedList.count == 0
            }
        }

        Connections {
            target: root.feedModel
            onRowsInserted: pageFlickable.notifyAddition()
        }

        function notifyAddition() {
            // Items are added at the bottom; if it's visible, we have nothing
            // to do
            if (atYEnd) return
            itemsAddedNotifier.show(
                qsTr("\u{1f847} Scroll down to see the newly added item \u{1f847}"),
                3000)
        }
    }

    Timer {
        interval: 5000
        running: true
        repeat: false
        onTriggered: root.checkForUpdates()
    }

    function checkForUpdates() {
        checkForDownloaderUpdates()
        checkForMiTuboUpdates()
    }

    function createObject(componentUrl) {
        var component = Qt.createComponent(Qt.resolvedUrl(componentUrl))
        if (component.status != Component.Ready) {
            console.warn("Component failed to load: " + component.errorString())
            return
        }
        return component.createObject(root)
    }

    function checkForDownloaderUpdates() {
        var obj = createObject("DownloaderMaintenance.qml")
        obj.finished.connect(function() {
            obj.destroy()
        })
    }

    function checkForMiTuboUpdates() {
        var obj = createObject("Updater.qml")
        obj.start()
        obj.finished.connect(function() {
            obj.destroy()
        })
    }

    function openFeedFolder(index) {
        var path = index >= 0 ?
            feedModel.get(index, "folderPath") : feedModel.parentPath
        console.log("Opening folder on path " + path)
        var pageStack = StackView.view
        var page = pageStack.push(Qt.resolvedUrl("FeedFolderPage.qml"), {
            "folderPath": path,
            "playlistModel": root.playlists,
        })
        page.playRequested.connect(root.urlAdded)
        page.infoRequested.connect(root.infoRequested)
        page.feedClicked.connect(root.feedClicked)
    }
}
