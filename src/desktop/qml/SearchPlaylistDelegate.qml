import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

RowLayout {
    id: root

    property url pageUrl
    property alias title: titleLabel.text
    property string author
    property int itemCount: 0

    signal openRequested()

    ColumnLayout {
        Layout.fillWidth: true
        Layout.fillHeight: true
        Layout.alignment: Qt.AlignVCenter

        Label {
            id: titleLabel
            Layout.fillWidth: true
            elide: Text.ElideRight
            font.bold: true
        }

        Label {
            id: descriptionLabel
            Layout.fillWidth: true
            elide: Text.ElideRight
            textFormat: Text.RichText
            text: qsTr("By %1 - %n elements", "", root.itemCount).
                arg(root.author)
        }
    }

    ColumnLayout {
        Layout.fillWidth: false

        Button {
            Layout.fillWidth: true
            Layout.minimumWidth: implicitWidth
            text: qsTr("Open playlist")
            highlighted: true
            onClicked: root.openRequested()
        }

        Button {
            Layout.fillWidth: true
            Layout.minimumWidth: implicitWidth
            text: qsTr("Visit site")
            onClicked: Qt.openUrlExternally(root.pageUrl)
        }
    }
}
