import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3

Drawer {
    id: root

    signal finished()

    edge: Qt.BottomEdge
    interactive: false
    modal: false
    width: parent.width
    // padding: 8  <- Does not work, for some reason
    onClosed: if (!updating) finished()

    property bool updating: false

    contentHeight: contentItem.implicitHeight + 16
    contentItem: RowLayout {
        anchors { fill: parent; margins: 8 }

        Label {
            Layout.fillWidth: true
            Layout.fillHeight: true
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.WordWrap
            text: qsTr("A new version (%1) of <b>%2</b> is available; <a href=\"foo\">click here</a> to install it.")
                .arg(installer.latestVersion)
                .arg(installer.programName)
            onLinkActivated: root.update()
        }

        Button {
            Layout.alignment: Qt.AlignVCenter
            id: doneButton
            text: qsTr("Hide")
            onClicked: {
                root.close()
            }
        }
    }

    YoutubeDlInstaller {
        id: installer
        onStatusChanged: {
            console.log("Installer status: " + status)
            if (status == YoutubeDlInstaller.UpdateAvailable) {
                root.open()
            }
        }
        autoDownload: false
        Component.onCompleted: checkForUpdates()
    }

    function update() {
        root.close()
        root.updating = true
        var page =
            pageStack.push(Qt.resolvedUrl("DownloaderInstallationPage.qml"), {
        })
        page.finished.connect(function() {
            root.finished()
        })
    }
}
