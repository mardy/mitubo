import MiTubo 1.0
import QtQuick 2.7
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import "popupUtils.js" as PopupUtils

SwipeDelegate {
    id: root

    property url feedUrl
    property alias title: titleLabel.text
    property alias description: descriptionLabel.text
    property alias logoUrl: logo.source
    property bool expanded: false
    property var playlistModel: null
    property bool isFolder: true

    signal feedClicked(var feedModel, int feedIndex)
    signal playRequested(url videoUrl)
    signal moveRequested()
    signal unsubscriptionRequested()

    swipe.left: DeleteButton {
        anchors {
            top: parent.top; bottom: parent.bottom
            right: parent.background.left
        }
        text: root.isFolder? qsTr("Remove entire folder") : qsTr("Unsubscribe")
        onClicked: {
            if (root.isFolder && contentsLoader.item.model.count > 0) {
                var popup = PopupUtils.open(Qt.resolvedUrl("FeedDeletionDialog.qml"),
                                            root, {})
                popup.accepted.connect(function(urls, onTop) {
                    root.unsubscriptionRequested()
                })
            } else {
                root.unsubscriptionRequested()
            }
        }
    }
    swipe.right: MoveListItemButton {
        anchors {
            top: parent.top; bottom: parent.bottom
            left: parent.background.right
        }
        onClicked: { root.moveRequested(); swipe.close(); tooltip.open() }

        ToolTip {
            id: tooltip
            timeout: 3000
            parent: root
            text: qsTr("Drag the feed to the desired position")
        }
    }
    clip: true
    contentItem: GridLayout {
        id: layout
        columns: 2

        Image {
            id: logo
            Layout.minimumWidth: status == Image.Null ? 0 : 64
            Layout.maximumWidth: 64
            Layout.rowSpan: 2
            sourceSize.width: 64
            fillMode: Image.PreserveAspectFit
        }

        Label {
            id: titleLabel
            Layout.fillWidth: true
            elide: Text.ElideRight
        }

        Label {
            id: descriptionLabel
            Layout.fillWidth: true
            Layout.fillHeight: true
            maximumLineCount: 3
            elide: Text.ElideRight
            wrapMode: Text.WordWrap
            textFormat: Text.RichText
            clip: true

            Rectangle {
                anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                height: parent.height / 2
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "transparent"}
                    GradientStop { position: 1.0; color: root.background.color}
                }
                visible: dots.visible
            }
            Label {
                id: dots
                anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                text: "..."
                font.pointSize: 30
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
                visible: parent.height < parent.implicitHeight
            }
        }

        Loader {
            id: contentsLoader
            Layout.fillWidth: true
            Layout.columnSpan: 2
            Connections {
                target: contentsLoader.item
                ignoreUnknownSignals: true
                onItemClicked: root.playRequested(item.url)
                onFeedClicked: root.feedClicked(feedModel, feedIndex)
            }
        }

        function loadFeedContents() {
            contentsLoader.setSource(Qt.resolvedUrl("FeedContentsPreview.qml"), {
                playlistModel: root.playlistModel,
                interactive: false,
                overlayColor: root.background.color,
            })
        }

        function loadFeedFolder() {
            var feedModel = feedModelComponent.createObject(root, {
                path: root.title,
            })
            contentsLoader.setSource(Qt.resolvedUrl("FeedFolderPreview.qml"), {
                model: feedModel,
                playlistModel: root.playlistModel,
                interactive: false,
                overlayColor: root.background.color,
            })
        }

        Component.onCompleted: {
            if (root.isFolder) {
                loadFeedFolder()
            } else {
                loadFeedContents()
            }
        }
    }

    onClicked: expanded = true

    Timer {
        running: !root.isFolder
        interval: 500
        repeat: false
        onTriggered: {
            var feedItemModel = feedItemModelComponent.createObject(root, {
                url: root.feedUrl,
            })
            contentsLoader.item.model = feedItemModel
        }
    }

    Component {
        id: feedItemModelComponent
        FeedItemModel {}
    }

    Component {
        id: feedModelComponent
        FeedModel {}
    }

    function refresh() {
        contentsLoader.item.refresh()
    }
}
