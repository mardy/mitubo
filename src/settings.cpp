/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "settings.h"

#include <QDebug>
#include <QSettings>

using namespace MiTubo;

namespace MiTubo {

const QString keyBlacklistedCodecs = QStringLiteral("BlacklistedCodecs");
const QString keyPreferredFormats = QStringLiteral("PreferredFormats");
const QString keyDefaultVolume = QStringLiteral("DefaultVolume");
const QString keyWindowSize = QStringLiteral("WindowSize");

class SettingsPrivate
{
public:
private:
    friend class Settings;
    QSettings m_settings;
};

} // namespace

Settings::Settings(QObject *parent):
    QObject(parent),
    d_ptr(new SettingsPrivate())
{
}

Settings::~Settings()
{
}

void Settings::setBlacklistedCodecs(const QStringList &codecs)
{
    Q_D(Settings);
    d->m_settings.setValue(keyBlacklistedCodecs, codecs);
    Q_EMIT blacklistedCodecsChanged();
}

QStringList Settings::blacklistedCodecs() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyBlacklistedCodecs).toStringList();
}

void Settings::setPreferredFormats(const QStringList &formats)
{
    Q_D(Settings);
    d->m_settings.setValue(keyPreferredFormats, formats);
    Q_EMIT preferredFormatsChanged();
}

QStringList Settings::preferredFormats() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyPreferredFormats).toStringList();
}

void Settings::setDefaultVolume(qreal defaultVolume)
{
    Q_D(Settings);
    d->m_settings.setValue(keyDefaultVolume, defaultVolume);
    Q_EMIT defaultVolumeChanged();
}

qreal Settings::defaultVolume() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyDefaultVolume, 1.0).toReal();
}

void Settings::setWindowSize(const QSizeF &windowSize)
{
    Q_D(Settings);
    d->m_settings.setValue(keyWindowSize, windowSize);
    Q_EMIT windowSizeChanged();
}

QSizeF Settings::windowSize() const
{
    Q_D(const Settings);
    return d->m_settings.value(keyWindowSize, QSizeF(640, 480)).toSizeF();
}
