/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "html_parser.h"

#include <Html/Parser>
#include <QByteArray>
#include <QDebug>
#include <QIODevice>
#include <QUrl>

using namespace MiTubo;
using namespace it::mardy::Html;

namespace MiTubo {

class HtmlParserPrivate: public Parser
{
    enum State {
        NotStarted = 0,
        InHtml,
        InHead,
        InBody,
    };

public:
    HtmlParserPrivate(HtmlParser *q);

    void handleStartTag(const QString &tag, const Parser::Attributes &attrs) override;
    void handleEndTag(const QString &tag) override;

    bool isYoutube() const;

private:
    Q_DECLARE_PUBLIC(HtmlParser)
    State m_state;
    QUrl m_baseUrl;
    HtmlParser *q_ptr;
};

} // namespace

HtmlParserPrivate::HtmlParserPrivate(HtmlParser *q):
    Parser(Parser::ConvertCharRefs),
    m_state(NotStarted),
    q_ptr(q)
{
}

void HtmlParserPrivate::handleStartTag(const QString &tag,
                                       const Parser::Attributes &attrs)
{
    Q_Q(HtmlParser);
    if (m_state == NotStarted) {
        if (tag == "html") {
            m_state = InHtml;
        }
    } else if (m_state == InHtml) {
        if (tag == "head") {
            m_state = InHead;
        } else if (tag == "body") {
            m_state = InBody;
        }
    } else if (m_state == InHead || m_state == InBody) {
        if (tag == "link" && attrs.value("rel") == "alternate") {
            QUrl url = attrs.value("href");
            Q_EMIT q->gotAlternate(attrs.value("title"),
                                   attrs.value("type"),
                                   m_baseUrl.resolved(url));
        }
    }
}

void HtmlParserPrivate::handleEndTag(const QString &tag)
{
    Q_Q(HtmlParser);
    if (m_state == InHead) {
        if (tag == "head") {
            m_state = InHtml;
            if (isYoutube()) {
                /* Youtube has the RSS links in the <body> element, so continue
                 * parsing */
            } else {
                // Nothing interesting for us anymore
                Q_EMIT q->finished();
            }
        }
    }
}

bool HtmlParserPrivate::isYoutube() const
{
    const QString hostName = m_baseUrl.host();
    return hostName.endsWith("youtube.com") || hostName == "youtu.be";
}

HtmlParser::HtmlParser(QObject *parent):
    QObject(parent),
    d_ptr(new HtmlParserPrivate(this))
{
}

HtmlParser::~HtmlParser() = default;

void HtmlParser::reset()
{
    Q_D(HtmlParser);
    d->m_state = HtmlParserPrivate::NotStarted;
    d->m_baseUrl.clear();
}

void HtmlParser::setBaseUrl(const QUrl &url)
{
    Q_D(HtmlParser);
    d->m_baseUrl = url;
}

void HtmlParser::feed(const QByteArray &data)
{
    Q_D(HtmlParser);
    d->feed(QString::fromUtf8(data));
}
