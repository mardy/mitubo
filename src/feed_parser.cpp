/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed_parser.h"

#include <QByteArray>
#include <QDateTime>
#include <QDebug>
#include <QIODevice>
#include <QXmlStreamReader>

using namespace MiTubo;

/*
 * Base class for XML-based feeds
 */
class AbstractXmlParser: public AbstractParser
{
    enum class MediaState {
        NotMedia,
        InMediaGroup,
        ReadingDescription,
    };

public:
    AbstractXmlParser(QIODevice *ioDevice);
    virtual ~AbstractXmlParser() = default;

    bool resumeRead();

protected:
    void skipCurrentElement();
    bool resumeSkippingIfNeeded();

    QString readElementText(QXmlStreamReader::ReadElementTextBehaviour behaviour =
                            QXmlStreamReader::ErrorOnUnexpectedElement);

    void readMediaThumbnail();
    void readMediaDescription();
    void readMediaGroup();

private:
    QString resumeReadElementText();

protected:
    QXmlStreamReader m_reader;
    FeedItem m_currentItem;

private:
    int m_skipDepth;
    QString m_partialText;
    QXmlStreamReader::ReadElementTextBehaviour m_readTextBehaviour;
    int m_readDepth;
    MediaState m_mediaState;
    bool m_inMediaGroup;
};

AbstractXmlParser::AbstractXmlParser(QIODevice *ioDevice):
    AbstractParser(ioDevice),
    m_reader(ioDevice),
    m_skipDepth(0),
    m_readDepth(0),
    m_mediaState(MediaState::NotMedia),
    m_inMediaGroup(false)
{
}

bool AbstractXmlParser::resumeRead()
{
    if (!resumeSkippingIfNeeded()) return false;

    if (m_mediaState == MediaState::ReadingDescription) {
        readMediaDescription();
        if (m_mediaState == MediaState::InMediaGroup) { qDebug() << "found!"; }
    }
    if (m_mediaState == MediaState::InMediaGroup) {
        readMediaGroup();
    }

    return m_mediaState == MediaState::NotMedia;
}

void AbstractXmlParser::skipCurrentElement()
{
    m_skipDepth = 1;
    resumeSkippingIfNeeded();
}

bool AbstractXmlParser::resumeSkippingIfNeeded()
{
    while (m_skipDepth > 0 &&
           m_reader.readNext() != QXmlStreamReader::Invalid) {
        if (m_reader.isEndElement()) {
            m_skipDepth--;
        } else if (m_reader.isStartElement()) {
            m_skipDepth++;
        }
    }
    return m_skipDepth == 0;
}

// Reimplementing because of https://bugreports.qt.io/browse/QTBUG-14661
QString AbstractXmlParser::readElementText(
        QXmlStreamReader::ReadElementTextBehaviour behaviour)
{
    if (m_readDepth > 0) {
        // We are resuming a previous read
        return resumeReadElementText();
    }

    if (!m_reader.isStartElement()) {
        qWarning() << "readElementText must be called on start element";
        return QString();
    }

    m_readTextBehaviour = behaviour;
    m_readDepth = 1;
    m_partialText.clear();
    return resumeReadElementText();
}

QString AbstractXmlParser::resumeReadElementText()
{
    while (m_readDepth > 0) {
        switch (m_reader.readNext()) {
        case QXmlStreamReader::Characters:
        case QXmlStreamReader::EntityReference:
            if (m_readDepth == 1 ||
                m_readTextBehaviour == QXmlStreamReader::IncludeChildElements) {
                m_partialText += m_reader.text();
            }
            break;
        case QXmlStreamReader::Invalid:
            return QString();
        case QXmlStreamReader::StartElement:
            m_readDepth++;
            break;
        case QXmlStreamReader::EndElement:
            m_readDepth--;
            break;
        default:
            break;
        }
    }
    return m_partialText;
}

void AbstractXmlParser::readMediaThumbnail() {
    const auto text = m_reader.attributes().value("url");
    m_currentItem.thumbnail = QUrl(text.toString());
    skipCurrentElement();
}

void AbstractXmlParser::readMediaDescription() {
    if (m_mediaState != MediaState::ReadingDescription) {
        // This is the very first time we explore this element
        const auto type = m_reader.attributes().value("type");
        m_currentItem.descriptionFormat = type == "html" ? Qt::RichText : Qt::PlainText;
    }

    const QString text = readElementText();
    if (!m_reader.hasError()) {
        m_currentItem.description = text;
        m_mediaState = m_inMediaGroup ?
            MediaState::InMediaGroup : MediaState::NotMedia;
    } else {
        m_mediaState = MediaState::ReadingDescription;
    }
}

void AbstractXmlParser::readMediaGroup() {
    m_mediaState = MediaState::InMediaGroup;
    m_inMediaGroup = true;
    while (m_reader.readNextStartElement()) {
        const QStringRef name = m_reader.name();

        if (name == "thumbnail") {
            readMediaThumbnail();
        } else if (name == "description") {
            readMediaDescription();
        } else {
            skipCurrentElement();
        }
    }
    if (!m_reader.hasError() && m_reader.isEndElement()) {
        m_inMediaGroup = false;
        m_mediaState = MediaState::NotMedia;
    }
}

class RssParser: public AbstractXmlParser
{
    enum State {
        NotStarted,
        InRss,
        InChannel,
        ReadingChannelTitle,
        ReadingChannelDescription,
        ReadingChannelLastUpdate,
        ReadingChannelLogo,
        ReadingChannelLogoUrl,
        InItem,
        ReadingItemTitle,
        ReadingItemDescription,
        ReadingItemDate,
        ReadingItemLink,
    };

public:
    RssParser(QIODevice *device):
        AbstractXmlParser(device),
        m_state(NotStarted)
    {
    }

    bool readText(std::function<void(const QString &)> callback,
                  QXmlStreamReader::ReadElementTextBehaviour behaviour =
                  QXmlStreamReader::QXmlStreamReader::ErrorOnUnexpectedElement) {
        const QString text = readElementText(behaviour);
        bool ok = !m_reader.hasError();
        if (ok) {
            callback(text);
        }
        return ok;
    }

    void readChannelTitle() {
        bool ok = readText(setTitle, QXmlStreamReader::IncludeChildElements);
        m_state = ok ? InChannel : ReadingChannelTitle;
    }

    void readChannelDescription() {
        bool ok = readText(setDescription, QXmlStreamReader::IncludeChildElements);
        m_state = ok ? InChannel : ReadingChannelDescription;
    }

    void readChannelLastUpdate() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            QDateTime date = QDateTime::fromString(text, Qt::RFC2822Date);
            setLastUpdate(date);
            m_state = InChannel;
        } else {
            m_state = ReadingChannelLastUpdate;
        }
    }

    void readChannelLogoUrl() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            setLogoUrl(text);
            m_state = ReadingChannelLogo;
        }
    }

    void readChannelLogo() {
        while (m_reader.readNextStartElement()) {
            if (m_reader.name() == "url") {
                m_state = ReadingChannelLogoUrl;
                readChannelLogoUrl();
            } else {
                skipCurrentElement();
            }
        }
        if (m_reader.isEndElement()) {
            m_state = InChannel;
        }
    }

    QString readItemText() {
        const QString text =
            m_reader.readElementText(QXmlStreamReader::IncludeChildElements);
        if (!m_reader.hasError()) {
            m_state = InItem;
        }
        return text;
    }

    void readItemTitle() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            m_currentItem.title = text;
            m_state = InItem;
        } else {
            m_state = ReadingItemTitle;
        }
    }

    void readItemDescription() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            m_currentItem.description = text;
            m_currentItem.descriptionFormat = Qt::RichText;
            m_state = InItem;
        } else {
            m_state = ReadingItemDescription;
        }
    }

    void readItemLink() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            m_currentItem.url = text;
            m_state = InItem;
        } else {
            m_state = ReadingItemLink;
        }
    }

    void readItemDate() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            m_currentItem.date = QDateTime::fromString(text, Qt::RFC2822Date);
            m_state = InItem;
        } else {
            m_state = ReadingItemDate;
        }
    }

    void readItemThumbnail() {
        const auto text = m_reader.attributes().value("url");
        m_currentItem.thumbnail = QUrl(text.toString());
        skipCurrentElement();
    }

    void readItem() {
        while (m_reader.readNextStartElement()) {
            const QStringRef name = m_reader.name();
            const QStringRef nsUri = m_reader.namespaceUri();
            bool noNs = nsUri.isEmpty();

            if (noNs && name == "title") {
                readItemTitle();
            } else if (noNs && name == "description") {
                readItemDescription();
            } else if (noNs && name == "link") {
                readItemLink();
            } else if (noNs && name == "pubDate") {
                readItemDate();
            } else if (name == "thumbnail") {
                readItemThumbnail();
            } else if (nsUri == "http://search.yahoo.com/mrss/" && name == "description") {
                readMediaDescription();
            } else {
                skipCurrentElement();
            }
        }
        if (!m_reader.hasError() && m_reader.isEndElement()) {
            if (!m_currentItem.url.isEmpty()) {
                addItem(m_currentItem);
            }
            m_state = InChannel;
        }
    }

    void onReadyRead() override {
        if (!resumeRead()) return;

        if (m_state == ReadingItemTitle) {
            readItemTitle();
        } else if (m_state == ReadingItemDescription) {
            readItemDescription();
        } else if (m_state == ReadingItemLink) {
            readItemLink();
        } else if (m_state == ReadingItemDate) {
            readItemDate();
        }

        if (m_state == ReadingChannelTitle) {
            readChannelTitle();
        } else if (m_state == ReadingChannelDescription) {
            readChannelDescription();
        } else if (m_state == ReadingChannelLastUpdate) {
            readChannelLastUpdate();
        } else if (m_state == ReadingChannelLogoUrl) {
            readChannelLogoUrl();
        } else if (m_state == ReadingChannelLogo) {
            readChannelLogo();
        } else if (m_state == InItem) {
            readItem();
        }

        while (m_reader.readNextStartElement()) {
            const QStringRef name = m_reader.name();

            if (m_state == NotStarted && name == "rss") {
                m_state = InRss;
            } else if (m_state == InRss && name == "channel") {
                m_state = InChannel;
            } else if (m_state == InChannel) {
                if (name == "title") {
                    readChannelTitle();
                } else if (name == "description") {
                    readChannelDescription();
                } else if (name == "lastBuildDate") {
                    readChannelLastUpdate();
                } else if (name == "image") {
                    m_state = ReadingChannelLogo;
                    readChannelLogo();
                } else if (name == "item") {
                    m_state = InItem;
                    m_currentItem = {};
                    readItem();
                } else {
                    skipCurrentElement();
                }
            }
        }
        if (m_reader.hasError() &&
            m_reader.error() != QXmlStreamReader::PrematureEndOfDocumentError) {
            gotError(m_reader.errorString());
        }
    }

private:
    State m_state;
};

class AtomParser: public AbstractXmlParser
{
    enum State {
        NotStarted,
        InFeed,
        ReadingFeedTitle,
        ReadingFeedSubtitle,
        ReadingFeedLastUpdate,
        ReadingFeedIcon,
        ReadingFeedIconUrl,
        InEntry,
        ReadingEntryTitle,
        ReadingEntryDate,
        ReadingEntryLink,
    };

public:
    AtomParser(QIODevice *device):
        AbstractXmlParser(device),
        m_state(NotStarted)
    {
    }

    bool readText(std::function<void(const QString &)> callback,
                  QXmlStreamReader::ReadElementTextBehaviour behaviour =
                  QXmlStreamReader::QXmlStreamReader::ErrorOnUnexpectedElement) {
        const QString text = readElementText(behaviour);
        bool ok = !m_reader.hasError();
        if (ok) {
            callback(text);
        }
        return ok;
    }

    void readFeedTitle() {
        bool ok = readText(setTitle, QXmlStreamReader::IncludeChildElements);
        m_state = ok ? InFeed : ReadingFeedTitle;
    }

    void readFeedSubtitle() {
        bool ok = readText(setDescription, QXmlStreamReader::IncludeChildElements);
        m_state = ok ? InFeed : ReadingFeedSubtitle;
    }

    void readFeedLastUpdate() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            QDateTime date = QDateTime::fromString(text, Qt::ISODate);
            setLastUpdate(date);
            m_state = InFeed;
        } else {
            m_state = ReadingFeedLastUpdate;
        }
    }

    QString readEntryText() {
        const QString text =
            m_reader.readElementText(QXmlStreamReader::IncludeChildElements);
        if (!m_reader.hasError()) {
            m_state = InEntry;
        }
        return text;
    }

    void readEntryTitle() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            m_currentItem.title = text;
            m_state = InEntry;
        } else {
            m_state = ReadingEntryTitle;
        }
    }

    void readEntryLink() {
        const auto attributes = m_reader.attributes();
        if (attributes.value("rel") == "alternate") {
            m_currentItem.url = QUrl(attributes.value("href").toString());
        }
        skipCurrentElement();
    }

    void readEntryDate() {
        const QString text = readElementText();
        if (!m_reader.hasError()) {
            m_currentItem.date = QDateTime::fromString(text, Qt::ISODate);
            m_state = InEntry;
        } else {
            m_state = ReadingEntryDate;
        }
    }

    void readEntry() {
        while (m_reader.readNextStartElement()) {
            const QStringRef name = m_reader.name();
            const QStringRef nsUri = m_reader.namespaceUri();
            bool atomNs = nsUri == "http://www.w3.org/2005/Atom";

            if (atomNs && name == "title") {
                readEntryTitle();
            } else if (atomNs && name == "link") {
                readEntryLink();
            } else if (atomNs && name == "published") {
                readEntryDate();
            } else if (nsUri == "http://search.yahoo.com/mrss/" && name == "group") {
                readMediaGroup();
            } else {
                skipCurrentElement();
            }
        }
        if (!m_reader.hasError() && m_reader.isEndElement()) {
            if (!m_currentItem.url.isEmpty()) {
                addItem(m_currentItem);
            }
            m_state = InFeed;
        }
    }

    void onReadyRead() override {
        if (!resumeRead()) return;

        if (m_state == ReadingEntryTitle) {
            readEntryTitle();
        } else if (m_state == ReadingEntryDate) {
            readEntryDate();
        }

        if (m_state == ReadingFeedTitle) {
            readFeedTitle();
        } else if (m_state == ReadingFeedSubtitle) {
            readFeedSubtitle();
        } else if (m_state == ReadingFeedLastUpdate) {
            readFeedLastUpdate();
        } else if (m_state == InEntry) {
            readEntry();
        }

        while (m_reader.readNextStartElement()) {
            const QStringRef name = m_reader.name();

            if (m_state == NotStarted && name == "feed") {
                m_state = InFeed;
            } else if (m_state == InFeed) {
                if (name == "title") {
                    readFeedTitle();
                } else if (name == "subtitle") {
                    readFeedSubtitle();
                } else if (name == "updated") {
                    readFeedLastUpdate();
                } else if (name == "entry") {
                    m_state = InEntry;
                    m_currentItem = {};
                    readEntry();
                } else {
                    skipCurrentElement();
                }
            }
        }
        if (m_reader.hasError() &&
            m_reader.error() != QXmlStreamReader::PrematureEndOfDocumentError) {
            qWarning() << "Parser error:" << m_reader.errorString();
            gotError(m_reader.errorString());
        }
    }

private:
    State m_state;
};

static AbstractParser *createParser(QIODevice *device,
                                    const QByteArray &preRead)
{
    qDebug() << Q_FUNC_INFO << preRead;
    if (preRead.contains("<rss ")) {
        return new RssParser(device);
    } else if (preRead.contains("<feed ")) {
        return new AtomParser(device);
    }
    return nullptr;
}

FeedParser::FeedParser(QIODevice *device)
{
    qRegisterMetaType<FeedItem>("FeedItem");
    QObject::connect(device, &QIODevice::readyRead,
                     this, &FeedParser::onReadyRead);
}

void FeedParser::onReadyRead()
{
    if (!m_parser) {
        QIODevice *device = qobject_cast<QIODevice*>(sender());
        /* Detect which parser to create */
        QByteArray buffer = device->peek(128);
        if (buffer.size() < 128) {
            /* We'll try again later */
            qDebug() << "could not read 64 bytes" << buffer;
            return;
        }

        m_parser.reset(createParser(device, buffer));
        if (m_parser) {
            m_parser->setTitle = [this](const QString &title) {
                Q_EMIT gotTitle(title);
            };
            m_parser->setDescription = [this](const QString &description) {
                Q_EMIT gotDescription(description);
            };
            m_parser->setLastUpdate = [this](const QDateTime &lastUpdate) {
                Q_EMIT gotLastUpdate(lastUpdate);
            };
            m_parser->setLogoUrl = [this](const QUrl &logoUrl) {
                Q_EMIT gotLogoUrl(logoUrl);
            };
            m_parser->addItem = [this](const FeedItem &item) {
                Q_EMIT gotItem(item);
            };
            m_parser->gotError = [this](const QString &message) {
                Q_EMIT gotError(message);
            };
        } else {
            Q_EMIT gotError("Could not create parser for " +
                            QString::fromUtf8(buffer));
            return;
        }
    }

    m_parser->onReadyRead();
}
