/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "installer_utils.h"

#include <QByteArray>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QProcess>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QTemporaryFile>
#include <QUrl>
#include <QVersionNumber>

using namespace MiTubo;

namespace MiTubo {

class InstallerUtilsPrivate {
public:
    static QString dataPath();
    static QFile *openTempFile(const QString &nameTemplate);
    static void saveData(QIODevice *network, QFile *file);

    static void windowWorkaround(QFile *tempFile);
};

} // namespace

static const QString s_activeDir = QStringLiteral("downloader");
static const QString s_extractionDir = QStringLiteral("downloader-latest");
static const QString s_obsoleteDir = QStringLiteral("downloader-old");
static const int s_maxDownloadSize = 50 * 1024 * 1024;

QString InstallerUtilsPrivate::dataPath()
{
    return QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
}

void InstallerUtils::extractZip(const QString &zipFile,
                                const QString &extractionDir,
                                const ExtractCb &callback)
{
    QProcess *extractor = new QProcess();
    extractor->setProgram("unzip");
    QObject::connect(extractor,
                     qOverload<int,QProcess::ExitStatus>(&QProcess::finished),
                     [=](int exitCode, QProcess::ExitStatus exitStatus) {
        extractor->deleteLater();
        qDebug() << "process error output" << extractor->readAllStandardError();
        bool ok = (exitStatus == QProcess::NormalExit && exitCode == 0);
        callback(ok);
        QFile::remove(zipFile);
    });
    extractor->setWorkingDirectory(extractionDir);
    extractor->setArguments({ zipFile });
    extractor->start();
}

QVersionNumber InstallerUtils::pythonVersion()
{
    QProcess python;
    python.start("python3", { "--version" });
    python.waitForFinished();
    QByteArray output = python.readAllStandardOutput();
    QByteArray versionData = output.split(' ').last();
    QVersionNumber version =
        QVersionNumber::fromString(QString::fromUtf8(versionData));
    qDebug() << "Python version:" << version;
    return version;
}

QFile *InstallerUtilsPrivate::openTempFile(
    const QString &nameTemplate)
{
    QFile *file;
    if (nameTemplate.contains("XXXXXX")) {
        QString templateName = QDir::tempPath() + "/" + nameTemplate;
        QTemporaryFile *tempFile = new QTemporaryFile(templateName);
        tempFile->setAutoRemove(false);
        file = tempFile;
    } else {
        file = new QFile(nameTemplate);
    }
    bool ok = file->open(QIODevice::WriteOnly);
    qDebug() << "Writing to" << file->fileName();
    if (!ok) {
        delete file;
        return nullptr;
    }

    return file;
}

void InstallerUtilsPrivate::saveData(QIODevice *network, QFile *file)
{
    qint64 availableStorage = s_maxDownloadSize - file->pos();
    if (availableStorage <= 0) {
        qWarning() << "Download file exceeds maximum size:"
            << file->pos();
        network->close();
        return;
    }

    QByteArray data = network->read(availableStorage);
    /* The downloaded file starts with a shebang line which confuses unzip: it
     * still processes the archive, but returns 1 as exit code. So, let's
     * remove it. */
    if (file->size() == 0 && data.startsWith("#!")) {
        int endOfFirstLine = data.indexOf("\nPK");
        if (endOfFirstLine > 0) {
            data = data.mid(endOfFirstLine + 1);
        }
    }
    int readCount = data.length();
    int64_t writtenCount = file->write(data);
    if (Q_UNLIKELY(writtenCount != readCount)) {
        qWarning() << "read" << readCount << "but wrote only" << writtenCount;
    }
}

void InstallerUtilsPrivate::windowWorkaround(QFile *tempFile)
{
#ifdef Q_OS_WIN
    /* Workaround QTemporaryFile not being ready (unzip doesn't see it).
     * flushing and calling sleep() does not help.
     */
    QString dummyFileName;
    {
        QTemporaryFile dummy;
        if (dummy.open()) {
            dummyFileName = dummy.fileName();
            qDebug() << "prepared dummy file" << dummyFileName;
            dummy.close();
        }
    }
    bool ok = tempFile->copy(dummyFileName);
    qDebug() << "Temporary copy succeded:" << ok << tempFile->error() << tempFile->errorString();
#else
    Q_UNUSED(tempFile);
#endif
}

void InstallerUtils::downloadFile(QNetworkAccessManager *nam,
                                  const QUrl &url,
                                  const QString &nameTemplate,
                                  const ProgressCb &progressCb,
                                  const DownloadCb &downloadCb)
{
    QFile *tempFile =
        InstallerUtilsPrivate::openTempFile(nameTemplate);
    if (!tempFile) {
        downloadCb(QString());
        return;
    }

    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);

    QNetworkReply *reply = nam->get(req);
    reply->ignoreSslErrors();
    QObject::connect(reply, &QNetworkReply::downloadProgress,
                     progressCb);
    QObject::connect(reply, &QIODevice::readyRead,
                     [reply, tempFile]() {
        InstallerUtilsPrivate::saveData(reply, tempFile);
    });
    QObject::connect(reply, &QNetworkReply::finished,
                     [reply, tempFile, downloadCb]() {
        reply->deleteLater();

        InstallerUtilsPrivate::saveData(reply, tempFile);
        tempFile->close();
        if (reply->error() == QNetworkReply::NoError) {
            InstallerUtilsPrivate::windowWorkaround(tempFile);
            downloadCb(tempFile->fileName());
        } else {
            downloadCb(QString());
        }
    });
}

void InstallerUtils::fetchUrl(QNetworkAccessManager *nam,
                              const QUrl &url,
                              const FetchUrlCb &callback)
{
    QNetworkRequest req(url);
    req.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                     QNetworkRequest::NoLessSafeRedirectPolicy);

    QNetworkReply *reply = nam->get(req);
    reply->ignoreSslErrors();
    QObject::connect(reply, &QNetworkReply::finished,
                     [reply, callback]() {
        reply->deleteLater();

        QByteArray contents = reply->readAll();
        bool ok = reply->error() == QNetworkReply::NoError;
        callback(contents, ok);
    });
}

void InstallerUtils::activateLatestVersion()
{
    qDebug() << "activating";
    QDir baseDir(InstallerUtilsPrivate::dataPath());

    baseDir.rename(s_activeDir, s_obsoleteDir);
    baseDir.rename(s_extractionDir, s_activeDir);
    if (baseDir.cd(s_obsoleteDir)) {
        baseDir.removeRecursively();
    }
}

QDir InstallerUtils::extractionDir(DirFlags flags)
{
    QDir dataDir(InstallerUtilsPrivate::dataPath() + "/" + s_extractionDir);
    if (flags & DirFlag::ClearContents) {
        dataDir.removeRecursively();
    }
    if (flags & DirFlag::EnsureExists) {
        dataDir.mkpath(".");
    }
    return dataDir;
}

QDir InstallerUtils::activeDir(DirFlags flags)
{
    QDir dataDir(InstallerUtilsPrivate::dataPath() + "/" + s_activeDir);
    if (flags & DirFlag::ClearContents) {
        dataDir.removeRecursively();
    }
    if (flags & DirFlag::EnsureExists) {
        dataDir.mkpath(".");
    }
    return dataDir;
}
