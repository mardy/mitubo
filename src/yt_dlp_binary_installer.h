/*
 * Copyright (C) 2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_YT_DLP_BINARY_INSTALLER_H
#define MITUBO_YT_DLP_BINARY_INSTALLER_H

#include "abstract_github_installer.h"

namespace MiTubo {

/*
 * The yt-dlp downloader, binary release
 *
 * https://github.com/yt-dlp/yt-dlp
 */
class YtDlpBinaryInstaller: public AbstractGithubInstaller
{
    Q_OBJECT

public:
    YtDlpBinaryInstaller(QObject *parent = nullptr);

    void downloadUpdate() override;
    void installUpdate() override;

private:
    QString m_downloadedFile;
};

} // namespace

#endif // MITUBO_YT_DLP_BINARY_INSTALLER_H
