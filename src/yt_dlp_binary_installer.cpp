/*
 * Copyright (C) 2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "yt_dlp_binary_installer.h"

#include "installer_utils.h"

#include <QDebug>
#include <QDir>
#include <QStandardPaths>

using namespace MiTubo;

YtDlpBinaryInstaller::YtDlpBinaryInstaller(QObject *parent):
    AbstractGithubInstaller(QStringLiteral("yt-dlp/yt-dlp"), parent)
{
    m_programName = QStringLiteral("yt-dlp");
}

void YtDlpBinaryInstaller::downloadUpdate()
{
    auto progressCb = [this](quint64 received, quint64 total) {
        Q_EMIT downloadProgress(received, total);
    };

    auto downloadCb = [this](const QString &filePath) {
        m_downloadedFile = filePath;
        Q_EMIT downloadDone(!filePath.isEmpty());
    };

    QString assetName = QStringLiteral(
#ifdef Q_OS_LINUX
                                       "yt-dlp_linux"
#  ifdef __aarch64__
                                                   "_aarch64"
#  elif defined Q_PROCESSOR_ARM
                                                   "_armv7l"
#  endif
#elif defined Q_OS_MACOS
                                       "yt-dlp_macos"
#elif defined Q_OS_WIN64
                                       "yt-dlp.exe"
#elif defined Q_OS_WIN32
                                       "yt-dlp_x86.exe"
#endif
                                               );
    QUrl downloadUrl = assetUrl(assetName);
    if (downloadUrl.isValid()) {
        qDebug() << "Downloading" << downloadUrl;
    } else {
        qDebug() << "Missing asset for" << assetName;
    }
    QDir extractionDir =
        InstallerUtils::extractionDir(InstallerUtils::EnsureExists);
    InstallerUtils::downloadFile(m_nam,
                                 downloadUrl,
                                 extractionDir.filePath("youtube-dl"),
                                 progressCb, downloadCb);
}

void YtDlpBinaryInstaller::installUpdate()
{
    qDebug() << "activating";

    QDir extractionDir = InstallerUtils::extractionDir();
    QFile::setPermissions(extractionDir.filePath("youtube-dl"),
                          QFileDevice::ReadOwner |
                          QFileDevice::WriteOwner |
                          QFileDevice::ExeOwner |
                          QFileDevice::ReadUser |
                          QFileDevice::WriteUser |
                          QFileDevice::ExeUser |
                          QFileDevice::ReadGroup |
                          QFileDevice::ExeGroup |
                          QFileDevice::ReadOther |
                          QFileDevice::ExeOther);
    InstallerUtils::activateLatestVersion();

    Q_EMIT installDone(true);
}
