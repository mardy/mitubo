/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "youtube_dl_installer.h"

#include "installer_utils.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>

using namespace MiTubo;

YoutubeDlInstaller::YoutubeDlInstaller(QObject *parent):
    AbstractGithubInstaller(QStringLiteral("ytdl-patched/youtube-dl"), parent)
{
    m_programName = QStringLiteral("youtube-dl");
}

void YoutubeDlInstaller::applyPatches(const QString &archivePath)
{
    QDir archiveDir(archivePath);

    patchCompatFile(archiveDir);
    patchMainFile(archiveDir);

    /* Rename the main file */
    archiveDir.rename("__main__.py", "youtube-dl");
}

void YoutubeDlInstaller::patchCompatFile(const QDir &archiveDir)
{
    /* We need to remove the compat_http_server, which is used only in
     * youtube-dl testing framework and causes a startup failure in Ubuntu
     * Touch due to confinement. */
    QFile compatFile(archiveDir.filePath("youtube_dl/compat.py"));
    bool ok = compatFile.open(QIODevice::ReadWrite);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open youtube_dl/compat.py for patching";
        return;
    }
    QByteArray contents = compatFile.readAll();
    contents.replace("import http.server as compat_http_server", "pass");
    contents.replace("import BaseHTTPServer as compat_http_server", "pass");
    compatFile.resize(0);
    compatFile.write(contents);
}

void YoutubeDlInstaller::patchMainFile(const QDir &archiveDir)
{
    /* Correct the shebang line */
    QFile mainFile(archiveDir.filePath("__main__.py"));
    bool ok = mainFile.open(QIODevice::ReadWrite);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open youtube_dl/main.py for patching";
        return;
    }

    QString pythonPath = QStandardPaths::findExecutable("python3");
    QByteArray newShebangLine = "#! " + pythonPath.toUtf8();
    QByteArray contents = mainFile.readAll();
    int lineEnd = contents.indexOf('\n');
    contents.replace(0, lineEnd, newShebangLine);
    mainFile.resize(0);
    mainFile.write(contents);
}

void YoutubeDlInstaller::activateLatestVersion()
{
    qDebug() << "activating";

    applyPatches(InstallerUtils::extractionDir().path());
    InstallerUtils::activateLatestVersion();

    Q_EMIT installDone(true);
}

void YoutubeDlInstaller::downloadUpdate()
{
    auto progressCb = [this](quint64 received, quint64 total) {
        Q_EMIT downloadProgress(received, total);
    };

    auto downloadCb = [this](const QString &filePath) {
        m_downloadedFile = filePath;
        Q_EMIT downloadDone(!filePath.isEmpty());
    };

    QUrl downloadUrl = assetUrl(QStringLiteral("youtube-dl"));
    InstallerUtils::downloadFile(m_nam,
                                 downloadUrl,
                                 "youtube-dl_XXXXXX.zip",
                                 progressCb, downloadCb);
}

void YoutubeDlInstaller::installUpdate()
{
    qDebug() << "extracting";
    QDir extractionDir =
        InstallerUtils::extractionDir(InstallerUtils::ClearContents |
                                      InstallerUtils::EnsureExists);
    InstallerUtils::extractZip(m_downloadedFile,
                               extractionDir.path(),
                               [this](bool ok) {
        if (ok) {
            activateLatestVersion();
        } else {
            Q_EMIT installDone(false);
        }
    });
}
