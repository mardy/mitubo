/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_INSTALLER_UTILS_H
#define MITUBO_INSTALLER_UTILS_H

#include "abstract_installer.h"

#include <QDir>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QVersionNumber>
#include <functional>

class QNetworkAccessManager;
class QUrl;

namespace MiTubo {

class InstallerUtilsPrivate;
class InstallerUtils
{
public:
    enum DirFlag {
        None = 0,
        EnsureExists = 1 << 0,
        ClearContents = 1 << 1,
    };
    Q_DECLARE_FLAGS(DirFlags, DirFlag)

    InstallerUtils(QObject *parent = nullptr);
    ~InstallerUtils();

    using ExtractCb = std::function<void(bool)>;
    static void extractZip(const QString &zipFile,
                           const QString &extractionDir,
                           const ExtractCb &callback);

    static QVersionNumber pythonVersion();

    using ProgressCb = std::function<void(quint64 received, quint64 total)>;
    using DownloadCb = std::function<void(const QString &filePath)>;
    static void downloadFile(QNetworkAccessManager *nam,
                             const QUrl &url,
                             const QString &nameTemplate,
                             const ProgressCb &progressCb,
                             const DownloadCb &downloadCb);

    using FetchUrlCb = std::function<void(const QByteArray &contents,
                                          bool ok)>;
    static void fetchUrl(QNetworkAccessManager *nam,
                         const QUrl &url,
                         const FetchUrlCb &callback);

    /* Make the extraction dir the active dir */
    static void activateLatestVersion();

    static QDir extractionDir(DirFlags flags = DirFlag::None);
    static QDir activeDir(DirFlags flags = DirFlag::None);

private:
    Q_DECLARE_PRIVATE(InstallerUtils)
    QScopedPointer<InstallerUtilsPrivate> d_ptr;
};

} // namespace

Q_DECLARE_METATYPE(MiTubo::InstallerUtils::DirFlags)
Q_DECLARE_OPERATORS_FOR_FLAGS(MiTubo::InstallerUtils::DirFlags)

#endif // MITUBO_INSTALLER_UTILS_H
