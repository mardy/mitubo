/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed_model.h"

#include "feed.h"
#include "feed_store.h"

#include <QDebug>
#include <QQmlEngine>

using namespace MiTubo;

namespace MiTubo {

class FeedModelPrivate {
public:
    FeedModelPrivate(FeedModel *q);

    void reset();

    void handleAddition(int row);
    void handleRemoval(int row);

    void moveFeedInto(int row, int intoRow);

private:
    Q_DECLARE_PUBLIC(FeedModel)
    FeedStore *m_store;
    QString m_path;
    QVector<Feed> m_feeds;
    QHash<int, QByteArray> m_roles;
    FeedModel *q_ptr;
};

} // namespace

FeedModelPrivate::FeedModelPrivate(FeedModel *q):
    m_store(FeedStore::instance()),
    q_ptr(q)
{
    m_roles[FeedModel::UrlRole] = "url";
    m_roles[FeedModel::IsFolderRole] = "isFolder";
    m_roles[FeedModel::FolderPathRole] = "folderPath";
    m_roles[FeedModel::TitleRole] = "title";
    m_roles[FeedModel::DescriptionRole] = "description";
    m_roles[FeedModel::LogoUrlRole] = "logoUrl";
    m_roles[FeedModel::LastUpdatedRole] = "lastUpdated";

    m_feeds = m_store->feeds(m_path);

    QObject::connect(m_store, &FeedStore::feedChanged,
                     q, [this, q](const FeedStore::Position &pos) {
        if (pos.path != m_path) return;
        int row = pos.index;
        QModelIndex index = q->index(row, 0);
        m_feeds[row] = m_store->feeds(m_path)[row];
        Q_EMIT q->dataChanged(index, index);
    });
    QObject::connect(m_store, &FeedStore::feedAdded,
                     q, [this](const FeedStore::Position &pos) {
        if (pos.path != m_path) return;
        handleAddition(pos.index);
    });
    QObject::connect(m_store, &FeedStore::feedMoved,
                     q, [this, q](const FeedStore::Position &from,
                                  const FeedStore::Position &to) {
        if (from.path == m_path && to.path == m_path) {
            /* Unfortunately beginMoveRows() and QVector::move() interpret the
             * "to" index differently; we follow the QVector::move()
             * interpretation here, which means we have to tweak the "to" index
             * we pass to beginMoveRows().
             */
            q->beginMoveRows(QModelIndex(), from.index, from.index,
                             QModelIndex(), from.index > to.index ?
                             to.index : to.index + 1);
            m_feeds.move(from.index, to.index);
            q->endMoveRows();
        } else if (from.path == m_path) {
            // For us it's a removal
            handleRemoval(from.index);
        } else if (to.path == m_path) {
            // For us it's an addition
            handleAddition(to.index);
        }
    });
    QObject::connect(m_store, &FeedStore::feedRemoved,
                     q, [this](const FeedStore::Position &pos, const Feed &) {
        if (pos.path != m_path) return;
        handleRemoval(pos.index);
    });
}

void FeedModelPrivate::reset()
{
    Q_Q(FeedModel);
    q->beginResetModel();
    m_feeds = m_store->feeds(m_path);
    q->endResetModel();
}

void FeedModelPrivate::handleAddition(int row)
{
    Q_Q(FeedModel);
    q->beginInsertRows(QModelIndex(), row, row);
    m_feeds.insert(row, m_store->feeds(m_path)[row]);
    q->endInsertRows();
}

void FeedModelPrivate::handleRemoval(int row)
{
    Q_Q(FeedModel);
    q->beginRemoveRows(QModelIndex(), row, row);
    m_feeds.remove(row);
    q->endRemoveRows();
}

void FeedModelPrivate::moveFeedInto(int row, int intoRow)
{
    if (Q_UNLIKELY(intoRow < -1 || intoRow >= m_feeds.count())) {
        qWarning() << "Invalid destination row:" << intoRow;
        return;
    }
    // No need to check "row": the FeedStore does that

    QString newPath;
    if (intoRow == -1) {
        // Moving to parent folder
        newPath = Feed::parentPath(m_path);
    } else {
        const Feed &folder = m_feeds[intoRow];
        if (Q_UNLIKELY(!folder.isFolder())) {
            qWarning() << "Cannot move into non-folder!";
            return;
        }
        newPath = Feed::subPath(m_path, folder.title);
    }

    m_store->moveFeed({ m_path, row }, { newPath, -1 });
}

FeedModel::FeedModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new FeedModelPrivate(this))
{
    QObject::connect(this, &QAbstractItemModel::rowsInserted,
                     this, &FeedModel::countChanged);
    QObject::connect(this, &QAbstractItemModel::rowsRemoved,
                     this, &FeedModel::countChanged);
    QObject::connect(this, &QAbstractItemModel::modelReset,
                     this, &FeedModel::countChanged);
}

FeedModel::~FeedModel() = default;

void FeedModel::setPath(const QString &path)
{
    Q_D(FeedModel);
    if (path == d->m_path) return;
    d->m_path = path;
    d->reset();
    Q_EMIT pathChanged();
}

QString FeedModel::path() const
{
    Q_D(const FeedModel);
    return d->m_path;
}

QString FeedModel::parentPath() const
{
    return Feed::parentPath(path());
}

QVariantMap FeedModel::folderInfo() const
{
    Q_D(const FeedModel);
    if (d->m_path.isEmpty()) return QVariantMap();

    const FeedStore::Position &pos = d->m_store->findFolder(d->m_path);
    if (pos.index < 0) return QVariantMap();

    const Feed &folder = d->m_store->feedAt(pos);
    return folder.toMap();
}

QVariant FeedModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

void FeedModel::addOrUpdateFeed(const QVariantMap &m)
{
    Q_D(FeedModel);

    Feed feed;
    bool ok = Feed::fromMap(m, &feed);
    if (Q_UNLIKELY(!ok || feed.isFolder())) {
        qWarning() << "Cannot add invalid feed:" << m;
        return;
    }

    d->m_store->addOrUpdateFeed(feed);
}

void FeedModel::addOrUpdateFolder(const QVariantMap &m)
{
    Q_D(FeedModel);

    Feed feed;
    bool ok = Feed::fromMap(m, &feed);
    if (Q_UNLIKELY(!ok || !feed.isFolder())) {
        qWarning() << "Cannot add invalid folder:" << m;
        return;
    }

    d->m_store->addOrUpdateFeed(feed);
}

void FeedModel::moveFeed(int row, int toRow)
{
    Q_D(FeedModel);
    d->m_store->moveFeed({ d->m_path, row }, { d->m_path, toRow });
}

void FeedModel::moveFeedInto(int row, int intoRow)
{
    Q_D(FeedModel);
    d->moveFeedInto(row, intoRow);
}

void FeedModel::deleteFeed(int row)
{
    Q_D(FeedModel);
    if (Q_UNLIKELY(row < 0 || row >= rowCount())) {
        qWarning() << "Invalid row requested:" << row;
        return;
    }

    d->m_store->deleteFeed({ d->m_path, row });
}

QVariant FeedModel::data(const QModelIndex &index, int role) const
{
    Q_D(const FeedModel);

    Q_ASSERT(index.column() == 0);

    int row = index.row();
    if (Q_UNLIKELY(row < 0 || row >= rowCount())) {
        qWarning() << "Invalid index requested:" << row;
        return QVariant();
    }

    const Feed &feed = d->m_feeds[row];
    switch (role) {
    case UrlRole:
        return feed.url;
    case IsFolderRole:
        return feed.isFolder();
    case FolderPathRole:
        if (Q_UNLIKELY(!feed.isFolder())) {
            qWarning() << "requested path of non folder!";
            return QString();
        }
        return Feed::subPath(d->m_path, feed.title);
    case TitleRole:
        return feed.title;
    case DescriptionRole:
        return feed.description;
    case LogoUrlRole:
        return feed.logoUrl;
    case LastUpdatedRole:
        return QVariant::fromValue(feed.lastUpdated);
    default:
        return QVariant();
    }
}

int FeedModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const FeedModel);
    Q_ASSERT(!parent.isValid());
    return d->m_feeds.count();
}

QHash<int, QByteArray> FeedModel::roleNames() const
{
    Q_D(const FeedModel);
    return d->m_roles;
}
