/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_INSTALLER_H
#define MITUBO_INSTALLER_H

#include "abstract_installer.h"

#include <QObject>
#include <QScopedPointer>
#include <QString>

namespace MiTubo {

class InstallerPrivate;
class Installer: public QObject
{
    Q_OBJECT
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString programName READ programName CONSTANT)
    Q_PROPERTY(QString currentVersion READ currentVersion
               NOTIFY currentVersionChanged)
    Q_PROPERTY(QString latestVersion READ latestVersion NOTIFY statusChanged)
    Q_PROPERTY(bool autoDownload READ autoDownload
               WRITE setAutoDownload NOTIFY autoDownloadChanged)
    Q_PROPERTY(bool autoInstall READ autoInstall
               WRITE setAutoInstall NOTIFY autoInstallChanged)
    Q_PROPERTY(qint64 bytesReceived READ bytesReceived
               NOTIFY bytesReceivedChanged)
    Q_PROPERTY(qint64 bytesTotal READ bytesTotal NOTIFY bytesTotalChanged)

public:
    enum Status {
        UnknownStatus,
        CheckingForUpdates,
        NoUpdatesAvailable,
        UpdateAvailable,
        CheckForUpdatesFailed,
        Downloading,
        DownloadComplete,
        DownloadFailed,
        Installing,
        InstallationComplete,
        InstallationFailed,
    };
    Q_ENUM(Status)

    Installer(QObject *parent = nullptr);
    ~Installer();

    static QString executableDir();

    Status status() const;

    QString programName() const;

    QString currentVersion() const;
    QString latestVersion() const;

    void setAutoDownload(bool autoDownload);
    bool autoDownload() const;

    void setAutoInstall(bool autoInstall);
    bool autoInstall() const;

    int64_t bytesReceived() const;
    int64_t bytesTotal() const;

    Q_INVOKABLE void checkForUpdates();
    Q_INVOKABLE void downloadLatestVersion();
    Q_INVOKABLE void installLatestVersion();

Q_SIGNALS:
    void statusChanged();
    void currentVersionChanged();
    void autoDownloadChanged();
    void autoInstallChanged();
    void bytesReceivedChanged();
    void bytesTotalChanged();

protected:
    void setStatus(Status status);

private:
    Q_DECLARE_PRIVATE(Installer)
    QScopedPointer<InstallerPrivate> d_ptr;
};

} // namespace

#endif // MITUBO_INSTALLER_H
