/*
 * Copyright (C) 2021-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "feed_store.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QHash>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QStandardPaths>
#include <algorithm>

using namespace MiTubo;

namespace MiTubo {

static FeedStore *s_instance = nullptr;

const QString keyDescription = QStringLiteral("description");
const QString keyFeeds = QStringLiteral("feeds");
const QString keyLastUpdated = QStringLiteral("lastUpdated");
const QString keyLogoUrl = QStringLiteral("logoUrl");
const QString keyTitle = QStringLiteral("title");
const QString keyUrl = QStringLiteral("url");
const QString s_feedDir = QStringLiteral("feeds");

class FeedStorePrivate {
public:
    FeedStorePrivate(FeedStore *q);

    QJsonArray saveFeedFolder(const QString &path) const;
    bool saveFeeds() const;
    void loadFeedFolder(const QString &path, const QJsonArray &items);
    void loadFeeds();

    void insertFeed(const Feed &feed, const FeedStore::Position &pos);

    static QJsonObject feedToJson(const Feed &feed);
    static bool feedFromJson(const QJsonObject &object, Feed *feed);

private:
    Q_DECLARE_PUBLIC(FeedStore)
    QDir m_baseDir;
    QHash<QString,QVector<Feed>> m_feeds;
    Feed m_invalidFeed;
    FeedStore *q_ptr;
};

} // namespace

FeedStorePrivate::FeedStorePrivate(FeedStore *q):
    m_baseDir(QStandardPaths::writableLocation(
        QStandardPaths::AppDataLocation)),
    q_ptr(q)
{
    loadFeeds();
}

QJsonObject FeedStorePrivate::feedToJson(const Feed &feed)
{
    return {
        { keyUrl, feed.url.toString() },
        { keyLastUpdated, feed.lastUpdated.toString(Qt::ISODateWithMs) },
        { keyTitle, feed.title },
        { keyDescription, feed.description },
        { keyLogoUrl, feed.logoUrl.toString() },
    };
}

bool FeedStorePrivate::feedFromJson(const QJsonObject &o, Feed *feed)
{
    feed->url = QUrl(o[keyUrl].toString());
    feed->lastUpdated = QDateTime::fromString(o[keyLastUpdated].toString(),
                                              Qt::ISODateWithMs);
    feed->title = o[keyTitle].toString();
    feed->description = o[keyDescription].toString();
    feed->logoUrl = QUrl(o[keyLogoUrl].toString());
    return feed->url.isValid() || !feed->title.isEmpty();
}

QJsonArray FeedStorePrivate::saveFeedFolder(const QString &path) const
{
    QJsonArray feeds;
    for (const auto &feed: m_feeds[path]) {
        QJsonObject feedObject = feedToJson(feed);
        if (feed.isFolder()) {
            if (Q_UNLIKELY(feed.title.isEmpty())) {
                qWarning() << "Feed with empty URL and title, ignoring";
                continue;
            }

            QString subPath = Feed::subPath(path, feed.title);
            QJsonArray subItems = saveFeedFolder(subPath);
            feedObject.insert(keyFeeds, subItems);
        }
        feeds.append(feedObject);
    }
    return feeds;
}

bool FeedStorePrivate::saveFeeds() const
{
    QFile store(m_baseDir.filePath("feeds.json"));
    if (!store.open(QIODevice::WriteOnly)) {
        qWarning() << "Could not create feed file" << store.errorString();
        return false;
    }

    const QJsonArray feeds = saveFeedFolder(QString());

    QJsonObject jsonStore {
        { keyFeeds, feeds },
    };
    const QByteArray contents =
        QJsonDocument(jsonStore).toJson(QJsonDocument::Indented);
    int bytesWritten = store.write(contents);
    if (Q_UNLIKELY(bytesWritten < 0 || bytesWritten < contents.size())) {
        qWarning() << "Could not write feed" << store.fileName()
            << ":" << store.errorString();
        return false;
    }

    return true;
}

void FeedStorePrivate::loadFeedFolder(const QString &path, const QJsonArray &items)
{
    QVector<Feed> feeds;
    feeds.reserve(items.count());
    for (const QJsonValue &v: items) {
        Feed feed;
        const QJsonObject feedObject = v.toObject();
        bool ok = feedFromJson(feedObject, &feed);
        if (ok) {
            feeds.append(feed);
        } else {
            qWarning() << "Could not load feed:" << v;
        }

        if (feedObject.contains(keyFeeds)) {
            // it's a feed folder
            const QJsonArray subItems = feedObject.value(keyFeeds).toArray();
            loadFeedFolder(Feed::subPath(path, feed.title), subItems);
        }
    }

    if (feeds.count() > 0) {
        m_feeds.insert(path, feeds);
    }
}

void FeedStorePrivate::loadFeeds()
{
    QFile store(m_baseDir.filePath("feeds.json"));
    if (!store.open(QIODevice::ReadOnly)) {
        return;
    }

    const QJsonObject jsonStore =
        QJsonDocument::fromJson(store.readAll()).object();
    const QJsonArray feeds = jsonStore.value(keyFeeds).toArray();
    loadFeedFolder(QString(), feeds);
}

void FeedStorePrivate::insertFeed(const Feed &feed,
                                  const FeedStore::Position &pos)
{
    Q_Q(FeedStore);
    QVector<Feed> &feeds = m_feeds[pos.path];
    if (pos.index < 0) {
        feeds.append(feed);
    } else {
        feeds.insert(pos.index, feed);
    }
    Q_EMIT q->feedAdded(pos);
}

FeedStore::FeedStore(QObject *parent):
    QObject(parent),
    d_ptr(new FeedStorePrivate(this))
{
}

FeedStore::~FeedStore() = default;

FeedStore *FeedStore::instance()
{
    if (!s_instance) {
        s_instance = new FeedStore();
    }
    return s_instance;
}

const QVector<Feed> FeedStore::feeds(const QString &path) const
{
    Q_D(const FeedStore);
    return d->m_feeds[path];
}

FeedStore::Position FeedStore::findFeed(const Feed &feed) const
{
    Q_D(const FeedStore);
    for (auto p = d->m_feeds.begin(); p != d->m_feeds.end(); p++) {
        const auto &feeds = p.value();
        for (int i = 0; i < feeds.count(); i++) {
            const Feed &f = feeds[i];
            if ((feed.isFolder() && f.title == feed.title) ||
                (!feed.isFolder() && f.url == feed.url)) {
                return Position { p.key(), i };
            }
        }
    }
    return Position();
}

FeedStore::Position FeedStore::findFolder(const QString &path) const
{
    Q_D(const FeedStore);
    if (path.isEmpty()) return Position();
    const QString parentPath = Feed::parentPath(path);
    const QVector<Feed> &feeds = d->m_feeds[parentPath];
    for (int i = 0; i < feeds.count(); i++) {
        const Feed &feed = feeds[i];
        if (feed.isFolder() &&
            Feed::subPath(parentPath, feed.title) == path) {
            return Position { parentPath, i };
        }
    }
    return Position();
}

const Feed &FeedStore::feedAt(const Position &pos) const
{
    Q_D(const FeedStore);
    auto iPos = d->m_feeds.find(pos.path);
    if (Q_UNLIKELY(iPos == d->m_feeds.constEnd())) {
        qWarning() << "Invalid feed path:" << pos.path;
        return d->m_invalidFeed;
    }
    auto &folder = iPos.value();
    if (Q_UNLIKELY(pos.index < 0 || pos.index >= folder.count())) {
        qWarning() << "Invalid feed index:" << pos.index << "in" << pos.path;
        return d->m_invalidFeed;
    }
    return folder.at(pos.index);
}

void FeedStore::addOrUpdateFeed(const Feed &feed)
{
    Q_D(FeedStore);

    const Position pos = findFeed(feed);
    if (pos.index >= 0) {
        d->m_feeds[pos.path][pos.index] = feed;
        Q_EMIT feedChanged(pos);
    } else {
        int index = d->m_feeds[QString()].count();
        d->m_feeds[QString()].append(feed);
        Q_EMIT feedAdded(Position { QString(), index });
    }
    d->saveFeeds();
}

void FeedStore::moveFeed(const Position &from, const Position &to)
{
    Q_D(FeedStore);
    auto iFrom = d->m_feeds.find(from.path);
    if (Q_UNLIKELY(iFrom == d->m_feeds.constEnd())) {
        qWarning() << "Invalid feed path:" << from.path;
        return;
    }
    auto &srcFolder = iFrom.value();
    if (Q_UNLIKELY(from.index < 0 || from.index >= srcFolder.count())) {
        qWarning() << "Invalid feed index:" << from.index << "in" << from.path;
        return;
    }
    auto &dstFolder = d->m_feeds[to.path];
    if (Q_UNLIKELY(to.index < -1 || to.index >= dstFolder.count())) {
        qWarning() << "Invalid feed destination:" << to.index << "in" << to.path;
        return;
    }
    int toIndex = to.index;
    if (toIndex == -1) {
        toIndex = dstFolder.count();
    }

    if (from.path == to.path) {
        srcFolder.move(from.index, toIndex);
    } else {
        const Feed &feed = srcFolder.at(from.index);
        dstFolder.insert(toIndex, feed);
        srcFolder.removeAt(from.index);
    }
    d->saveFeeds();
    // prefer toIndex over to.index (we don't like -1)
    Q_EMIT feedMoved(from, {to.path, toIndex});
}

void FeedStore::deleteFeed(const Position &pos)
{
    Q_D(FeedStore);
    auto iPos = d->m_feeds.find(pos.path);
    if (Q_UNLIKELY(iPos == d->m_feeds.constEnd())) {
        qWarning() << "Invalid feed path:" << pos.path;
        return;
    }
    auto &folder = iPos.value();
    if (Q_UNLIKELY(pos.index < 0 || pos.index >= folder.count())) {
        qWarning() << "Invalid feed index:" << pos.index << "in" << pos.path;
        return;
    }

    const Feed feed = folder.takeAt(pos.index);
    Q_EMIT feedRemoved(pos, feed);

    d->saveFeeds();
}
