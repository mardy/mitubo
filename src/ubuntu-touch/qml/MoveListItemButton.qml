import QtQuick.Controls 2.12

ToolButton {
	icon.name: "sort-listitem"
	display: AbstractButton.TextUnderIcon
	text: qsTr("Move…")
}
