import QtMultimedia 5.8
import QtQuick.Controls 2.12

Label {
    property var playerStatus: null

    property var _statusMap: new Map([
        [MediaPlayer.Loading, QT_TRANSLATE_NOOP("Media", "Loading")],
        [MediaPlayer.Buffering, QT_TRANSLATE_NOOP("Media", "Buffering")],
        [MediaPlayer.Stalled, QT_TRANSLATE_NOOP("Media", "Buffering")],
    ])
    text: qsTr(_statusMap.get(playerStatus) || "")
}
