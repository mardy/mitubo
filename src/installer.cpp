/*
 * Copyright (C) 2020-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "installer.h"

#include "installer_utils.h"
#include "yt_dlp_binary_installer.h"
#include "yt_dlp_installer.h"
#include "youtube_dl.h"
#include "youtube_dl_installer.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QNetworkAccessManager>
#include <QQmlEngine>
#include <QUrl>
#include <QVersionNumber>

using namespace MiTubo;

namespace MiTubo {

class InstallerPrivate
{
    Q_DECLARE_PUBLIC(Installer)

public:
    InstallerPrivate(Installer *q);

    static AbstractInstaller *installerFactory();

    bool prepareDownloadFile();
    void saveData(QIODevice *network);

    void checkNeedsUpdate();

    void setBytesReceived(int64_t bytesReceived);
    void setBytesTotal(int64_t bytesTotal);

private:
    QScopedPointer<AbstractInstaller> m_installer;
    Installer::Status m_status;
    QString m_currentVersion;
    QString m_latestVersion;
    bool m_currentVersionRetrieved = false;
    bool m_latestVersionRetrieved = false;
    bool m_autoDownload = false;
    bool m_autoInstall = true;
    int64_t m_bytesReceived = 0;
    int64_t m_bytesTotal = 0;
    QString m_downloadedFile;
    Installer *q_ptr;
};

} // namespace

InstallerPrivate::InstallerPrivate(Installer *q):
    m_installer(installerFactory()),
    m_status(Installer::UnknownStatus),
    q_ptr(q)
{
    using S = Installer::Status;
    QObject::connect(m_installer.data(), &AbstractInstaller::checkLatestDone,
                     q, [this, q](bool ok) {
        if (ok) {
            m_latestVersion = m_installer->latestVersion();
            m_latestVersionRetrieved = true;
            checkNeedsUpdate();
        } else {
            q->setStatus(S::CheckForUpdatesFailed);
        }
    });

    QObject::connect(m_installer.data(), &AbstractInstaller::downloadProgress,
                     q, [this](quint64 received, quint64 total) {
        setBytesReceived(received);
        setBytesTotal(total);
    });
    QObject::connect(m_installer.data(), &AbstractInstaller::downloadDone,
                     q, [this, q](bool ok) {
        q->setStatus(ok ? S::DownloadComplete : S::DownloadFailed);
    });

    QObject::connect(m_installer.data(), &AbstractInstaller::installDone,
                     q, [this, q](bool ok) {
        q->setStatus(ok ? S::InstallationComplete : S::InstallationFailed);
    });
}

AbstractInstaller *InstallerPrivate::installerFactory()
{
    QVersionNumber version = InstallerUtils::pythonVersion();
    qDebug() << "Python version:" << version;
    if (version >= QVersionNumber(3, 9)) {
        qDebug() << "Using yt-dlp with source code";
        return new YtDlpInstaller();
    } else {
        qDebug() << "Using pre-built binary installer";
        return new YtDlpBinaryInstaller();
    }
}

void InstallerPrivate::checkNeedsUpdate()
{
    Q_Q(Installer);

    if (!m_currentVersionRetrieved ||
        !m_latestVersionRetrieved) return;

    q->setStatus(m_latestVersion > m_currentVersion ?
                 Installer::UpdateAvailable : Installer::NoUpdatesAvailable);
}

void InstallerPrivate::setBytesReceived(int64_t bytesReceived)
{
    Q_Q(Installer);
    m_bytesReceived = bytesReceived;
    Q_EMIT q->bytesReceivedChanged();
}

void InstallerPrivate::setBytesTotal(int64_t bytesTotal)
{
    Q_Q(Installer);
    if (bytesTotal == m_bytesTotal) return;
    m_bytesTotal = bytesTotal;
    Q_EMIT q->bytesTotalChanged();
}

Installer::Installer(QObject *parent):
    QObject(parent),
    d_ptr(new InstallerPrivate(this))
{
    QObject::connect(this, &Installer::statusChanged,
                     [this]() {
        if (status() == UpdateAvailable && autoDownload()) {
            downloadLatestVersion();
        } else if (status() == DownloadComplete && autoInstall()) {
            installLatestVersion();
        }
    });
}

Installer::~Installer() = default;

QString Installer::executableDir()
{
    return InstallerUtils::activeDir().path();
}

void Installer::setStatus(Status status)
{
    Q_D(Installer);

    if (status == d->m_status) return;
    d->m_status = status;
    Q_EMIT statusChanged();
}

Installer::Status Installer::status() const
{
    Q_D(const Installer);
    return d->m_status;
}

QString Installer::programName() const
{
    Q_D(const Installer);
    return d->m_installer->programName();
}

QString Installer::currentVersion() const
{
    Q_D(const Installer);
    return d->m_currentVersion;
}

QString Installer::latestVersion() const
{
    Q_D(const Installer);
    return d->m_latestVersion;
}

void Installer::setAutoDownload(bool autoDownload)
{
    Q_D(Installer);

    if (autoDownload == d->m_autoDownload) return;
    d->m_autoDownload = autoDownload;
    Q_EMIT autoDownloadChanged();
}

bool Installer::autoDownload() const
{
    Q_D(const Installer);
    return d->m_autoDownload;
}

void Installer::setAutoInstall(bool autoInstall)
{
    Q_D(Installer);

    if (autoInstall == d->m_autoInstall) return;
    d->m_autoInstall = autoInstall;
    Q_EMIT autoInstallChanged();
}

bool Installer::autoInstall() const
{
    Q_D(const Installer);
    return d->m_autoInstall;
}

int64_t Installer::bytesReceived() const
{
    Q_D(const Installer);
    return d->m_bytesReceived;
}

int64_t Installer::bytesTotal() const
{
    Q_D(const Installer);
    return d->m_bytesTotal;
}

void Installer::checkForUpdates()
{
    Q_D(Installer);
    setStatus(CheckingForUpdates);

    QQmlEngine *engine = qmlEngine(this);
    Q_ASSERT(engine);
    QNetworkAccessManager *nam = engine->networkAccessManager();
    Q_ASSERT(nam);
    d->m_installer->setNetworkAccessManager(nam);

    d->m_currentVersionRetrieved = false;
    d->m_latestVersionRetrieved = false;
    YoutubeDl *downloader = new YoutubeDl(this);
    downloader->runCommand({ QStringLiteral("--version") },
                           [this, downloader](const QByteArray &output,
                                              const QByteArray &errorOutput,
                                              bool ok) {
        Q_D(Installer);

        Q_UNUSED(errorOutput);
        d->m_currentVersion = ok ? output.trimmed() : QString();
        Q_EMIT currentVersionChanged();
        qDebug() << "Installed version:" << d->m_currentVersion;
        d->m_currentVersionRetrieved = true;
        d->checkNeedsUpdate();
        downloader->deleteLater();
    });

    d->m_installer->checkLatest();
}

void Installer::downloadLatestVersion()
{
    Q_D(Installer);
    setStatus(Downloading);
    d->m_installer->downloadUpdate();
}

void Installer::installLatestVersion()
{
    Q_D(Installer);
    setStatus(Installing);
    d->m_installer->installUpdate();
}
