/*
 * Copyright (C) 2022-2024 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of MiTubo.
 *
 * MiTubo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * MiTubo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with MiTubo.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MITUBO_ABSTRACT_GITHUB_INSTALLER_H
#define MITUBO_ABSTRACT_GITHUB_INSTALLER_H

#include "abstract_installer.h"

#include <QJsonObject>

namespace MiTubo {

/*
 * Subclass to facilitate getting the latest version out of GitHub projects.
 */
class AbstractGithubInstaller: public AbstractInstaller
{
    Q_OBJECT

public:
    AbstractGithubInstaller(const QString &projectPath,
                            QObject *parent = nullptr);

    void checkLatest() override;

protected:
    QUrl assetUrl(const QString &assetName) const;
    void parseLatestVersion(const QByteArray &contents);

protected:
    QString m_projectPath;
    QJsonObject m_latestRelease;
    QUrl m_downloadUrl;
};

} // namespace

#endif // MITUBO_ABSTRACT_GITHUB_INSTALLER_H
